import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://school.moodledemo.net/login/index.php')

WebUI.setText(findTestObject('Object Repository/Page_Log in to the site  Mount Orange School/input_username'), 'teacher')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Log in to the site  Mount Orange School/input_password'), 
    'o10DKVTkvpQ=')

WebUI.click(findTestObject('Object Repository/Page_Log in to the site  Mount Orange School/button_Log in'))

WebUI.click(findTestObject('Object Repository/Page_My courses  Mount Orange School/span_Mindful course creation'))

WebUI.click(findTestObject('Object Repository/Page_Course Mindful course creation  Mount _7dd107/input_setmode'))

WebUI.click(findTestObject('Object Repository/Page_Edit course Mindful course creation  M_f26f7d/span_Add an activity or resource'))

WebUI.click(findTestObject('Object Repository/Page_Edit course Mindful course creation  M_f26f7d/a_Workshop'))

WebUI.click(findTestObject('Object Repository/Page_Editing Workshop  Mount Orange School/input_name'))

WebUI.click(findTestObject('Object Repository/Page_Editing Workshop  Mount Orange School/div_Description'))

WebUI.setText(findTestObject('Object Repository/Page_Editing Workshop  Mount Orange School/input_name'), worshop_name)

WebUI.click(findTestObject('Object Repository/Page_Editing Workshop  Mount Orange School/div_Description'))

if (expected_result == 'pass') {
    if (WebUI.getText(findTestObject('Object Repository/Page_Editing Workshop  Mount Orange School/div_- You must supply a value here')) == 
    '- You must supply a value here.') {
        assert false
    }
} else if (WebUI.getText(findTestObject('Object Repository/Page_Editing Workshop  Mount Orange School/div_- You must supply a value here')) == 
'') {
    assert false
}

WebUI.click(findTestObject('Object Repository/Page_Editing Workshop  Mount Orange School/input_submitbutton'))

WebUI.closeBrowser()

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

