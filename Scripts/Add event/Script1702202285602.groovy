import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://school.moodledemo.net/login/index.php')

WebUI.setText(findTestObject('Object Repository/addevent3/Page_Log in to the site  Mount Orange School/input_username'), 
    'teacher')

WebUI.setEncryptedText(findTestObject('Object Repository/addevent3/Page_Log in to the site  Mount Orange School/input_password'), 
    'o10DKVTkvpQ=')

WebUI.click(findTestObject('Object Repository/addevent3/Page_Log in to the site  Mount Orange School/button_Log in'))

WebUI.navigateToUrl('https://school.moodledemo.net/my/')

WebUI.click(findTestObject('Object Repository/addevent3/Page_Dashboard  Mount Orange School/button_New event'))

WebUI.setText(findTestObject('Object Repository/addevent3/Page_Dashboard  Mount Orange School/input_name'), even_name)

WebUI.selectOptionByValue(findTestObject('Object Repository/addevent3/Page_Dashboard  Mount Orange School/select_1        2        3        4        _45ea30'), 
    day, true)

WebUI.selectOptionByValue(findTestObject('Object Repository/addevent3/Page_Dashboard  Mount Orange School/select_1        2        3        4        _45ea30'), 
    day, true)

WebUI.selectOptionByValue(findTestObject('Object Repository/addevent3/Page_Dashboard  Mount Orange School/select_1900        1901        1902        _6405c9'), 
    year, true)

WebUI.selectOptionByValue(findTestObject('Object Repository/addevent3/Page_Dashboard  Mount Orange School/select_00        01        02        03    _f6e37b'), 
    hour, true)

WebUI.selectOptionByValue(findTestObject('Object Repository/addevent3/Page_Dashboard  Mount Orange School/select_00        01        02        03    _f6e37b_1'), 
    minute, true)

WebUI.click(findTestObject('Object Repository/addevent3/Page_Dashboard  Mount Orange School/button_Save_1'))

if (expected_result == 'pass') {
    if (WebUI.getText(findTestObject('Object Repository/addevent3/Page_Dashboard  Mount Orange School/div_- Required_1')) == 
    '- Required') {
        assert false
    }
} else if (WebUI.getText(findTestObject('Object Repository/addevent3/Page_Dashboard  Mount Orange School/div_- Required_1')) == 
'') {
    assert false
}

WebUI.closeBrowser()

