import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://school.moodledemo.net/')

WebUI.click(findTestObject('Object Repository/Page_Home  Mount Orange School/a_Log in'))

WebUI.setText(findTestObject('Object Repository/Page_Log in to the site  Mount Orange School/input_username'), 'teacher')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Log in to the site  Mount Orange School/input_password'), 
    'o10DKVTkvpQ=')

WebUI.click(findTestObject('Object Repository/Page_Log in to the site  Mount Orange School/button_Log in'))

boolean b1

boolean b2 = false

switch (url) {
    case '/login':
        try {
            WebUI.verifyElementPresent(findTestObject('Object Repository/LoginTestCase/Page_Policies and agreements  Mount Orange School/a_Policies and agreements'), 
                0)

            WebUI.click(findTestObject('Object Repository/LoginTestCase/Page_Policies and agreements  Mount Orange School/a_Next'))

            WebUI.click(findTestObject('Object Repository/LoginTestCase/Page_Policies and agreements  Mount Orange School/a_Next'))

            WebUI.click(findTestObject('Object Repository/LoginTestCase/Page_Policies and agreements  Mount Orange School/a_Next'))

            WebUI.click(findTestObject('Object Repository/LoginTestCase/Page_Policies and agreements  Mount Orange School/input_status8'))

            WebUI.click(findTestObject('Object Repository/LoginTestCase/Page_Policies and agreements  Mount Orange School/input_status12'))

            WebUI.click(findTestObject('Object Repository/LoginTestCase/Page_Policies and agreements  Mount Orange School/input_status13'))

            WebUI.click(findTestObject('Object Repository/LoginTestCase/Page_Policies and agreements  Mount Orange School/input_submit'))
        }
        catch (def e) {
        } 
        
        WebUI.click(findTestObject('Object Repository/LoginTestCase/Page_My courses  Mount Orange School/auser-menu-toggle'))

        WebUI.click(findTestObject('Object Repository/LoginTestCase/Page_My courses  Mount Orange School/a_Profile'))

        boolean a = WebUI.verifyElementText(findTestObject('Object Repository/LoginTestCase/Page_Richard Kim Public profile  Mount Oran_d614e1/a_richardkim225example.com'), 
            expected_email)

        WebUI.closeBrowser()

        assert a

        break
    case '/update-profile':
        WebUI.navigateToUrl('https://school.moodledemo.net/user/profile.php')

        WebUI.click(findTestObject('Object Repository/Page_Jeffrey Sanders Public profile  Mount _939a30/a_Edit profile'))

        WebUI.setText(findTestObject('Object Repository/Page_Mount Orange School Edit profile  Moun_c26fb2/input_firstname'), 
            textfield1)

        WebUI.setText(findTestObject('Object Repository/Page_Mount Orange School Edit profile  Moun_c26fb2/input_lastname'), 
            textfield2)

        WebUI.click(findTestObject('Object Repository/Page_Mount Orange School Edit profile  Moun_c26fb2/input_submitbutton'))

        b1 = WebUI.verifyTextPresent('- Missing given name', true, FailureHandling.OPTIONAL)

        b2 = WebUI.verifyTextPresent('- Missing last name', true, FailureHandling.OPTIONAL)

        break
    case '/message':
        WebUI.navigateToUrl('https://school.moodledemo.net/message/index.php')

        WebUI.click(findTestObject('Object Repository/Page_Messages  Mount Orange School/a_Barbara Gardner                          _1f06d2'))

        WebUI.verifyElementText(findTestObject('Object Repository/Page_Messages  Mount Orange School/textareayui_3_18_1_1_1702220560356_39'), 
    textfield1)

        WebUI.click(findTestObject('Object Repository/Page_Messages  Mount Orange School/button_btn btn-link btn-icon icon-size-3 ml_fd4df5'))

        break
    case '/add_workshop':
        WebUI.navigateToUrl('https://school.moodledemo.net/my/courses.php')

        WebUI.click(findTestObject('Object Repository/my-test/Page_My courses  Mount Orange School/span_Mindful course creation'))

        WebUI.click(findTestObject('Object Repository/my-test/Page_Course Mindful course creation  Mount _7dd107/input_setmode'))

        WebUI.click(findTestObject('Object Repository/my-test/Page_Edit course Mindful course creation  M_f26f7d/span_Add an activity or resource'))

        WebUI.click(findTestObject('Object Repository/my-test/Page_Edit course Mindful course creation  M_f26f7d/div_Workshop'))

        WebUI.setText(findTestObject('Object Repository/my-test/Page_Editing Workshop  Mount Orange School/input_name'), 
            text_field1)

        if (expected_result == 'pass') {
            if (WebUI.getText(findTestObject('Object Repository/Page_Editing Workshop  Mount Orange School/div_- You must supply a value here')) == 
            '- You must supply a value here.') {
                assert false
            }
        } else if (WebUI.getText(findTestObject('Object Repository/Page_Editing Workshop  Mount Orange School/div_- You must supply a value here')) == 
        '') {
            assert false
        }
        
        WebUI.click(findTestObject('Object Repository/my-test/Page_Editing Workshop  Mount Orange School/input_submitbutton'))

        break
    case '/add-event':
        WebUI.navigateToUrl('https://school.moodledemo.net/my/')

        WebUI.click(findTestObject('Object Repository/Page_Dashboard  Mount Orange School/button_New event'))

        WebUI.setText(findTestObject('Object Repository/Page_Dashboard  Mount Orange School/input_name'), textfield1)

        WebUI.click(findTestObject('Object Repository/my-test/Page_Editing Workshop  Mount Orange School/input_submitbutton'))

        if (expected_result == 'pass') {
            if (WebUI.getText(findTestObject('Object Repository/addevent3/Page_Dashboard  Mount Orange School/div_- Required_1')) == 
            '- Required') {
                assert false
            }
        } else if (WebUI.getText(findTestObject('Object Repository/addevent3/Page_Dashboard  Mount Orange School/div_- Required_1')) == 
        '') {
            assert false
        }
        
        break
    case '/add-quiz':
        WebUI.click(findTestObject('Object Repository/Page_My courses  Mount Orange School/span_Moodle and Mountaineering'))

        WebUI.click(findTestObject('Object Repository/Page_Course Moodle and Mountaineering  Moun_af917f/input_setmode'))

        WebUI.click(findTestObject('Object Repository/Page_Edit course Moodle and Mountaineering _c4af8c/span_Add an activity or resource'))

        WebUI.click(findTestObject('Object Repository/Page_Edit course Moodle and Mountaineering _c4af8c/img_icon activityicon'))

        if (textfield1 != '') {
            WebUI.setText(findTestObject('Object Repository/Page_Editing Quiz  Mount Orange School/input_name'), textfield1)
        }
        
        WebUI.click(findTestObject('Object Repository/my-test/Page_Editing Workshop  Mount Orange School/input_submitbutton'))

        b1 = WebUI.verifyTextPresent('You must supply a value here.', true, FailureHandling.OPTIONAL)

        break
    default:
        break
}

WebUI.closeBrowser()

switch (expected_result) {
    case 'pass':
        assert !(b1) && !(b2)

        break
    case 'fail':
        assert !(b1 || b2)

        break
    default:
        assert false

        break
}

