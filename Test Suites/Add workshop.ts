<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Add workshop</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>453b5248-7640-4e4d-bef0-41754657f833</testSuiteGuid>
   <testCaseLink>
      <guid>13d4636b-8fc8-4855-b155-80c5f4c19a3a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/add workshop</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>65c01ffc-0a81-4849-83f1-d45a41fa2070</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/workshop_data</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>65c01ffc-0a81-4849-83f1-d45a41fa2070</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>worshop_name</value>
         <variableId>fb2dd8c5-c858-4d47-bde4-1d3010ab93aa</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>65c01ffc-0a81-4849-83f1-d45a41fa2070</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>expected_result</value>
         <variableId>a0513006-85f8-421d-8669-20828fb6cbb1</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
