<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Update Profile</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>10313a08-538c-43ca-af88-a750a198b51c</testSuiteGuid>
   <testCaseLink>
      <guid>b33e97d5-8df2-4dd5-a7bd-91a8113dd613</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Update Profile</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>7b101b63-d553-4349-96cc-e2686e393789</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Update Profile</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>7b101b63-d553-4349-96cc-e2686e393789</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>first_name</value>
         <variableId>2efa38af-d4a3-4bf8-9486-6bbdcc1dd5fb</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7b101b63-d553-4349-96cc-e2686e393789</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>last_name</value>
         <variableId>9a1241b7-76c2-4df4-bc99-ad31633ab5b4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7b101b63-d553-4349-96cc-e2686e393789</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>visibility</value>
         <variableId>72741bb9-0e27-450e-a32b-33f4cc94c65e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7b101b63-d553-4349-96cc-e2686e393789</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>city</value>
         <variableId>44122240-4367-46c4-ac58-983cad4ddaed</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7b101b63-d553-4349-96cc-e2686e393789</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>theme</value>
         <variableId>12430ad1-a3b6-480d-93b9-7891c8175aa7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7b101b63-d553-4349-96cc-e2686e393789</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>expected_result</value>
         <variableId>cbd552be-2989-4d9a-be6d-a4176eabc831</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
