<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Save</name>
   <tag></tag>
   <elementGuidId>6b5af2f4-f8e5-45d4-8eb1-cc59ae9aa4bf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='yui_3_18_1_1_1701704412122_390']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#yui_3_18_1_1_1701704412122_390</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>5cfaa03a-699c-4399-a568-b2fa9800aa0b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>e8c842e4-6783-4849-9925-107fba4742e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
      <webElementGuid>96ca84fd-c5a8-4b77-bfdc-b78c9fde9c08</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-action</name>
      <type>Main</type>
      <value>save</value>
      <webElementGuid>897265a7-88e0-4c5f-8bfb-0980bf12ab28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>yui_3_18_1_1_1701704412122_390</value>
      <webElementGuid>12ea13bd-5ae3-4728-83b1-e308f1be5ff1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

            Save
            


                
            
        </value>
      <webElementGuid>96f60d10-c5dd-47f8-9228-e16bd77c3712</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;yui_3_18_1_1_1701704412122_390&quot;)</value>
      <webElementGuid>35ea693a-9137-4999-9a16-2680a8eecb52</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='yui_3_18_1_1_1701704412122_390']</value>
      <webElementGuid>ae8d824a-c137-4a87-a92e-dac43ff4fdb7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='yui_3_18_1_1_1701704412122_391']/button</value>
      <webElementGuid>8bb70389-6bd2-4368-948b-244fef77a2b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div[3]/button</value>
      <webElementGuid>c1162903-4799-4f6c-bc81-479a1970ea21</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'yui_3_18_1_1_1701704412122_390' and (text() = '

            Save
            


                
            
        ' or . = '

            Save
            


                
            
        ')]</value>
      <webElementGuid>c743d27b-894d-499f-9a76-9e6ac420cc98</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
