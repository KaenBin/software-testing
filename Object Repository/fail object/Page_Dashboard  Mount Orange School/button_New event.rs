<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_New event</name>
   <tag></tag>
   <elementGuidId>c0096ab3-afb0-4dfb-9285-e6ca8b006c24</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='calendar-month-6574779f9475c6574779f5ea633-1']/div/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-primary.float-sm-right.float-right</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>7749d94f-ad23-4039-920c-f361a2ffe5a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary float-sm-right float-right</value>
      <webElementGuid>7c1634d9-09f0-4f8f-8aa1-05626b5cb242</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-context-id</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>aed3367d-2780-46b6-bbe7-a952a4dd6d7c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-action</name>
      <type>Main</type>
      <value>new-event-button</value>
      <webElementGuid>6d19697e-187e-4b45-b044-5dadcef09c71</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            New event
        </value>
      <webElementGuid>52f61e5b-4ef6-418d-9c06-bfbde49869f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;calendar-month-6574779f9475c6574779f5ea633-1&quot;)/div[@class=&quot;header d-flex flex-wrap p-1&quot;]/button[@class=&quot;btn btn-primary float-sm-right float-right&quot;]</value>
      <webElementGuid>002ee868-bb44-4a14-a107-6e83d7674c5e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='calendar-month-6574779f9475c6574779f5ea633-1']/div/button</value>
      <webElementGuid>331a5cfd-19f7-4319-88fc-a59e4b45ae7f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section[3]/div/div/div/div/div/button</value>
      <webElementGuid>5fe7818f-06d4-4591-815f-6a3dc973c547</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
            New event
        ' or . = '
            New event
        ')]</value>
      <webElementGuid>9a240f0b-7d28-409f-98b7-bb5341156b78</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
