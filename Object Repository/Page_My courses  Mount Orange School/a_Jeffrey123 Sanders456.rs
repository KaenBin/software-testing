<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Jeffrey123 Sanders456</name>
   <tag></tag>
   <elementGuidId>d7315de8-1457-4245-9b4a-d9049dac32cb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='action-menu-toggle-0']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#action-menu-toggle-0</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>b47e4c74-641d-49d0-a7f2-a461d4ea22f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>9ce9b299-0742-4f36-8380-fde299b7a187</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>101337ab-9b6d-4072-9e98-65997902eda6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nav-link dropdown-toggle icon-no-margin</value>
      <webElementGuid>3ef79a56-ef7e-4924-9a89-60d758ca4d7d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>action-menu-toggle-0</value>
      <webElementGuid>d859e73f-0bc8-44c2-8642-de7c8cfa3b88</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>User menu</value>
      <webElementGuid>96a298e9-96f3-4779-bb4a-d4a8117f0b12</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>dropdown</value>
      <webElementGuid>47d7214d-1108-44ed-b0d2-827973550f76</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>dbab5d90-455b-4835-a449-3cd41f34cbcd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>d50010d2-588c-43ba-8527-d32737df8731</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>a09a54d0-bbd5-4f3f-8ffd-9d632e77af27</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-controls</name>
      <type>Main</type>
      <value>action-menu-0-menu</value>
      <webElementGuid>7038a3bd-35a4-46cf-bfee-1701461a0f8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                            Jeffrey123 Sanders456
                                
                            
                        </value>
      <webElementGuid>8bd4599d-c3dc-4caf-9683-ac74a5517840</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;action-menu-toggle-0&quot;)</value>
      <webElementGuid>7f70b9ef-467c-4803-b783-af7f7364ce57</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='action-menu-toggle-0']</value>
      <webElementGuid>ccd25989-52be-4246-b940-d0e4e2a04280</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='action-menu-0-menubar']/div/div/a</value>
      <webElementGuid>13554a61-2b5e-473f-aac0-07d2f8a99280</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[7]</value>
      <webElementGuid>520a129c-c8af-4094-8907-bff4192cf981</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div/div/div/a</value>
      <webElementGuid>5fa58609-d601-4d11-9297-f380b03f38ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#' and @id = 'action-menu-toggle-0' and (text() = '
                            
                            Jeffrey123 Sanders456
                                
                            
                        ' or . = '
                            
                            Jeffrey123 Sanders456
                                
                            
                        ')]</value>
      <webElementGuid>2fe1eb9b-2dc7-40ba-8e2c-d26c963bed4f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
