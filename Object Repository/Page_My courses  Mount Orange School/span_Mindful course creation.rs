<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Mindful course creation</name>
   <tag></tag>
   <elementGuidId>ee6aa162-de0d-4021-b238-1924a0b3c16a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='yui_3_18_1_1_1702135076749_32']/a/span[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.multiline</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>31218eff-35c4-466a-9338-2b47432927a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>multiline</value>
      <webElementGuid>7b82d797-1c3a-4bbc-8f96-8ca87b0733ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
             Mindful course creation
        </value>
      <webElementGuid>2a04afba-e57f-4448-831f-31048972c877</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;yui_3_18_1_1_1702135076749_32&quot;)/a[@class=&quot;aalink coursename mr-2 mb-1&quot;]/span[@class=&quot;multiline&quot;]</value>
      <webElementGuid>326bd2a4-7fc0-42cf-a15c-63e6c1d0e429</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='yui_3_18_1_1_1702135076749_32']/a/span[3]</value>
      <webElementGuid>d2d7e057-61d3-4157-9da2-874706ed1953</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[3]</value>
      <webElementGuid>499df6b6-9ea8-408b-8145-d0d22c85b5f6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '
             Mindful course creation
        ' or . = '
             Mindful course creation
        ')]</value>
      <webElementGuid>1e5082f3-b015-4435-8e12-fa8a0c903f00</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
