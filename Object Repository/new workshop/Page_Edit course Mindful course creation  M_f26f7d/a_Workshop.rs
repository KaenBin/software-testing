<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Workshop</name>
   <tag></tag>
   <elementGuidId>18263cfe-82aa-42e7-96a1-b13d5e7c0ec5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='all-6']/div/div[23]/div/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>ee4bebf9-94ad-4bcc-81be-a141c46b116f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>d-flex flex-column justify-content-between flex-fill</value>
      <webElementGuid>36fdedbb-49c1-479f-8281-4c201971a389</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://school.moodledemo.net/course/mod.php?id=69&amp;add=workshop&amp;section=0&amp;sr=0&amp;beforemod=0</value>
      <webElementGuid>3a2761df-937c-4200-9657-f29010666574</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Add a new Workshop</value>
      <webElementGuid>97b6e565-1fc0-4763-aae8-143a712873c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>298a258d-4289-43a7-9368-52dd73be5178</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-action</name>
      <type>Main</type>
      <value>add-chooser-option</value>
      <webElementGuid>b0a2ebae-7557-4441-bfc1-15dfb9e50d32</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                                                    
                                                
                                                Workshop
                                            </value>
      <webElementGuid>6a1eb1f6-31a2-4439-8212-bbb0c8572662</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;all-6&quot;)/div[@class=&quot;optionscontainer d-flex flex-wrap p-1 mw-100 position-relative&quot;]/div[@class=&quot;option border-0 card m-1 bg-white&quot;]/div[@class=&quot;optioninfo card-body d-flex flex-column text-center p-1&quot;]/a[@class=&quot;d-flex flex-column justify-content-between flex-fill&quot;]</value>
      <webElementGuid>fb9e8e34-66ca-427b-bd9e-37a4e9f4c530</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='all-6']/div/div[23]/div/a</value>
      <webElementGuid>004402b3-0948-4ead-ad82-c67451fbe452</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://school.moodledemo.net/course/mod.php?id=69&amp;add=workshop&amp;section=0&amp;sr=0&amp;beforemod=0')]</value>
      <webElementGuid>f69068e0-759c-41de-aae4-4deb67b58035</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[23]/div/a</value>
      <webElementGuid>6286d000-9f9c-45bd-9368-cf3ae759f93d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://school.moodledemo.net/course/mod.php?id=69&amp;add=workshop&amp;section=0&amp;sr=0&amp;beforemod=0' and @title = 'Add a new Workshop' and (text() = '
                                                
                                                    
                                                
                                                Workshop
                                            ' or . = '
                                                
                                                    
                                                
                                                Workshop
                                            ')]</value>
      <webElementGuid>86e78459-43b5-4c41-af4c-aed7b31cc76a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
