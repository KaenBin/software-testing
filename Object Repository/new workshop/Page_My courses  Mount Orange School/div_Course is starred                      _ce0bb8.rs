<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Course is starred                      _ce0bb8</name>
   <tag></tag>
   <elementGuidId>18e81821-175b-4e0c-98ea-3c3c46434708</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='course-info-container-69-4']/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.w-100.text-truncate</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a0ceeaf6-6316-463a-a014-781ec197e1b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>w-100 text-truncate</value>
      <webElementGuid>d1a322b4-f982-495a-9d52-5d5686ccfb42</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    


                        
                            
                                
                                Course is starred
                            
                        
                        
                            Course name
                        

        
             Mindful course creation
        
                        
                    

            
                Course category
            
            
                Mount Orange Community
            
                        
                </value>
      <webElementGuid>a0330396-7432-4fea-8a3d-215e74f36e68</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;course-info-container-69-4&quot;)/div[@class=&quot;d-flex align-items-start&quot;]/div[@class=&quot;w-100 text-truncate&quot;]</value>
      <webElementGuid>d8067ae7-a78d-4417-9d03-4cf98b2e11db</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='course-info-container-69-4']/div/div</value>
      <webElementGuid>517f8261-aa45-458b-9229-0cec2d002d69</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div/div/div/div/div/div/div/div/div</value>
      <webElementGuid>6048d1f9-7950-429d-82cc-4723b0670919</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    


                        
                            
                                
                                Course is starred
                            
                        
                        
                            Course name
                        

        
             Mindful course creation
        
                        
                    

            
                Course category
            
            
                Mount Orange Community
            
                        
                ' or . = '
                    


                        
                            
                                
                                Course is starred
                            
                        
                        
                            Course name
                        

        
             Mindful course creation
        
                        
                    

            
                Course category
            
            
                Mount Orange Community
            
                        
                ')]</value>
      <webElementGuid>d6dec0d5-2632-4363-aee1-e6e36236661f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
