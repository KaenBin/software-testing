<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Mindful course creation</name>
   <tag></tag>
   <elementGuidId>34a7fb16-bf6f-45f2-bc87-b5b945dd797e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='yui_3_18_1_1_1702135076749_32']/a/span[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.multiline</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>392ff128-92db-4511-9a5f-cd3e7b7bf0fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>multiline</value>
      <webElementGuid>fb1a7b7d-9b19-457b-b2bf-5065e39d6f89</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
             Mindful course creation
        </value>
      <webElementGuid>dce608d9-976b-4824-8ebc-4f3aa5713e0f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;yui_3_18_1_1_1702135076749_32&quot;)/a[@class=&quot;aalink coursename mr-2 mb-1&quot;]/span[@class=&quot;multiline&quot;]</value>
      <webElementGuid>446ce9a9-8459-41d1-961d-ac2fe663cb3a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='yui_3_18_1_1_1702135076749_32']/a/span[3]</value>
      <webElementGuid>a2c973bd-2927-4e97-8b58-102ec727b8b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[3]</value>
      <webElementGuid>0001be78-47f3-4e14-8ece-ad547ded996c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '
             Mindful course creation
        ' or . = '
             Mindful course creation
        ')]</value>
      <webElementGuid>f6beb3ce-5f92-4c1b-928b-dc2e885bf5aa</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
