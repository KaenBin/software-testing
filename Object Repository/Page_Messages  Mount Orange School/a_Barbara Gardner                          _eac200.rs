<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Barbara Gardner                          _eac200</name>
   <tag></tag>
   <elementGuidId>40a52fe1-f223-46ba-9a79-31e5340ad816</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.py-0.px-2.d-flex.list-group-item.list-group-item-action.align-items-center</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='view-overview-favourites-target-657570db481b9657570db3cff77']/div[2]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>37442710-bdc6-49c4-a531-76fa202a5f9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>e98f9dcf-ea37-4d08-a0bd-b60f073316e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>py-0 px-2 d-flex list-group-item list-group-item-action align-items-center</value>
      <webElementGuid>68857a10-6974-4c08-9ae1-ed64a27e4389</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-conversation-id</name>
      <type>Main</type>
      <value>3</value>
      <webElementGuid>e0ff27e4-b525-41c4-afa3-d6eff85b6bcf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-user-id</name>
      <type>Main</type>
      <value>56</value>
      <webElementGuid>87f5a174-3050-48a7-801b-9ee1c4fa34f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>806fee43-e5c4-4147-a267-d8d7e9033553</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
        
            
                Barbara Gardner
                
                    
                
                
                    
                
            
            
                    You:
                hello
            
        
        
            
                        09:01

            
            
                
                    
                    There are  unread messages
                

                


                    
                                    
            
        
    </value>
      <webElementGuid>dbd3f725-4204-4933-9b21-93535f3bc0b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;view-overview-favourites-target-657570db481b9657570db3cff77&quot;)/div[@class=&quot;list-group&quot;]/a[@class=&quot;py-0 px-2 d-flex list-group-item list-group-item-action align-items-center&quot;]</value>
      <webElementGuid>06f89c8e-5ab7-4eba-adb3-d43d50183641</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='view-overview-favourites-target-657570db481b9657570db3cff77']/div[2]/a</value>
      <webElementGuid>fd28be25-ad5e-42f4-bffc-be438efc0f55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[13]</value>
      <webElementGuid>f0854a83-9113-40d3-8010-9017250eeb20</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/a</value>
      <webElementGuid>79f4c618-d234-43ad-ac7a-10e61aa9a308</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#' and (text() = '
            
        
            
                Barbara Gardner
                
                    
                
                
                    
                
            
            
                    You:
                hello
            
        
        
            
                        09:01

            
            
                
                    
                    There are  unread messages
                

                


                    
                                    
            
        
    ' or . = '
            
        
            
                Barbara Gardner
                
                    
                
                
                    
                
            
            
                    You:
                hello
            
        
        
            
                        09:01

            
            
                
                    
                    There are  unread messages
                

                


                    
                                    
            
        
    ')]</value>
      <webElementGuid>8fb4f48f-9850-4467-942f-a96fa9ea0369</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
