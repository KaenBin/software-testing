<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Barbara Gardner                          _1f06d2</name>
   <tag></tag>
   <elementGuidId>8765a329-aec9-4b4c-a8fa-c90a453b8421</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='view-overview-favourites-target-6575d450843956575d450781f97']/div[2]/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.py-0.px-2.d-flex.list-group-item.list-group-item-action.align-items-center</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>fff988d0-8e56-4f45-96c0-93f970e3484a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>1c70b6da-91ee-4646-9de2-3346c6ab66f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>py-0 px-2 d-flex list-group-item list-group-item-action align-items-center</value>
      <webElementGuid>cc6b2f23-b6c9-4f71-9e7f-13e02218c367</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-conversation-id</name>
      <type>Main</type>
      <value>3</value>
      <webElementGuid>9a8ca12b-4a44-43ac-9975-4e721739e8c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-user-id</name>
      <type>Main</type>
      <value>56</value>
      <webElementGuid>25df7857-6874-4996-b210-24ac71032cee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>efa8f6ce-b00e-4cb3-8c6a-00ecce28f04b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
        
            
                Barbara Gardner
                
                    
                
                
                    
                
            
            
                Thanks😁I was very pleased with my feedback.🏆
            
        
        
            
                        21/11/19

            
            
                
                    
                    There are  unread messages
                

                


                    
                                    
            
        
    </value>
      <webElementGuid>a3813e58-c45b-4d44-badc-48d17a942a03</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;view-overview-favourites-target-6575d450843956575d450781f97&quot;)/div[@class=&quot;list-group&quot;]/a[@class=&quot;py-0 px-2 d-flex list-group-item list-group-item-action align-items-center&quot;]</value>
      <webElementGuid>caef0d3d-bfbc-49c3-b6e3-f677ba829639</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='view-overview-favourites-target-6575d450843956575d450781f97']/div[2]/a</value>
      <webElementGuid>f0875b29-aa53-4c6c-9595-a359be22a647</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[13]</value>
      <webElementGuid>2dbfd828-b7f9-4d33-aa47-ad9ac7802b54</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/a</value>
      <webElementGuid>f6daca01-1b8e-4703-a943-4deb2547ae8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#' and (text() = '
            
        
            
                Barbara Gardner
                
                    
                
                
                    
                
            
            
                Thanks😁I was very pleased with my feedback.🏆
            
        
        
            
                        21/11/19

            
            
                
                    
                    There are  unread messages
                

                


                    
                                    
            
        
    ' or . = '
            
        
            
                Barbara Gardner
                
                    
                
                
                    
                
            
            
                Thanks😁I was very pleased with my feedback.🏆
            
        
        
            
                        21/11/19

            
            
                
                    
                    There are  unread messages
                

                


                    
                                    
            
        
    ')]</value>
      <webElementGuid>07cfba56-5f66-4c3f-afe1-1c21645160b2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
