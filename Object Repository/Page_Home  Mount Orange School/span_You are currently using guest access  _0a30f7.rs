<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_You are currently using guest access  _0a30f7</name>
   <tag></tag>
   <elementGuidId>c1867413-ff23-4fe7-a69f-d9be383802ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='usernavigation']/div[5]/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.login.pl-2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>c099d547-f7ff-45e5-a438-fd903c29acc4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>login pl-2</value>
      <webElementGuid>550593d1-81c8-4b65-9d2b-c2072dcb7596</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    You are currently using guest access
                                    
                                    Log in
                            </value>
      <webElementGuid>d188a68b-a391-459b-82d6-da35d2330bc8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;usernavigation&quot;)/div[@class=&quot;d-flex align-items-stretch usermenu-container&quot;]/div[@class=&quot;usermenu&quot;]/span[@class=&quot;login pl-2&quot;]</value>
      <webElementGuid>f5488095-dd3b-498c-b4ba-68f92ceaa3e5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='usernavigation']/div[5]/div/span</value>
      <webElementGuid>f2d163ed-ba11-4830-aa3d-050a2f2bfc44</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/span</value>
      <webElementGuid>3830871b-14c0-453a-b051-0dfb53e22b3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '
                                    You are currently using guest access
                                    
                                    Log in
                            ' or . = '
                                    You are currently using guest access
                                    
                                    Log in
                            ')]</value>
      <webElementGuid>d7dcd4ff-6bf4-4da3-bb3b-974af107493e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
