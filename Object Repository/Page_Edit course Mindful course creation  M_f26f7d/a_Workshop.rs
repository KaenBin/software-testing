<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Workshop</name>
   <tag></tag>
   <elementGuidId>e35a73ea-5669-42b8-8f66-1256551f5b36</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='all-6']/div/div[23]/div/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>0bb594dd-df0c-4874-8ec5-1f55efe1ac53</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>d-flex flex-column justify-content-between flex-fill</value>
      <webElementGuid>3ee4385c-0960-415b-9fac-bd2a0466c43c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://school.moodledemo.net/course/mod.php?id=69&amp;add=workshop&amp;section=0&amp;sr=0&amp;beforemod=0</value>
      <webElementGuid>d2735e2d-6757-4c50-a49a-c1c6467bfd3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Add a new Workshop</value>
      <webElementGuid>3782bc45-f249-4e0a-a695-2ad317b6302f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>71ee65db-8029-494e-9c72-0a250c81498e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-action</name>
      <type>Main</type>
      <value>add-chooser-option</value>
      <webElementGuid>a0df0af0-92e8-40be-a81e-6ebf2ff12c4f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                                                    
                                                
                                                Workshop
                                            </value>
      <webElementGuid>7d3349b9-93ef-4d35-a95f-261d194dd9fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;all-6&quot;)/div[@class=&quot;optionscontainer d-flex flex-wrap p-1 mw-100 position-relative&quot;]/div[@class=&quot;option border-0 card m-1 bg-white&quot;]/div[@class=&quot;optioninfo card-body d-flex flex-column text-center p-1&quot;]/a[@class=&quot;d-flex flex-column justify-content-between flex-fill&quot;]</value>
      <webElementGuid>ee00ae7c-97ed-40b6-baf4-461b4e557ec1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='all-6']/div/div[23]/div/a</value>
      <webElementGuid>61236e7e-a115-4f0e-8e5d-ebf7b9216134</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://school.moodledemo.net/course/mod.php?id=69&amp;add=workshop&amp;section=0&amp;sr=0&amp;beforemod=0')]</value>
      <webElementGuid>de5b3824-dd21-4166-bb2b-88796bf0081f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[23]/div/a</value>
      <webElementGuid>5ffa5286-bc7e-4cc8-bbbb-440002d5fd7b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://school.moodledemo.net/course/mod.php?id=69&amp;add=workshop&amp;section=0&amp;sr=0&amp;beforemod=0' and @title = 'Add a new Workshop' and (text() = '
                                                
                                                    
                                                
                                                Workshop
                                            ' or . = '
                                                
                                                    
                                                
                                                Workshop
                                            ')]</value>
      <webElementGuid>8dd2f9ad-bcf4-4b58-ae6d-9b0e4f2f02aa</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
