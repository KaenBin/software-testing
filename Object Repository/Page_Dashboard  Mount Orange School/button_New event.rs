<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_New event</name>
   <tag></tag>
   <elementGuidId>f1d49bf0-f1e7-4c61-a0a4-115357411e47</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='calendar-month-657473ec4c498657473ec227053-1']/div/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-primary.float-sm-right.float-right</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>d026643d-75d3-407f-bd32-cd5adc183f6c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary float-sm-right float-right</value>
      <webElementGuid>70ec1250-4892-426e-828b-3360a272b132</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-context-id</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>ed4445a8-61f7-404d-88d9-7718bdf6cfa2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-action</name>
      <type>Main</type>
      <value>new-event-button</value>
      <webElementGuid>fc6e7dc4-492c-49b5-9bd2-353fef698c1c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            New event
        </value>
      <webElementGuid>c714ed46-7fbf-4b71-8932-14f956c104a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;calendar-month-657473ec4c498657473ec227053-1&quot;)/div[@class=&quot;header d-flex flex-wrap p-1&quot;]/button[@class=&quot;btn btn-primary float-sm-right float-right&quot;]</value>
      <webElementGuid>2d8e31f7-3f73-48d7-bf11-918eee7c8c16</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='calendar-month-657473ec4c498657473ec227053-1']/div/button</value>
      <webElementGuid>1b716fde-bb3f-434e-a6a9-e1b27fe1e4ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section[3]/div/div/div/div/div/button</value>
      <webElementGuid>b3a5cc22-5c18-422d-bfd3-4db9f6799ea7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
            New event
        ' or . = '
            New event
        ')]</value>
      <webElementGuid>fb236d4c-de2c-4c2a-b9a4-0d939cce8456</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
