<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Save</name>
   <tag></tag>
   <elementGuidId>027a57c9-66ee-4707-9436-abbc17a30beb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='yui_3_18_1_1_1702130668887_409']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#yui_3_18_1_1_1702130668887_409</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>e5f9a954-022e-4cb1-b804-3ef924a3340e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>07ac0bfb-35b2-43c9-98f2-1e36d72e31bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
      <webElementGuid>183dd808-4f0b-4e7e-9cec-41f1538bbe55</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-action</name>
      <type>Main</type>
      <value>save</value>
      <webElementGuid>411555c5-8b44-47fe-8560-5aca6f65e452</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>yui_3_18_1_1_1702130668887_409</value>
      <webElementGuid>7e34b97f-f0c1-42d8-b80b-9f74fd80d572</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

            Save
            


                
            
        </value>
      <webElementGuid>53b53663-53b9-4092-9dba-c162bae4f524</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;yui_3_18_1_1_1702130668887_409&quot;)</value>
      <webElementGuid>87de6643-c402-4253-b733-2b076efc5211</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='yui_3_18_1_1_1702130668887_409']</value>
      <webElementGuid>8ff2afb0-19fa-4a7d-b3d7-5258895defdb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='yui_3_18_1_1_1702130668887_410']/button</value>
      <webElementGuid>0f02ffad-44b6-4aaf-b437-c7c1a6d068c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div[3]/button</value>
      <webElementGuid>18087979-5fa4-4e4f-a383-4bc369bc1bc8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'yui_3_18_1_1_1702130668887_409' and (text() = '

            Save
            


                
            
        ' or . = '

            Save
            


                
            
        ')]</value>
      <webElementGuid>f5806624-a899-41c1-8021-0c8b890dbe31</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
