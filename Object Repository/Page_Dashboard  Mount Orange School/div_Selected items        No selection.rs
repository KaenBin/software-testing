<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Selected items        No selection</name>
   <tag></tag>
   <elementGuidId>376b5ba7-e440-4b30-a132-6139b1de239b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='fitem_id_groupcourseid']/div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#fitem_id_groupcourseid > div.col-md-9.form-inline.align-items-start.felement</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a54d9238-2d4c-4196-8135-9c2c915a9450</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-9 form-inline align-items-start felement</value>
      <webElementGuid>067e520d-d9d4-4080-819f-06626b656b05</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-fieldtype</name>
      <type>Main</type>
      <value>autocomplete</value>
      <webElementGuid>fa0dd8e6-efda-4a28-ae37-b3dcec84c032</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
        
        
            
        
    Selected items:



        No selection


    
    ▼


        
</value>
      <webElementGuid>78429bde-2750-4cd3-8b9d-a678e84495bf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;fitem_id_groupcourseid&quot;)/div[@class=&quot;col-md-9 form-inline align-items-start felement&quot;]</value>
      <webElementGuid>d0cf7447-86b8-4bb4-a17f-e306c9a4ef49</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='fitem_id_groupcourseid']/div[2]</value>
      <webElementGuid>ec70bd5f-9ee3-4370-be60-4eccac0b6611</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[5]/div[2]</value>
      <webElementGuid>61401663-0011-48e3-88ac-5b902d7f933b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        
        
        
            
        
    Selected items:



        No selection


    
    ▼


        
' or . = '
        
        
        
            
        
    Selected items:



        No selection


    
    ▼


        
')]</value>
      <webElementGuid>cd2f98c3-cac5-4dc4-bc86-c04f72d76504</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
