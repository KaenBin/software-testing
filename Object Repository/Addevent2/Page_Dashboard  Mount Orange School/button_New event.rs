<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_New event</name>
   <tag></tag>
   <elementGuidId>77951eb6-8227-4da1-a315-4d4843c795cf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='calendar-month-656df370d1071656df370c096b3-1']/div/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-primary.float-sm-right.float-right</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>139a9d31-f038-4df3-8d62-5947e6107963</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary float-sm-right float-right</value>
      <webElementGuid>7b7c161f-05d5-4e33-8455-df485328b27a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-context-id</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>47d3b2e2-80b5-41bc-9a31-6cd87678bb2e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-action</name>
      <type>Main</type>
      <value>new-event-button</value>
      <webElementGuid>bd64e794-f799-4a46-b7ef-b94eb75052d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            New event
        </value>
      <webElementGuid>23361a27-ea02-4b8c-a42b-37f4e1694a7f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;calendar-month-656df370d1071656df370c096b3-1&quot;)/div[@class=&quot;header d-flex flex-wrap p-1&quot;]/button[@class=&quot;btn btn-primary float-sm-right float-right&quot;]</value>
      <webElementGuid>6f10ba55-fbf6-4a99-ba5f-f4df1e420fdd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='calendar-month-656df370d1071656df370c096b3-1']/div/button</value>
      <webElementGuid>bdee9c1c-4a3b-4fc6-9e24-574a7ec4b06d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/div/div/div/div/div/button</value>
      <webElementGuid>25b3d423-4662-4903-a537-adf407095183</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
            New event
        ' or . = '
            New event
        ')]</value>
      <webElementGuid>217ff4d5-09d4-4f51-a796-87311fcee024</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
