<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Without duration                       _effa9e</name>
   <tag></tag>
   <elementGuidId>a320f6b7-fd17-4374-b8f7-17717fd1d10e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='fgroup_id_durationgroup']/div[2]/fieldset/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>fieldset.w-100.m-0.p-0.border-0 > div.d-flex.flex-wrap.align-items-center</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>05649ecd-293f-4961-b45e-40ab82b48099</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>d-flex flex-wrap align-items-center</value>
      <webElementGuid>4c0ecdc9-e4e3-4cb3-99c8-2417b5f6d96e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                


    Without duration



    

                
                


    Until



    

                
                
    
        
            
            
        
            Day
        
    
    
        1
        2
        3
        4
        5
        6
        7
        8
        9
        10
        11
        12
        13
        14
        15
        16
        17
        18
        19
        20
        21
        22
        23
        24
        25
        26
        27
        28
        29
        30
        31
    
    
    
        
    

             
            
        
            Month
        
    
    
        January
        February
        March
        April
        May
        June
        July
        August
        September
        October
        November
        December
    
    
    
        
    

             
            
        
            Year
        
    
    
        1900
        1901
        1902
        1903
        1904
        1905
        1906
        1907
        1908
        1909
        1910
        1911
        1912
        1913
        1914
        1915
        1916
        1917
        1918
        1919
        1920
        1921
        1922
        1923
        1924
        1925
        1926
        1927
        1928
        1929
        1930
        1931
        1932
        1933
        1934
        1935
        1936
        1937
        1938
        1939
        1940
        1941
        1942
        1943
        1944
        1945
        1946
        1947
        1948
        1949
        1950
        1951
        1952
        1953
        1954
        1955
        1956
        1957
        1958
        1959
        1960
        1961
        1962
        1963
        1964
        1965
        1966
        1967
        1968
        1969
        1970
        1971
        1972
        1973
        1974
        1975
        1976
        1977
        1978
        1979
        1980
        1981
        1982
        1983
        1984
        1985
        1986
        1987
        1988
        1989
        1990
        1991
        1992
        1993
        1994
        1995
        1996
        1997
        1998
        1999
        2000
        2001
        2002
        2003
        2004
        2005
        2006
        2007
        2008
        2009
        2010
        2011
        2012
        2013
        2014
        2015
        2016
        2017
        2018
        2019
        2020
        2021
        2022
        2023
        2024
        2025
        2026
        2027
        2028
        2029
        2030
        2031
        2032
        2033
        2034
        2035
        2036
        2037
        2038
        2039
        2040
        2041
        2042
        2043
        2044
        2045
        2046
        2047
        2048
        2049
        2050
    
    
    
        
    

             
            
        
            Hour
        
    
    
        00
        01
        02
        03
        04
        05
        06
        07
        08
        09
        10
        11
        12
        13
        14
        15
        16
        17
        18
        19
        20
        21
        22
        23
    
    
    
        
    

             
            
        
            Minute
        
    
    
        00
        01
        02
        03
        04
        05
        06
        07
        08
        09
        10
        11
        12
        13
        14
        15
        16
        17
        18
        19
        20
        21
        22
        23
        24
        25
        26
        27
        28
        29
        30
        31
        32
        33
        34
        35
        36
        37
        38
        39
        40
        41
        42
        43
        44
        45
        46
        47
        48
        49
        50
        51
        52
        53
        54
        55
        56
        57
        58
        59
    
    
    
        
    

             
            
        
    
    
        
    

                
                


    Duration in minutes



    

                
                
        
            Duration in minutes
        
    
    
    
    
        
    

            </value>
      <webElementGuid>723d55c5-c0c7-419c-b655-49d2d37b9789</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;fgroup_id_durationgroup&quot;)/div[@class=&quot;col-md-9 form-inline align-items-start felement&quot;]/fieldset[@class=&quot;w-100 m-0 p-0 border-0&quot;]/div[@class=&quot;d-flex flex-wrap align-items-center&quot;]</value>
      <webElementGuid>3948bf3e-058e-4670-9002-168f1a90c261</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='fgroup_id_durationgroup']/div[2]/fieldset/div</value>
      <webElementGuid>63124ba1-bcb1-4683-953b-e733f5c7161f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/fieldset/div</value>
      <webElementGuid>202eb137-b38d-4ccb-8914-882d93ecc1bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                
                


    Without duration



    

                
                


    Until



    

                
                
    
        
            
            
        
            Day
        
    
    
        1
        2
        3
        4
        5
        6
        7
        8
        9
        10
        11
        12
        13
        14
        15
        16
        17
        18
        19
        20
        21
        22
        23
        24
        25
        26
        27
        28
        29
        30
        31
    
    
    
        
    

             
            
        
            Month
        
    
    
        January
        February
        March
        April
        May
        June
        July
        August
        September
        October
        November
        December
    
    
    
        
    

             
            
        
            Year
        
    
    
        1900
        1901
        1902
        1903
        1904
        1905
        1906
        1907
        1908
        1909
        1910
        1911
        1912
        1913
        1914
        1915
        1916
        1917
        1918
        1919
        1920
        1921
        1922
        1923
        1924
        1925
        1926
        1927
        1928
        1929
        1930
        1931
        1932
        1933
        1934
        1935
        1936
        1937
        1938
        1939
        1940
        1941
        1942
        1943
        1944
        1945
        1946
        1947
        1948
        1949
        1950
        1951
        1952
        1953
        1954
        1955
        1956
        1957
        1958
        1959
        1960
        1961
        1962
        1963
        1964
        1965
        1966
        1967
        1968
        1969
        1970
        1971
        1972
        1973
        1974
        1975
        1976
        1977
        1978
        1979
        1980
        1981
        1982
        1983
        1984
        1985
        1986
        1987
        1988
        1989
        1990
        1991
        1992
        1993
        1994
        1995
        1996
        1997
        1998
        1999
        2000
        2001
        2002
        2003
        2004
        2005
        2006
        2007
        2008
        2009
        2010
        2011
        2012
        2013
        2014
        2015
        2016
        2017
        2018
        2019
        2020
        2021
        2022
        2023
        2024
        2025
        2026
        2027
        2028
        2029
        2030
        2031
        2032
        2033
        2034
        2035
        2036
        2037
        2038
        2039
        2040
        2041
        2042
        2043
        2044
        2045
        2046
        2047
        2048
        2049
        2050
    
    
    
        
    

             
            
        
            Hour
        
    
    
        00
        01
        02
        03
        04
        05
        06
        07
        08
        09
        10
        11
        12
        13
        14
        15
        16
        17
        18
        19
        20
        21
        22
        23
    
    
    
        
    

             
            
        
            Minute
        
    
    
        00
        01
        02
        03
        04
        05
        06
        07
        08
        09
        10
        11
        12
        13
        14
        15
        16
        17
        18
        19
        20
        21
        22
        23
        24
        25
        26
        27
        28
        29
        30
        31
        32
        33
        34
        35
        36
        37
        38
        39
        40
        41
        42
        43
        44
        45
        46
        47
        48
        49
        50
        51
        52
        53
        54
        55
        56
        57
        58
        59
    
    
    
        
    

             
            
        
    
    
        
    

                
                


    Duration in minutes



    

                
                
        
            Duration in minutes
        
    
    
    
    
        
    

            ' or . = '
                
                


    Without duration



    

                
                


    Until



    

                
                
    
        
            
            
        
            Day
        
    
    
        1
        2
        3
        4
        5
        6
        7
        8
        9
        10
        11
        12
        13
        14
        15
        16
        17
        18
        19
        20
        21
        22
        23
        24
        25
        26
        27
        28
        29
        30
        31
    
    
    
        
    

             
            
        
            Month
        
    
    
        January
        February
        March
        April
        May
        June
        July
        August
        September
        October
        November
        December
    
    
    
        
    

             
            
        
            Year
        
    
    
        1900
        1901
        1902
        1903
        1904
        1905
        1906
        1907
        1908
        1909
        1910
        1911
        1912
        1913
        1914
        1915
        1916
        1917
        1918
        1919
        1920
        1921
        1922
        1923
        1924
        1925
        1926
        1927
        1928
        1929
        1930
        1931
        1932
        1933
        1934
        1935
        1936
        1937
        1938
        1939
        1940
        1941
        1942
        1943
        1944
        1945
        1946
        1947
        1948
        1949
        1950
        1951
        1952
        1953
        1954
        1955
        1956
        1957
        1958
        1959
        1960
        1961
        1962
        1963
        1964
        1965
        1966
        1967
        1968
        1969
        1970
        1971
        1972
        1973
        1974
        1975
        1976
        1977
        1978
        1979
        1980
        1981
        1982
        1983
        1984
        1985
        1986
        1987
        1988
        1989
        1990
        1991
        1992
        1993
        1994
        1995
        1996
        1997
        1998
        1999
        2000
        2001
        2002
        2003
        2004
        2005
        2006
        2007
        2008
        2009
        2010
        2011
        2012
        2013
        2014
        2015
        2016
        2017
        2018
        2019
        2020
        2021
        2022
        2023
        2024
        2025
        2026
        2027
        2028
        2029
        2030
        2031
        2032
        2033
        2034
        2035
        2036
        2037
        2038
        2039
        2040
        2041
        2042
        2043
        2044
        2045
        2046
        2047
        2048
        2049
        2050
    
    
    
        
    

             
            
        
            Hour
        
    
    
        00
        01
        02
        03
        04
        05
        06
        07
        08
        09
        10
        11
        12
        13
        14
        15
        16
        17
        18
        19
        20
        21
        22
        23
    
    
    
        
    

             
            
        
            Minute
        
    
    
        00
        01
        02
        03
        04
        05
        06
        07
        08
        09
        10
        11
        12
        13
        14
        15
        16
        17
        18
        19
        20
        21
        22
        23
        24
        25
        26
        27
        28
        29
        30
        31
        32
        33
        34
        35
        36
        37
        38
        39
        40
        41
        42
        43
        44
        45
        46
        47
        48
        49
        50
        51
        52
        53
        54
        55
        56
        57
        58
        59
    
    
    
        
    

             
            
        
    
    
        
    

                
                


    Duration in minutes



    

                
                
        
            Duration in minutes
        
    
    
    
    
        
    

            ')]</value>
      <webElementGuid>dbd44111-308a-41f6-a556-b3f0e99ab342</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
