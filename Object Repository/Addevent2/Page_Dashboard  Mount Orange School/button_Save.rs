<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Save</name>
   <tag></tag>
   <elementGuidId>d8785d6d-62b2-42e3-b49b-bf551af4b0a1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='yui_3_18_1_1_1701704561993_524']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#yui_3_18_1_1_1701704561993_524</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>ad1ebf3e-2087-43d2-89cd-c3bfab5aec47</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>583e43c2-91c9-4bf2-993e-4b59ecfb452b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
      <webElementGuid>10308a5c-89eb-42f5-9627-2b052c6c0ab9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-action</name>
      <type>Main</type>
      <value>save</value>
      <webElementGuid>5ddb6962-0b87-4a28-a66b-9d4b6d6396e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>yui_3_18_1_1_1701704561993_524</value>
      <webElementGuid>19d3680a-d659-4a35-9552-8f6282d45265</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

            Save
            


                
            
        </value>
      <webElementGuid>d55644aa-1ca5-4347-8edb-5e7ce4963e26</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;yui_3_18_1_1_1701704561993_524&quot;)</value>
      <webElementGuid>7cab2083-eeaa-4eeb-9c1f-b98cf0b336a0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='yui_3_18_1_1_1701704561993_524']</value>
      <webElementGuid>fbe3276f-8d8a-46b0-a6aa-79eb7a572883</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='yui_3_18_1_1_1701704561993_525']/button</value>
      <webElementGuid>837c43f3-27f9-4674-86f6-6a3861345b10</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div[3]/button</value>
      <webElementGuid>17e0b8bf-e672-477a-8376-b7257a358fd6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'yui_3_18_1_1_1701704561993_524' and (text() = '

            Save
            


                
            
        ' or . = '

            Save
            


                
            
        ')]</value>
      <webElementGuid>ef826497-c25c-403a-8928-34f965dfe5d6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
