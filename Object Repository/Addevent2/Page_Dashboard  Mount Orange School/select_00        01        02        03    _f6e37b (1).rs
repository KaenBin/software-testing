<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_00        01        02        03    _f6e37b (1)</name>
   <tag></tag>
   <elementGuidId>0d14fc70-3875-4223-94d3-2b9e18671630</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='id_timestart_hour']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#id_timestart_hour</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>75abf6f7-7b8e-4115-b04e-afeec7748571</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>custom-select
                   
                   </value>
      <webElementGuid>94fd7363-30ff-4b05-9b97-a4ddf5268129</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>timestart[hour]</value>
      <webElementGuid>25b32ce2-23c2-45d9-a1e7-e97ff7387fd1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>id_timestart_hour</value>
      <webElementGuid>816cd032-9540-447f-8c23-ef0dcc52cf4b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-initial-value</name>
      <type>Main</type>
      <value>16</value>
      <webElementGuid>c25a890e-f208-4563-906d-20049d00e634</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        00
        01
        02
        03
        04
        05
        06
        07
        08
        09
        10
        11
        12
        13
        14
        15
        16
        17
        18
        19
        20
        21
        22
        23
    </value>
      <webElementGuid>8ba82abd-dad7-4d9b-aeb0-34f0d8d04ae4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;id_timestart_hour&quot;)</value>
      <webElementGuid>25327516-c872-40d4-a7a1-57612fc8b0bf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='id_timestart_hour']</value>
      <webElementGuid>d5fc7113-0106-4449-bb42-7991cec303cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='yui_3_18_1_1_1701704561993_487']/select</value>
      <webElementGuid>d07e6996-bb13-4040-a8d5-d253ea837a87</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/span/select</value>
      <webElementGuid>5c916cc1-5416-4d41-81b3-0d029197a9a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'timestart[hour]' and @id = 'id_timestart_hour' and (text() = '
        00
        01
        02
        03
        04
        05
        06
        07
        08
        09
        10
        11
        12
        13
        14
        15
        16
        17
        18
        19
        20
        21
        22
        23
    ' or . = '
        00
        01
        02
        03
        04
        05
        06
        07
        08
        09
        10
        11
        12
        13
        14
        15
        16
        17
        18
        19
        20
        21
        22
        23
    ')]</value>
      <webElementGuid>6e938df9-6d40-4df1-8d39-74938ea47bd1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
