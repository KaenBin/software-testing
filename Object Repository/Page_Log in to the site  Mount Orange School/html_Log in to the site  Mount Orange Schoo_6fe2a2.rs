<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>html_Log in to the site  Mount Orange Schoo_6fe2a2</name>
   <tag></tag>
   <elementGuidId>6301f3fb-dc70-4480-b67d-b017c455998c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//html</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>html.yui3-js-enabled</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>html</value>
      <webElementGuid>6e7eacdf-52bd-4c2a-a98e-2aabf34fc66e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>dir</name>
      <type>Main</type>
      <value>ltr</value>
      <webElementGuid>3f5e9bee-197e-426c-b507-4887e2a93749</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>lang</name>
      <type>Main</type>
      <value>en-us</value>
      <webElementGuid>47016c30-0002-432c-96ba-74fc91fc9e6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xml:lang</name>
      <type>Main</type>
      <value>en-us</value>
      <webElementGuid>559c32a4-6b6e-4c3b-9b87-864cbe40aab9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>yui3-js-enabled</value>
      <webElementGuid>1e09777c-e945-4330-814a-88c8f17400cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    Log in to the site | Mount Orange School
    
    

/** Required in order to fix style inclusion problems in IE with YUI **/

//&lt;![CDATA[
var M = {}; M.yui = {};
M.pageloadstarttime = new Date();
M.cfg = {&quot;wwwroot&quot;:&quot;https:\/\/school.moodledemo.net&quot;,&quot;homeurl&quot;:{},&quot;sesskey&quot;:&quot;h7aTgK4p2a&quot;,&quot;sessiontimeout&quot;:&quot;7200&quot;,&quot;sessiontimeoutwarning&quot;:1200,&quot;themerev&quot;:&quot;1702130560&quot;,&quot;slasharguments&quot;:1,&quot;theme&quot;:&quot;boost&quot;,&quot;iconsystemmodule&quot;:&quot;core\/icon_system_fontawesome&quot;,&quot;jsrev&quot;:&quot;1702130560&quot;,&quot;admin&quot;:&quot;admin&quot;,&quot;svgicons&quot;:true,&quot;usertimezone&quot;:&quot;UTC+1&quot;,&quot;courseId&quot;:1,&quot;courseContextId&quot;:2,&quot;contextid&quot;:1,&quot;contextInstanceId&quot;:0,&quot;langrev&quot;:1702134037,&quot;templaterev&quot;:&quot;1702130560&quot;};var yui1ConfigFn = function(me) {if(/-skin|reset|fonts|grids|base/.test(me.name)){me.type='css';me.path=me.path.replace(/\.js/,'.css');me.path=me.path.replace(/\/yui2-skin/,'/assets/skins/sam/yui2-skin')}};
var yui2ConfigFn = function(me) {var parts=me.name.replace(/^moodle-/,'').split('-'),component=parts.shift(),module=parts[0],min='-min';if(/-(skin|core)$/.test(me.name)){parts.pop();me.type='css';min=''}
if(module){var filename=parts.join('-');me.path=component+'/'+module+'/'+filename+min+'.'+me.type}else{me.path=component+'/'+component+'.'+me.type}};
YUI_config = {&quot;debug&quot;:false,&quot;base&quot;:&quot;https:\/\/school.moodledemo.net\/lib\/yuilib\/3.18.1\/&quot;,&quot;comboBase&quot;:&quot;https:\/\/school.moodledemo.net\/theme\/yui_combo.php?&quot;,&quot;combine&quot;:true,&quot;filter&quot;:null,&quot;insertBefore&quot;:&quot;firstthemesheet&quot;,&quot;groups&quot;:{&quot;yui2&quot;:{&quot;base&quot;:&quot;https:\/\/school.moodledemo.net\/lib\/yuilib\/2in3\/2.9.0\/build\/&quot;,&quot;comboBase&quot;:&quot;https:\/\/school.moodledemo.net\/theme\/yui_combo.php?&quot;,&quot;combine&quot;:true,&quot;ext&quot;:false,&quot;root&quot;:&quot;2in3\/2.9.0\/build\/&quot;,&quot;patterns&quot;:{&quot;yui2-&quot;:{&quot;group&quot;:&quot;yui2&quot;,&quot;configFn&quot;:yui1ConfigFn}}},&quot;moodle&quot;:{&quot;name&quot;:&quot;moodle&quot;,&quot;base&quot;:&quot;https:\/\/school.moodledemo.net\/theme\/yui_combo.php?m\/1702130560\/&quot;,&quot;combine&quot;:true,&quot;comboBase&quot;:&quot;https:\/\/school.moodledemo.net\/theme\/yui_combo.php?&quot;,&quot;ext&quot;:false,&quot;root&quot;:&quot;m\/1702130560\/&quot;,&quot;patterns&quot;:{&quot;moodle-&quot;:{&quot;group&quot;:&quot;moodle&quot;,&quot;configFn&quot;:yui2ConfigFn}},&quot;filter&quot;:null,&quot;modules&quot;:{&quot;moodle-core-formchangechecker&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;event-focus&quot;,&quot;moodle-core-event&quot;]},&quot;moodle-core-blocks&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;io&quot;,&quot;dom&quot;,&quot;dd&quot;,&quot;dd-scroll&quot;,&quot;moodle-core-dragdrop&quot;,&quot;moodle-core-notification&quot;]},&quot;moodle-core-maintenancemodetimer&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;]},&quot;moodle-core-event&quot;:{&quot;requires&quot;:[&quot;event-custom&quot;]},&quot;moodle-core-dragdrop&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;io&quot;,&quot;dom&quot;,&quot;dd&quot;,&quot;event-key&quot;,&quot;event-focus&quot;,&quot;moodle-core-notification&quot;]},&quot;moodle-core-lockscroll&quot;:{&quot;requires&quot;:[&quot;plugin&quot;,&quot;base-build&quot;]},&quot;moodle-core-handlebars&quot;:{&quot;condition&quot;:{&quot;trigger&quot;:&quot;handlebars&quot;,&quot;when&quot;:&quot;after&quot;}},&quot;moodle-core-chooserdialogue&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;panel&quot;,&quot;moodle-core-notification&quot;]},&quot;moodle-core-notification&quot;:{&quot;requires&quot;:[&quot;moodle-core-notification-dialogue&quot;,&quot;moodle-core-notification-alert&quot;,&quot;moodle-core-notification-confirm&quot;,&quot;moodle-core-notification-exception&quot;,&quot;moodle-core-notification-ajaxexception&quot;]},&quot;moodle-core-notification-dialogue&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;panel&quot;,&quot;escape&quot;,&quot;event-key&quot;,&quot;dd-plugin&quot;,&quot;moodle-core-widget-focusafterclose&quot;,&quot;moodle-core-lockscroll&quot;]},&quot;moodle-core-notification-alert&quot;:{&quot;requires&quot;:[&quot;moodle-core-notification-dialogue&quot;]},&quot;moodle-core-notification-confirm&quot;:{&quot;requires&quot;:[&quot;moodle-core-notification-dialogue&quot;]},&quot;moodle-core-notification-exception&quot;:{&quot;requires&quot;:[&quot;moodle-core-notification-dialogue&quot;]},&quot;moodle-core-notification-ajaxexception&quot;:{&quot;requires&quot;:[&quot;moodle-core-notification-dialogue&quot;]},&quot;moodle-core-actionmenu&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;event&quot;,&quot;node-event-simulate&quot;]},&quot;moodle-core_availability-form&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;event-delegate&quot;,&quot;panel&quot;,&quot;moodle-core-notification-dialogue&quot;,&quot;json&quot;]},&quot;moodle-backup-backupselectall&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;event&quot;,&quot;node-event-simulate&quot;,&quot;anim&quot;]},&quot;moodle-course-util&quot;:{&quot;requires&quot;:[&quot;node&quot;],&quot;use&quot;:[&quot;moodle-course-util-base&quot;],&quot;submodules&quot;:{&quot;moodle-course-util-base&quot;:{},&quot;moodle-course-util-section&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;moodle-course-util-base&quot;]},&quot;moodle-course-util-cm&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;moodle-course-util-base&quot;]}}},&quot;moodle-course-categoryexpander&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;event-key&quot;]},&quot;moodle-course-management&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;io-base&quot;,&quot;moodle-core-notification-exception&quot;,&quot;json-parse&quot;,&quot;dd-constrain&quot;,&quot;dd-proxy&quot;,&quot;dd-drop&quot;,&quot;dd-delegate&quot;,&quot;node-event-delegate&quot;]},&quot;moodle-course-dragdrop&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;io&quot;,&quot;dom&quot;,&quot;dd&quot;,&quot;dd-scroll&quot;,&quot;moodle-core-dragdrop&quot;,&quot;moodle-core-notification&quot;,&quot;moodle-course-coursebase&quot;,&quot;moodle-course-util&quot;]},&quot;moodle-form-dateselector&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;overlay&quot;,&quot;calendar&quot;]},&quot;moodle-form-shortforms&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;base&quot;,&quot;selector-css3&quot;,&quot;moodle-core-event&quot;]},&quot;moodle-question-preview&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;dom&quot;,&quot;event-delegate&quot;,&quot;event-key&quot;,&quot;core_question_engine&quot;]},&quot;moodle-question-searchform&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;]},&quot;moodle-question-chooser&quot;:{&quot;requires&quot;:[&quot;moodle-core-chooserdialogue&quot;]},&quot;moodle-availability_completion-form&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;moodle-core_availability-form&quot;]},&quot;moodle-availability_date-form&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;io&quot;,&quot;moodle-core_availability-form&quot;]},&quot;moodle-availability_grade-form&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;moodle-core_availability-form&quot;]},&quot;moodle-availability_group-form&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;moodle-core_availability-form&quot;]},&quot;moodle-availability_grouping-form&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;moodle-core_availability-form&quot;]},&quot;moodle-availability_profile-form&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;moodle-core_availability-form&quot;]},&quot;moodle-mod_assign-history&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;transition&quot;]},&quot;moodle-mod_quiz-modform&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;]},&quot;moodle-mod_quiz-util&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;moodle-core-actionmenu&quot;],&quot;use&quot;:[&quot;moodle-mod_quiz-util-base&quot;],&quot;submodules&quot;:{&quot;moodle-mod_quiz-util-base&quot;:{},&quot;moodle-mod_quiz-util-slot&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;moodle-mod_quiz-util-base&quot;]},&quot;moodle-mod_quiz-util-page&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;moodle-mod_quiz-util-base&quot;]}}},&quot;moodle-mod_quiz-questionchooser&quot;:{&quot;requires&quot;:[&quot;moodle-core-chooserdialogue&quot;,&quot;moodle-mod_quiz-util&quot;,&quot;querystring-parse&quot;]},&quot;moodle-mod_quiz-dragdrop&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;io&quot;,&quot;dom&quot;,&quot;dd&quot;,&quot;dd-scroll&quot;,&quot;moodle-core-dragdrop&quot;,&quot;moodle-core-notification&quot;,&quot;moodle-mod_quiz-quizbase&quot;,&quot;moodle-mod_quiz-util-base&quot;,&quot;moodle-mod_quiz-util-page&quot;,&quot;moodle-mod_quiz-util-slot&quot;,&quot;moodle-course-util&quot;]},&quot;moodle-mod_quiz-quizbase&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;]},&quot;moodle-mod_quiz-toolboxes&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;event-key&quot;,&quot;io&quot;,&quot;moodle-mod_quiz-quizbase&quot;,&quot;moodle-mod_quiz-util-slot&quot;,&quot;moodle-core-notification-ajaxexception&quot;]},&quot;moodle-mod_quiz-autosave&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;event-valuechange&quot;,&quot;node-event-delegate&quot;,&quot;io-form&quot;]},&quot;moodle-message_airnotifier-toolboxes&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;io&quot;]},&quot;moodle-filter_glossary-autolinker&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;io-base&quot;,&quot;json-parse&quot;,&quot;event-delegate&quot;,&quot;overlay&quot;,&quot;moodle-core-event&quot;,&quot;moodle-core-notification-alert&quot;,&quot;moodle-core-notification-exception&quot;,&quot;moodle-core-notification-ajaxexception&quot;]},&quot;moodle-editor_atto-rangy&quot;:{&quot;requires&quot;:[]},&quot;moodle-editor_atto-editor&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;transition&quot;,&quot;io&quot;,&quot;overlay&quot;,&quot;escape&quot;,&quot;event&quot;,&quot;event-simulate&quot;,&quot;event-custom&quot;,&quot;node-event-html5&quot;,&quot;node-event-simulate&quot;,&quot;yui-throttle&quot;,&quot;moodle-core-notification-dialogue&quot;,&quot;moodle-editor_atto-rangy&quot;,&quot;handlebars&quot;,&quot;timers&quot;,&quot;querystring-stringify&quot;]},&quot;moodle-editor_atto-plugin&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;base&quot;,&quot;escape&quot;,&quot;event&quot;,&quot;event-outside&quot;,&quot;handlebars&quot;,&quot;event-custom&quot;,&quot;timers&quot;,&quot;moodle-editor_atto-menu&quot;]},&quot;moodle-editor_atto-menu&quot;:{&quot;requires&quot;:[&quot;moodle-core-notification-dialogue&quot;,&quot;node&quot;,&quot;event&quot;,&quot;event-custom&quot;]},&quot;moodle-report_eventlist-eventfilter&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;event&quot;,&quot;node&quot;,&quot;node-event-delegate&quot;,&quot;datatable&quot;,&quot;autocomplete&quot;,&quot;autocomplete-filters&quot;]},&quot;moodle-report_loglive-fetchlogs&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;event&quot;,&quot;node&quot;,&quot;io&quot;,&quot;node-event-delegate&quot;]},&quot;moodle-gradereport_history-userselector&quot;:{&quot;requires&quot;:[&quot;escape&quot;,&quot;event-delegate&quot;,&quot;event-key&quot;,&quot;handlebars&quot;,&quot;io-base&quot;,&quot;json-parse&quot;,&quot;moodle-core-notification-dialogue&quot;]},&quot;moodle-qbank_editquestion-chooser&quot;:{&quot;requires&quot;:[&quot;moodle-core-chooserdialogue&quot;]},&quot;moodle-tool_lp-dragdrop-reorder&quot;:{&quot;requires&quot;:[&quot;moodle-core-dragdrop&quot;]},&quot;moodle-assignfeedback_editpdf-editor&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;event&quot;,&quot;node&quot;,&quot;io&quot;,&quot;graphics&quot;,&quot;json&quot;,&quot;event-move&quot;,&quot;event-resize&quot;,&quot;transition&quot;,&quot;querystring-stringify-simple&quot;,&quot;moodle-core-notification-dialog&quot;,&quot;moodle-core-notification-alert&quot;,&quot;moodle-core-notification-warning&quot;,&quot;moodle-core-notification-exception&quot;,&quot;moodle-core-notification-ajaxexception&quot;]},&quot;moodle-atto_accessibilitychecker-button&quot;:{&quot;requires&quot;:[&quot;color-base&quot;,&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_accessibilityhelper-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_align-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_bold-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_charmap-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_clear-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_collapse-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_emojipicker-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_emoticon-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_equation-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;,&quot;moodle-core-event&quot;,&quot;io&quot;,&quot;event-valuechange&quot;,&quot;tabview&quot;,&quot;array-extras&quot;]},&quot;moodle-atto_h5p-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_html-beautify&quot;:{},&quot;moodle-atto_html-codemirror&quot;:{&quot;requires&quot;:[&quot;moodle-atto_html-codemirror-skin&quot;]},&quot;moodle-atto_html-button&quot;:{&quot;requires&quot;:[&quot;promise&quot;,&quot;moodle-editor_atto-plugin&quot;,&quot;moodle-atto_html-beautify&quot;,&quot;moodle-atto_html-codemirror&quot;,&quot;event-valuechange&quot;]},&quot;moodle-atto_image-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_indent-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_italic-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_link-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_managefiles-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_managefiles-usedfiles&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;escape&quot;]},&quot;moodle-atto_media-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;,&quot;moodle-form-shortforms&quot;]},&quot;moodle-atto_noautolink-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_orderedlist-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_recordrtc-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;,&quot;moodle-atto_recordrtc-recording&quot;]},&quot;moodle-atto_recordrtc-recording&quot;:{&quot;requires&quot;:[&quot;moodle-atto_recordrtc-button&quot;]},&quot;moodle-atto_rtl-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_strike-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_subscript-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_superscript-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_table-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;,&quot;moodle-editor_atto-menu&quot;,&quot;event&quot;,&quot;event-valuechange&quot;]},&quot;moodle-atto_title-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_underline-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_undo-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_unorderedlist-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]}}},&quot;gallery&quot;:{&quot;name&quot;:&quot;gallery&quot;,&quot;base&quot;:&quot;https:\/\/school.moodledemo.net\/lib\/yuilib\/gallery\/&quot;,&quot;combine&quot;:true,&quot;comboBase&quot;:&quot;https:\/\/school.moodledemo.net\/theme\/yui_combo.php?&quot;,&quot;ext&quot;:false,&quot;root&quot;:&quot;gallery\/1702130560\/&quot;,&quot;patterns&quot;:{&quot;gallery-&quot;:{&quot;group&quot;:&quot;gallery&quot;}}}},&quot;modules&quot;:{&quot;core_filepicker&quot;:{&quot;name&quot;:&quot;core_filepicker&quot;,&quot;fullpath&quot;:&quot;https:\/\/school.moodledemo.net\/lib\/javascript.php\/1702130560\/repository\/filepicker.js&quot;,&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;node-event-simulate&quot;,&quot;json&quot;,&quot;async-queue&quot;,&quot;io-base&quot;,&quot;io-upload-iframe&quot;,&quot;io-form&quot;,&quot;yui2-treeview&quot;,&quot;panel&quot;,&quot;cookie&quot;,&quot;datatable&quot;,&quot;datatable-sort&quot;,&quot;resize-plugin&quot;,&quot;dd-plugin&quot;,&quot;escape&quot;,&quot;moodle-core_filepicker&quot;,&quot;moodle-core-notification-dialogue&quot;]},&quot;core_comment&quot;:{&quot;name&quot;:&quot;core_comment&quot;,&quot;fullpath&quot;:&quot;https:\/\/school.moodledemo.net\/lib\/javascript.php\/1702130560\/comment\/comment.js&quot;,&quot;requires&quot;:[&quot;base&quot;,&quot;io-base&quot;,&quot;node&quot;,&quot;json&quot;,&quot;yui2-animation&quot;,&quot;overlay&quot;,&quot;escape&quot;]}},&quot;logInclude&quot;:[],&quot;logExclude&quot;:[],&quot;logLevel&quot;:null};
M.yui.loader = {modules: {}};

//]]&gt;



    
#katalon{font-family:monospace;font-size:13px;background-color:rgba(0,0,0,.7);position:fixed;top:0;left:0;right:0;display:block;z-index:999999999;line-height: normal} #katalon div{padding:0;margin:0;color:#fff;} #katalon kbd{display:inline-block;padding:3px 5px;font:13px Consolas,&quot;Liberation Mono&quot;,Menlo,Courier,monospace;line-height:10px;color:#555;vertical-align:middle;background-color:#fcfcfc;border:1px solid #ccc;border-bottom-color:#bbb;border-radius:3px;box-shadow:inset 0 -1px 0 #bbb;font-weight: bold} div#katalon-spy_elementInfoDiv {color: lightblue; padding: 0px 5px 5px} div#katalon-spy_instructionDiv {padding: 5px 5px 2.5px}MathJax.Hub.Config({&#xd;
    config: [&quot;Accessible.js&quot;, &quot;Safe.js&quot;],&#xd;
    errorSettings: { message: [&quot;!&quot;] },&#xd;
    skipStartupTypeset: true,&#xd;
    messageStyle: &quot;none&quot;&#xd;
});&#xd;






    
    Skip to main content



//&lt;![CDATA[
document.body.className += ' jsenabled';
//]]&gt;




    
        
            
                
                
                    
                    
        Log in to Mount Orange School
    
        
        document.getElementById('anchor').value = location.hash;
        
        
            
                    Username
            
            
        
        
            Password
            
        
        
            Log in
        
        
            Lost password?
        
    
        
        
            Is this your first time here?
            To explore this site, log in with the role of:Student - with the username student and password moodleTeacher - with the username teacher and password moodleManager - with the username manager and password moodleParent - with the username parent and password moodlePrivacy officer - with the username privacyofficer and password moodleOr explore one of our many Other accounts
        
        
        Some courses may allow guest access
        
            
            
            
            Access as a guest
        
    
    
            
                
                
                        
                
                            
                
                
                                
                                    
                                        
                                            
                                            English (United States) ‎(en_us)‎
                                                
                                            
                                        
                                            
                                                                                                
                                                 አማርኛ ‎(am)‎
                                        
                                                                                                
                                                Afaan Oromoo ‎(om)‎
                                        
                                                                                                
                                                Afrikaans ‎(af)‎
                                        
                                                                                                
                                                Aragonés ‎(an)‎
                                        
                                                                                                
                                                Aranés ‎(oc_es)‎
                                        
                                                                                                
                                                Asturianu ‎(ast)‎
                                        
                                                                                                
                                                Azərbaycanca ‎(az)‎
                                        
                                                                                                
                                                Bahasa Indonesia ‎(id)‎
                                        
                                                                                                
                                                Bahasa Melayu ‎(ms)‎
                                        
                                                                                                
                                                Bamanankan ‎(bm)‎
                                        
                                                                                                
                                                Bislama ‎(bi)‎
                                        
                                                                                                
                                                Bosanski ‎(bs)‎
                                        
                                                                                                
                                                Brezhoneg ‎(br)‎
                                        
                                                                                                
                                                Català ‎(ca)‎
                                        
                                                                                                
                                                Català (Valencià) ‎(ca_valencia)‎
                                        
                                                                                                
                                                Čeština ‎(cs)‎
                                        
                                                                                                
                                                Crnogorski ‎(mis)‎
                                        
                                                                                                
                                                Cymraeg ‎(cy)‎
                                        
                                                                                                
                                                Dansk ‎(da)‎
                                        
                                                                                                
                                                Davvisámegiella ‎(se)‎
                                        
                                                                                                
                                                Deutsch ‎(de)‎
                                        
                                                                                                
                                                Dolnoserbski ‎(dsb)‎
                                        
                                                                                                
                                                Ebon ‎(mh)‎
                                        
                                                                                                
                                                eesti ‎(et)‎
                                        
                                                                                                
                                                English ‎(en)‎
                                        
                                                                                                
                                                English (United States) ‎(en_us)‎
                                        
                                                                                                
                                                Español - Internacional ‎(es)‎
                                        
                                                                                                
                                                Español - México ‎(es_mx)‎
                                        
                                                                                                
                                                Español - Venezuela ‎(es_ve)‎
                                        
                                                                                                
                                                Esperanto ‎(eo)‎
                                        
                                                                                                
                                                Euskara ‎(eu)‎
                                        
                                                                                                
                                                Èʋegbe ‎(ee)‎
                                        
                                                                                                
                                                Filipino ‎(fil)‎
                                        
                                                                                                
                                                Finlandssvenska ‎(sv_fi)‎
                                        
                                                                                                
                                                Føroyskt ‎(fo)‎
                                        
                                                                                                
                                                Français (Canada) ‎(fr_ca)‎
                                        
                                                                                                
                                                Français ‎(fr)‎
                                        
                                                                                                
                                                Gaeilge ‎(ga)‎
                                        
                                                                                                
                                                Gàidhlig ‎(gd)‎
                                        
                                                                                                
                                                Galego ‎(gl)‎
                                        
                                                                                                
                                                Gascon ‎(oc_gsc)‎
                                        
                                                                                                
                                                Hausa ‎(ha)‎
                                        
                                                                                                
                                                Hrvatski ‎(hr)‎
                                        
                                                                                                
                                                ʻŌlelo Hawaiʻi ‎(haw)‎
                                        
                                                                                                
                                                Igbo ‎(ig)‎
                                        
                                                                                                
                                                isiZulu ‎(zu)‎
                                        
                                                                                                
                                                Íslenska ‎(is)‎
                                        
                                                                                                
                                                Italiano ‎(it)‎
                                        
                                                                                                
                                                Kalaallisut ‎(kl)‎
                                        
                                                                                                
                                                Kinyarwanda ‎(rw)‎
                                        
                                                                                                
                                                Kiswahili ‎(sw)‎
                                        
                                                                                                
                                                Kreyòl Ayisyen ‎(hat)‎
                                        
                                                                                                
                                                Kurmanji ‎(kmr)‎
                                        
                                                                                                
                                                Latin ‎(la)‎
                                        
                                                                                                
                                                Latviešu ‎(lv)‎
                                        
                                                                                                
                                                Lëtzebuergesch ‎(lb)‎
                                        
                                                                                                
                                                Lietuvių ‎(lt)‎
                                        
                                                                                                
                                                Lulesamisk ‎(smj)‎
                                        
                                                                                                
                                                magyar ‎(hu)‎
                                        
                                                                                                
                                                Malagasy ‎(mg)‎
                                        
                                                                                                
                                                Māori - Tainui ‎(mi_tn)‎
                                        
                                                                                                
                                                Māori - Waikato ‎(mi_wwow)‎
                                        
                                                                                                
                                                Māori Te Reo ‎(mi)‎
                                        
                                                                                                
                                                Mongolian ‎(mn_mong)‎
                                        
                                                                                                
                                                Nederlands ‎(nl)‎
                                        
                                                                                                
                                                Norsk - nynorsk ‎(nn)‎
                                        
                                                                                                
                                                Norsk ‎(no_gr)‎
                                        
                                                                                                
                                                Norsk ‎(no)‎
                                        
                                                                                                
                                                O'zbekcha ‎(uz)‎
                                        
                                                                                                
                                                Occitan-Lengadocian ‎(oc_lnc)‎
                                        
                                                                                                
                                                Pidgin ‎(pcm)‎
                                        
                                                                                                
                                                Polski ‎(pl)‎
                                        
                                                                                                
                                                Português - Brasil ‎(pt_br)‎
                                        
                                                                                                
                                                Português - Portugal ‎(pt)‎
                                        
                                                                                                
                                                Română ‎(ro)‎
                                        
                                                                                                
                                                Romansh Sursilvan ‎(rm_surs)‎
                                        
                                                                                                
                                                Samoan ‎(sm)‎
                                        
                                                                                                
                                                Setswana ‎(tn)‎
                                        
                                                                                                
                                                Shqip ‎(sq)‎
                                        
                                                                                                
                                                Sicilianu ‎(scn)‎
                                        
                                                                                                
                                                Ślōnski ‎(szl)‎
                                        
                                                                                                
                                                Slovenčina ‎(sk)‎
                                        
                                                                                                
                                                Slovenščina ‎(sl)‎
                                        
                                                                                                
                                                Soomaali ‎(so)‎
                                        
                                                                                                
                                                Sørsamisk ‎(sma)‎
                                        
                                                                                                
                                                Srpski ‎(sr_lt)‎
                                        
                                                                                                
                                                Suomi ‎(fi)‎
                                        
                                                                                                
                                                Suomi+ ‎(fi_co)‎
                                        
                                                                                                
                                                Svenska ‎(sv)‎
                                        
                                                                                                
                                                Tagalog ‎(tl)‎
                                        
                                                                                                
                                                Tamil ‎(ta)‎
                                        
                                                                                                
                                                Taqbaylit ‎(kab)‎
                                        
                                                                                                
                                                Thai ‎(th)‎
                                        
                                                                                                
                                                Tibetan ‎(xct)‎
                                        
                                                                                                
                                                Tok Pisin ‎(tpi)‎
                                        
                                                                                                
                                                Tongan ‎(to)‎
                                        
                                                                                                
                                                Türkçe ‎(tr)‎
                                        
                                                                                                
                                                Turkmen ‎(tk)‎
                                        
                                                                                                
                                                Uyghur - latin ‎(ug_lt)‎
                                        
                                                                                                
                                                VakaViti ‎(fj)‎
                                        
                                                                                                
                                                Vietnamese ‎(vi)‎
                                        
                                                                                                
                                                Wolof ‎(wo)‎
                                        
                                                                                                
                                                Ελληνικά ‎(el)‎
                                        
                                                                                                
                                                Башҡорт теле ‎(ba)‎
                                        
                                                                                                
                                                Беларуская ‎(be)‎
                                        
                                                                                                
                                                Български ‎(bg)‎
                                        
                                                                                                
                                                Кыргызча ‎(ky)‎
                                        
                                                                                                
                                                Қазақша ‎(kk)‎
                                        
                                                                                                
                                                Македонски ‎(mk)‎
                                        
                                                                                                
                                                Монгол ‎(mn)‎
                                        
                                                                                                
                                                Русский ‎(ru)‎
                                        
                                                                                                
                                                Српски ‎(sr_cr)‎
                                        
                                                                                                
                                                Татар ‎(tt)‎
                                        
                                                                                                
                                                Тоҷикӣ ‎(tg)‎
                                        
                                                                                                
                                                Українська ‎(uk)‎
                                        
                                                                                                
                                                ქართული ‎(ka)‎
                                        
                                                                                                
                                                Հայերեն ‎(hy)‎
                                        
                                                                                                
                                                עברית ‎(he)‎
                                        
                                                                                                
                                                ئۇيغۇرچە ‎(ug_ug)‎
                                        
                                                                                                
                                                اردو ‎(ur)‎
                                        
                                                                                                
                                                العربية ‎(ar)‎
                                        
                                                                                                
                                                دری ‎(prs)‎
                                        
                                                                                                
                                                سۆرانی ‎(ckb)‎
                                        
                                                                                                
                                                فارسی ‎(fa)‎
                                        
                                                                                                
                                                لیسي ‎(ps)‎
                                        
                                                                                                
                                                ދިވެހި ‎(dv)‎
                                        
                                                                                                
                                                ⵜⴰⵎⴰⵣⵉⵖⵜ ‎(zgh)‎
                                        
                                                                                                
                                                ትግርኛ ‎(ti)‎
                                        
                                                                                                
                                                नेपाली ‎(ne)‎
                                        
                                                                                                
                                                मराठी ‎(mr)‎
                                        
                                                                                                
                                                हिंदी ‎(hi)‎
                                        
                                                                                                
                                                বাংলা ‎(bn)‎
                                        
                                                                                                
                                                ਪੰਜਾਬੀ ‎(pan)‎
                                        
                                                                                                
                                                ગુજરાતી ‎(gu)‎
                                        
                                                                                                
                                                ଓଡ଼ିଆ ‎(or)‎
                                        
                                                                                                
                                                தமிழ் ‎(ta_lk)‎
                                        
                                                                                                
                                                తెలుగు  ‎(te)‎
                                        
                                                                                                
                                                ಕನ್ನಡ ‎(kn)‎
                                        
                                                                                                
                                                മലയാളം ‎(ml)‎
                                        
                                                                                                
                                                සිංහල ‎(si)‎
                                        
                                                                                                
                                                ລາວ ‎(lo)‎
                                        
                                                                                                
                                                རྫོང་ཁ ‎(dz)‎
                                        
                                                                                                
                                                ဗမာစာ ‎(my)‎
                                        
                                                                                                
                                                ខ្មែរ ‎(km)‎
                                        
                                                                                                
                                                한국어 ‎(ko)‎
                                        
                                                                                                
                                                日本語 ‎(ja)‎
                                        
                                                                                                
                                                正體中文 ‎(zh_tw)‎
                                        
                                                                                                
                                                简体中文 ‎(zh_cn)‎
                                        
                                            
                                    
                                
                
                        
                
                
            
            
        Cookies notice
    

                    
                
                
            
        
    
    
    
        
            
                
            
        
        
            
                
                    You are not logged in.
                
                
                
    
                Data retention summaryGet the mobile appPolicies
                

window.onload = () => {
    let demoresetcountdown = document.createElement('div');
    document.getElementById('page-footer').appendChild(demoresetcountdown);
    demoresetcountdown.textContent = '';
    demoresetcountdown.style.cssText = 'z-index:99999999;position:fixed;bottom:0rem;right:1rem;padding:1px 0.5rem;';
    setInterval(() => {
        let now = Math.floor(new Date().getTime() / 1000);
        let nextWholeHour = now - (now % 3600) + 3600;
        let display = new Date(1970, 0, 1);
        let remainingSecs = nextWholeHour - now;

        display.setSeconds(remainingSecs);

        let secs = display.getSeconds();
        let mins = display.getMinutes();
        let hours = Math.floor((remainingSecs - mins * 3600 - secs * 60) / 60 / 60);
        let text = 'This site will be reset in ';

        if (hours > 0) {
            text += ('0' + hours).slice(-2) + ' hours ';
        }

        text += ('0' + mins).slice(-2) + ' mins ' + ('0' + secs).slice(-2) + ' secs';

        demoresetcountdown.textContent = text;

        if (remainingSecs &lt; 300) {
            demoresetcountdown.style.cssText += 'background-color:red;color:white;font-weight:bold';
        } else {
            demoresetcountdown.style.cssText += 'background-color:black;color:white;font-weight:normal';
        }
    }, 1000);
}

//&lt;![CDATA[
var require = {
    baseUrl : 'https://school.moodledemo.net/lib/requirejs.php/1702130560/',
    // We only support AMD modules with an explicit define() statement.
    enforceDefine: true,
    skipDataMain: true,
    waitSeconds : 0,

    paths: {
        jquery: 'https://school.moodledemo.net/lib/javascript.php/1702130560/lib/jquery/jquery-3.7.1.min',
        jqueryui: 'https://school.moodledemo.net/lib/javascript.php/1702130560/lib/jquery/ui-1.13.2/jquery-ui.min',
        jqueryprivate: 'https://school.moodledemo.net/lib/javascript.php/1702130560/lib/requirejs/jquery-private'
    },

    // Custom jquery config map.
    map: {
      // '*' means all modules will get 'jqueryprivate'
      // for their 'jquery' dependency.
      '*': { jquery: 'jqueryprivate' },
      // Stub module for 'process'. This is a workaround for a bug in MathJax (see MDL-60458).
      '*': { process: 'core/first' },

      // 'jquery-private' wants the real jQuery module
      // though. If this line was not here, there would
      // be an unresolvable cyclic dependency.
      jqueryprivate: { jquery: 'jquery' }
    }
};

//]]&gt;



//&lt;![CDATA[
M.util.js_pending(&quot;core/first&quot;);
require(['core/first'], function() {
require(['core/prefetch'])
;
M.util.js_pending('filter_mathjaxloader/loader'); require(['filter_mathjaxloader/loader'], function(amd) {amd.configure({&quot;mathjaxconfig&quot;:&quot;MathJax.Hub.Config({\r\n    config: [\&quot;Accessible.js\&quot;, \&quot;Safe.js\&quot;],\r\n    errorSettings: { message: [\&quot;!\&quot;] },\r\n    skipStartupTypeset: true,\r\n    messageStyle: \&quot;none\&quot;\r\n});\r\n&quot;,&quot;lang&quot;:&quot;en&quot;}); M.util.js_complete('filter_mathjaxloader/loader');});;
require([&quot;media_videojs/loader&quot;], function(loader) {
    loader.setUp('en-GB');
});;


require(['jquery', 'tool_policy/jquery-eu-cookie-law-popup', 'tool_policy/policyactions'], function($, Popup, ActionsMod) {
        // Initialise the guest popup.
        $(document).ready(function() {
            // Only show message if there is some policy related to guests.
                // Get localised messages.
                var textmessage = &quot;If you continue browsing this website, you agree to our policies:&quot; +
                   &quot;&lt;ul>&quot; +
                   &quot;&lt;li>&quot; +
                   &quot;&lt;a href=\&quot;https://school.moodledemo.net/admin/tool/policy/view.php?versionid=8&amp;amp;returnurl=https%3A%2F%2Fschool.moodledemo.net%2Flogin%2Findex.php\&quot; &quot; +
                   &quot;   data-action=\&quot;view-guest\&quot; data-versionid=\&quot;8\&quot; data-behalfid=\&quot;1\&quot; >&quot; +
                   &quot;Cookie statement&quot; +
                   &quot;&lt;/a>&quot; +
                   &quot;&lt;/li>&quot; +
                   &quot;&quot; +
                   &quot;&lt;li>&quot; +
                   &quot;&lt;a href=\&quot;https://school.moodledemo.net/admin/tool/policy/view.php?versionid=12&amp;amp;returnurl=https%3A%2F%2Fschool.moodledemo.net%2Flogin%2Findex.php\&quot; &quot; +
                   &quot;   data-action=\&quot;view-guest\&quot; data-versionid=\&quot;12\&quot; data-behalfid=\&quot;1\&quot; >&quot; +
                   &quot;Privacy policy&quot; +
                   &quot;&lt;/a>&quot; +
                   &quot;&lt;/li>&quot; +
                   &quot;&quot; +
                   &quot;&lt;li>&quot; +
                   &quot;&lt;a href=\&quot;https://school.moodledemo.net/admin/tool/policy/view.php?versionid=7&amp;amp;returnurl=https%3A%2F%2Fschool.moodledemo.net%2Flogin%2Findex.php\&quot; &quot; +
                   &quot;   data-action=\&quot;view-guest\&quot; data-versionid=\&quot;7\&quot; data-behalfid=\&quot;1\&quot; >&quot; +
                   &quot;Rules for guests&quot; +
                   &quot;&lt;/a>&quot; +
                   &quot;&lt;/li>&quot; +
                   &quot;&quot; +
                   &quot;&lt;/ul>&quot;;
                var continuemessage = &quot;Continue&quot;;

                // Initialize popup.
                $(document.body).addClass('eupopup');
                if ($(&quot;.eupopup&quot;).length > 0) {
                    $(document).euCookieLawPopup().init({
                        popupPosition: 'bottom',
                        popupTitle: '',
                        popupText: textmessage,
                        buttonContinueTitle: continuemessage,
                        buttonLearnmoreTitle: '',
                        compactStyle: true,
                    });
                }

            // Initialise the JS for the modal window which displays the policy versions.
            ActionsMod.init('[data-action=&quot;view-guest&quot;]');
        });
});

;

require(['theme_boost/footer-popover'], function(FooterPopover) {
    FooterPopover.init();
});
;

M.util.js_pending('theme_boost/loader');
require(['theme_boost/loader'], function() {
  M.util.js_complete('theme_boost/loader');
});
;

    require(['core_form/submit'], function(Submit) {
        Submit.init(&quot;loginbtn&quot;);
            Submit.init(&quot;loginguestbtn&quot;);
    });
;
M.util.js_pending('core/notification'); require(['core/notification'], function(amd) {amd.init(1, []); M.util.js_complete('core/notification');});;
M.util.js_pending('core/log'); require(['core/log'], function(amd) {amd.setConfig({&quot;level&quot;:&quot;warn&quot;}); M.util.js_complete('core/log');});;
M.util.js_pending('core/page_global'); require(['core/page_global'], function(amd) {amd.init(); M.util.js_complete('core/page_global');});;
M.util.js_pending('core/utility'); require(['core/utility'], function(amd) {M.util.js_complete('core/utility');});
    M.util.js_complete(&quot;core/first&quot;);
});
//]]&gt;



//&lt;![CDATA[
M.str = {&quot;moodle&quot;:{&quot;lastmodified&quot;:&quot;Last modified&quot;,&quot;name&quot;:&quot;Name&quot;,&quot;error&quot;:&quot;Error&quot;,&quot;info&quot;:&quot;Information&quot;,&quot;yes&quot;:&quot;Yes&quot;,&quot;no&quot;:&quot;No&quot;,&quot;ok&quot;:&quot;OK&quot;,&quot;cancel&quot;:&quot;Cancel&quot;,&quot;confirm&quot;:&quot;Confirm&quot;,&quot;areyousure&quot;:&quot;Are you sure?&quot;,&quot;closebuttontitle&quot;:&quot;Close&quot;,&quot;unknownerror&quot;:&quot;Unknown error&quot;,&quot;file&quot;:&quot;File&quot;,&quot;url&quot;:&quot;URL&quot;,&quot;collapseall&quot;:&quot;Collapse all&quot;,&quot;expandall&quot;:&quot;Expand all&quot;},&quot;repository&quot;:{&quot;type&quot;:&quot;Type&quot;,&quot;size&quot;:&quot;Size&quot;,&quot;invalidjson&quot;:&quot;Invalid JSON string&quot;,&quot;nofilesattached&quot;:&quot;No files attached&quot;,&quot;filepicker&quot;:&quot;File picker&quot;,&quot;logout&quot;:&quot;Logout&quot;,&quot;nofilesavailable&quot;:&quot;No files available&quot;,&quot;norepositoriesavailable&quot;:&quot;Sorry, none of your current repositories can return files in the required format.&quot;,&quot;fileexistsdialogheader&quot;:&quot;File exists&quot;,&quot;fileexistsdialog_editor&quot;:&quot;A file with that name has already been attached to the text you are editing.&quot;,&quot;fileexistsdialog_filemanager&quot;:&quot;A file with that name has already been attached&quot;,&quot;renameto&quot;:&quot;Rename to \&quot;{$a}\&quot;&quot;,&quot;referencesexist&quot;:&quot;There are {$a} links to this file&quot;,&quot;select&quot;:&quot;Select&quot;},&quot;admin&quot;:{&quot;confirmdeletecomments&quot;:&quot;Are you sure you want to delete the selected comment(s)?&quot;,&quot;confirmation&quot;:&quot;Confirmation&quot;},&quot;debug&quot;:{&quot;debuginfo&quot;:&quot;Debug info&quot;,&quot;line&quot;:&quot;Line&quot;,&quot;stacktrace&quot;:&quot;Stack trace&quot;},&quot;langconfig&quot;:{&quot;labelsep&quot;:&quot;:&quot;}};
//]]&gt;


//&lt;![CDATA[
(function() {Y.use(&quot;moodle-filter_glossary-autolinker&quot;,function() {M.filter_glossary.init_filter_autolinking({&quot;courseid&quot;:0});
});
M.util.help_popups.setup(Y);
 M.util.js_pending('random65748643dc765150'); Y.on('domready', function() { M.util.js_complete(&quot;init&quot;);  M.util.js_complete('random65748643dc765150'); });
})();
//]]&gt;


            
            
                Powered by Moodle
            
        
    
        
            
                
            
        
    This site will be reset in 37 mins 09 secs



/html[@class=&quot;yui3-js-enabled&quot;]If you continue browsing this website, you agree to our policies:Cookie statementPrivacy policyRules for guestsContinuex</value>
      <webElementGuid>66862cef-25ee-4f38-a340-8479c9aa46c6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;yui3-js-enabled&quot;]</value>
      <webElementGuid>f40d773e-513d-4e98-a18f-f46de1953a06</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//html</value>
      <webElementGuid>b7d97ae3-42ab-49c9-8d2f-23e8c8ea0273</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//html[(text() = concat(&quot;
    Log in to the site | Mount Orange School
    
    

/** Required in order to fix style inclusion problems in IE with YUI **/

//&lt;![CDATA[
var M = {}; M.yui = {};
M.pageloadstarttime = new Date();
M.cfg = {&quot;wwwroot&quot;:&quot;https:\/\/school.moodledemo.net&quot;,&quot;homeurl&quot;:{},&quot;sesskey&quot;:&quot;h7aTgK4p2a&quot;,&quot;sessiontimeout&quot;:&quot;7200&quot;,&quot;sessiontimeoutwarning&quot;:1200,&quot;themerev&quot;:&quot;1702130560&quot;,&quot;slasharguments&quot;:1,&quot;theme&quot;:&quot;boost&quot;,&quot;iconsystemmodule&quot;:&quot;core\/icon_system_fontawesome&quot;,&quot;jsrev&quot;:&quot;1702130560&quot;,&quot;admin&quot;:&quot;admin&quot;,&quot;svgicons&quot;:true,&quot;usertimezone&quot;:&quot;UTC+1&quot;,&quot;courseId&quot;:1,&quot;courseContextId&quot;:2,&quot;contextid&quot;:1,&quot;contextInstanceId&quot;:0,&quot;langrev&quot;:1702134037,&quot;templaterev&quot;:&quot;1702130560&quot;};var yui1ConfigFn = function(me) {if(/-skin|reset|fonts|grids|base/.test(me.name)){me.type=&quot; , &quot;'&quot; , &quot;css&quot; , &quot;'&quot; , &quot;;me.path=me.path.replace(/\.js/,&quot; , &quot;'&quot; , &quot;.css&quot; , &quot;'&quot; , &quot;);me.path=me.path.replace(/\/yui2-skin/,&quot; , &quot;'&quot; , &quot;/assets/skins/sam/yui2-skin&quot; , &quot;'&quot; , &quot;)}};
var yui2ConfigFn = function(me) {var parts=me.name.replace(/^moodle-/,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;).split(&quot; , &quot;'&quot; , &quot;-&quot; , &quot;'&quot; , &quot;),component=parts.shift(),module=parts[0],min=&quot; , &quot;'&quot; , &quot;-min&quot; , &quot;'&quot; , &quot;;if(/-(skin|core)$/.test(me.name)){parts.pop();me.type=&quot; , &quot;'&quot; , &quot;css&quot; , &quot;'&quot; , &quot;;min=&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;}
if(module){var filename=parts.join(&quot; , &quot;'&quot; , &quot;-&quot; , &quot;'&quot; , &quot;);me.path=component+&quot; , &quot;'&quot; , &quot;/&quot; , &quot;'&quot; , &quot;+module+&quot; , &quot;'&quot; , &quot;/&quot; , &quot;'&quot; , &quot;+filename+min+&quot; , &quot;'&quot; , &quot;.&quot; , &quot;'&quot; , &quot;+me.type}else{me.path=component+&quot; , &quot;'&quot; , &quot;/&quot; , &quot;'&quot; , &quot;+component+&quot; , &quot;'&quot; , &quot;.&quot; , &quot;'&quot; , &quot;+me.type}};
YUI_config = {&quot;debug&quot;:false,&quot;base&quot;:&quot;https:\/\/school.moodledemo.net\/lib\/yuilib\/3.18.1\/&quot;,&quot;comboBase&quot;:&quot;https:\/\/school.moodledemo.net\/theme\/yui_combo.php?&quot;,&quot;combine&quot;:true,&quot;filter&quot;:null,&quot;insertBefore&quot;:&quot;firstthemesheet&quot;,&quot;groups&quot;:{&quot;yui2&quot;:{&quot;base&quot;:&quot;https:\/\/school.moodledemo.net\/lib\/yuilib\/2in3\/2.9.0\/build\/&quot;,&quot;comboBase&quot;:&quot;https:\/\/school.moodledemo.net\/theme\/yui_combo.php?&quot;,&quot;combine&quot;:true,&quot;ext&quot;:false,&quot;root&quot;:&quot;2in3\/2.9.0\/build\/&quot;,&quot;patterns&quot;:{&quot;yui2-&quot;:{&quot;group&quot;:&quot;yui2&quot;,&quot;configFn&quot;:yui1ConfigFn}}},&quot;moodle&quot;:{&quot;name&quot;:&quot;moodle&quot;,&quot;base&quot;:&quot;https:\/\/school.moodledemo.net\/theme\/yui_combo.php?m\/1702130560\/&quot;,&quot;combine&quot;:true,&quot;comboBase&quot;:&quot;https:\/\/school.moodledemo.net\/theme\/yui_combo.php?&quot;,&quot;ext&quot;:false,&quot;root&quot;:&quot;m\/1702130560\/&quot;,&quot;patterns&quot;:{&quot;moodle-&quot;:{&quot;group&quot;:&quot;moodle&quot;,&quot;configFn&quot;:yui2ConfigFn}},&quot;filter&quot;:null,&quot;modules&quot;:{&quot;moodle-core-formchangechecker&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;event-focus&quot;,&quot;moodle-core-event&quot;]},&quot;moodle-core-blocks&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;io&quot;,&quot;dom&quot;,&quot;dd&quot;,&quot;dd-scroll&quot;,&quot;moodle-core-dragdrop&quot;,&quot;moodle-core-notification&quot;]},&quot;moodle-core-maintenancemodetimer&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;]},&quot;moodle-core-event&quot;:{&quot;requires&quot;:[&quot;event-custom&quot;]},&quot;moodle-core-dragdrop&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;io&quot;,&quot;dom&quot;,&quot;dd&quot;,&quot;event-key&quot;,&quot;event-focus&quot;,&quot;moodle-core-notification&quot;]},&quot;moodle-core-lockscroll&quot;:{&quot;requires&quot;:[&quot;plugin&quot;,&quot;base-build&quot;]},&quot;moodle-core-handlebars&quot;:{&quot;condition&quot;:{&quot;trigger&quot;:&quot;handlebars&quot;,&quot;when&quot;:&quot;after&quot;}},&quot;moodle-core-chooserdialogue&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;panel&quot;,&quot;moodle-core-notification&quot;]},&quot;moodle-core-notification&quot;:{&quot;requires&quot;:[&quot;moodle-core-notification-dialogue&quot;,&quot;moodle-core-notification-alert&quot;,&quot;moodle-core-notification-confirm&quot;,&quot;moodle-core-notification-exception&quot;,&quot;moodle-core-notification-ajaxexception&quot;]},&quot;moodle-core-notification-dialogue&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;panel&quot;,&quot;escape&quot;,&quot;event-key&quot;,&quot;dd-plugin&quot;,&quot;moodle-core-widget-focusafterclose&quot;,&quot;moodle-core-lockscroll&quot;]},&quot;moodle-core-notification-alert&quot;:{&quot;requires&quot;:[&quot;moodle-core-notification-dialogue&quot;]},&quot;moodle-core-notification-confirm&quot;:{&quot;requires&quot;:[&quot;moodle-core-notification-dialogue&quot;]},&quot;moodle-core-notification-exception&quot;:{&quot;requires&quot;:[&quot;moodle-core-notification-dialogue&quot;]},&quot;moodle-core-notification-ajaxexception&quot;:{&quot;requires&quot;:[&quot;moodle-core-notification-dialogue&quot;]},&quot;moodle-core-actionmenu&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;event&quot;,&quot;node-event-simulate&quot;]},&quot;moodle-core_availability-form&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;event-delegate&quot;,&quot;panel&quot;,&quot;moodle-core-notification-dialogue&quot;,&quot;json&quot;]},&quot;moodle-backup-backupselectall&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;event&quot;,&quot;node-event-simulate&quot;,&quot;anim&quot;]},&quot;moodle-course-util&quot;:{&quot;requires&quot;:[&quot;node&quot;],&quot;use&quot;:[&quot;moodle-course-util-base&quot;],&quot;submodules&quot;:{&quot;moodle-course-util-base&quot;:{},&quot;moodle-course-util-section&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;moodle-course-util-base&quot;]},&quot;moodle-course-util-cm&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;moodle-course-util-base&quot;]}}},&quot;moodle-course-categoryexpander&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;event-key&quot;]},&quot;moodle-course-management&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;io-base&quot;,&quot;moodle-core-notification-exception&quot;,&quot;json-parse&quot;,&quot;dd-constrain&quot;,&quot;dd-proxy&quot;,&quot;dd-drop&quot;,&quot;dd-delegate&quot;,&quot;node-event-delegate&quot;]},&quot;moodle-course-dragdrop&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;io&quot;,&quot;dom&quot;,&quot;dd&quot;,&quot;dd-scroll&quot;,&quot;moodle-core-dragdrop&quot;,&quot;moodle-core-notification&quot;,&quot;moodle-course-coursebase&quot;,&quot;moodle-course-util&quot;]},&quot;moodle-form-dateselector&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;overlay&quot;,&quot;calendar&quot;]},&quot;moodle-form-shortforms&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;base&quot;,&quot;selector-css3&quot;,&quot;moodle-core-event&quot;]},&quot;moodle-question-preview&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;dom&quot;,&quot;event-delegate&quot;,&quot;event-key&quot;,&quot;core_question_engine&quot;]},&quot;moodle-question-searchform&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;]},&quot;moodle-question-chooser&quot;:{&quot;requires&quot;:[&quot;moodle-core-chooserdialogue&quot;]},&quot;moodle-availability_completion-form&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;moodle-core_availability-form&quot;]},&quot;moodle-availability_date-form&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;io&quot;,&quot;moodle-core_availability-form&quot;]},&quot;moodle-availability_grade-form&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;moodle-core_availability-form&quot;]},&quot;moodle-availability_group-form&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;moodle-core_availability-form&quot;]},&quot;moodle-availability_grouping-form&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;moodle-core_availability-form&quot;]},&quot;moodle-availability_profile-form&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;moodle-core_availability-form&quot;]},&quot;moodle-mod_assign-history&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;transition&quot;]},&quot;moodle-mod_quiz-modform&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;]},&quot;moodle-mod_quiz-util&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;moodle-core-actionmenu&quot;],&quot;use&quot;:[&quot;moodle-mod_quiz-util-base&quot;],&quot;submodules&quot;:{&quot;moodle-mod_quiz-util-base&quot;:{},&quot;moodle-mod_quiz-util-slot&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;moodle-mod_quiz-util-base&quot;]},&quot;moodle-mod_quiz-util-page&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;moodle-mod_quiz-util-base&quot;]}}},&quot;moodle-mod_quiz-questionchooser&quot;:{&quot;requires&quot;:[&quot;moodle-core-chooserdialogue&quot;,&quot;moodle-mod_quiz-util&quot;,&quot;querystring-parse&quot;]},&quot;moodle-mod_quiz-dragdrop&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;io&quot;,&quot;dom&quot;,&quot;dd&quot;,&quot;dd-scroll&quot;,&quot;moodle-core-dragdrop&quot;,&quot;moodle-core-notification&quot;,&quot;moodle-mod_quiz-quizbase&quot;,&quot;moodle-mod_quiz-util-base&quot;,&quot;moodle-mod_quiz-util-page&quot;,&quot;moodle-mod_quiz-util-slot&quot;,&quot;moodle-course-util&quot;]},&quot;moodle-mod_quiz-quizbase&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;]},&quot;moodle-mod_quiz-toolboxes&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;event-key&quot;,&quot;io&quot;,&quot;moodle-mod_quiz-quizbase&quot;,&quot;moodle-mod_quiz-util-slot&quot;,&quot;moodle-core-notification-ajaxexception&quot;]},&quot;moodle-mod_quiz-autosave&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;event-valuechange&quot;,&quot;node-event-delegate&quot;,&quot;io-form&quot;]},&quot;moodle-message_airnotifier-toolboxes&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;io&quot;]},&quot;moodle-filter_glossary-autolinker&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;io-base&quot;,&quot;json-parse&quot;,&quot;event-delegate&quot;,&quot;overlay&quot;,&quot;moodle-core-event&quot;,&quot;moodle-core-notification-alert&quot;,&quot;moodle-core-notification-exception&quot;,&quot;moodle-core-notification-ajaxexception&quot;]},&quot;moodle-editor_atto-rangy&quot;:{&quot;requires&quot;:[]},&quot;moodle-editor_atto-editor&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;transition&quot;,&quot;io&quot;,&quot;overlay&quot;,&quot;escape&quot;,&quot;event&quot;,&quot;event-simulate&quot;,&quot;event-custom&quot;,&quot;node-event-html5&quot;,&quot;node-event-simulate&quot;,&quot;yui-throttle&quot;,&quot;moodle-core-notification-dialogue&quot;,&quot;moodle-editor_atto-rangy&quot;,&quot;handlebars&quot;,&quot;timers&quot;,&quot;querystring-stringify&quot;]},&quot;moodle-editor_atto-plugin&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;base&quot;,&quot;escape&quot;,&quot;event&quot;,&quot;event-outside&quot;,&quot;handlebars&quot;,&quot;event-custom&quot;,&quot;timers&quot;,&quot;moodle-editor_atto-menu&quot;]},&quot;moodle-editor_atto-menu&quot;:{&quot;requires&quot;:[&quot;moodle-core-notification-dialogue&quot;,&quot;node&quot;,&quot;event&quot;,&quot;event-custom&quot;]},&quot;moodle-report_eventlist-eventfilter&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;event&quot;,&quot;node&quot;,&quot;node-event-delegate&quot;,&quot;datatable&quot;,&quot;autocomplete&quot;,&quot;autocomplete-filters&quot;]},&quot;moodle-report_loglive-fetchlogs&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;event&quot;,&quot;node&quot;,&quot;io&quot;,&quot;node-event-delegate&quot;]},&quot;moodle-gradereport_history-userselector&quot;:{&quot;requires&quot;:[&quot;escape&quot;,&quot;event-delegate&quot;,&quot;event-key&quot;,&quot;handlebars&quot;,&quot;io-base&quot;,&quot;json-parse&quot;,&quot;moodle-core-notification-dialogue&quot;]},&quot;moodle-qbank_editquestion-chooser&quot;:{&quot;requires&quot;:[&quot;moodle-core-chooserdialogue&quot;]},&quot;moodle-tool_lp-dragdrop-reorder&quot;:{&quot;requires&quot;:[&quot;moodle-core-dragdrop&quot;]},&quot;moodle-assignfeedback_editpdf-editor&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;event&quot;,&quot;node&quot;,&quot;io&quot;,&quot;graphics&quot;,&quot;json&quot;,&quot;event-move&quot;,&quot;event-resize&quot;,&quot;transition&quot;,&quot;querystring-stringify-simple&quot;,&quot;moodle-core-notification-dialog&quot;,&quot;moodle-core-notification-alert&quot;,&quot;moodle-core-notification-warning&quot;,&quot;moodle-core-notification-exception&quot;,&quot;moodle-core-notification-ajaxexception&quot;]},&quot;moodle-atto_accessibilitychecker-button&quot;:{&quot;requires&quot;:[&quot;color-base&quot;,&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_accessibilityhelper-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_align-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_bold-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_charmap-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_clear-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_collapse-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_emojipicker-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_emoticon-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_equation-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;,&quot;moodle-core-event&quot;,&quot;io&quot;,&quot;event-valuechange&quot;,&quot;tabview&quot;,&quot;array-extras&quot;]},&quot;moodle-atto_h5p-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_html-beautify&quot;:{},&quot;moodle-atto_html-codemirror&quot;:{&quot;requires&quot;:[&quot;moodle-atto_html-codemirror-skin&quot;]},&quot;moodle-atto_html-button&quot;:{&quot;requires&quot;:[&quot;promise&quot;,&quot;moodle-editor_atto-plugin&quot;,&quot;moodle-atto_html-beautify&quot;,&quot;moodle-atto_html-codemirror&quot;,&quot;event-valuechange&quot;]},&quot;moodle-atto_image-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_indent-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_italic-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_link-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_managefiles-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_managefiles-usedfiles&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;escape&quot;]},&quot;moodle-atto_media-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;,&quot;moodle-form-shortforms&quot;]},&quot;moodle-atto_noautolink-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_orderedlist-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_recordrtc-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;,&quot;moodle-atto_recordrtc-recording&quot;]},&quot;moodle-atto_recordrtc-recording&quot;:{&quot;requires&quot;:[&quot;moodle-atto_recordrtc-button&quot;]},&quot;moodle-atto_rtl-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_strike-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_subscript-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_superscript-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_table-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;,&quot;moodle-editor_atto-menu&quot;,&quot;event&quot;,&quot;event-valuechange&quot;]},&quot;moodle-atto_title-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_underline-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_undo-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_unorderedlist-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]}}},&quot;gallery&quot;:{&quot;name&quot;:&quot;gallery&quot;,&quot;base&quot;:&quot;https:\/\/school.moodledemo.net\/lib\/yuilib\/gallery\/&quot;,&quot;combine&quot;:true,&quot;comboBase&quot;:&quot;https:\/\/school.moodledemo.net\/theme\/yui_combo.php?&quot;,&quot;ext&quot;:false,&quot;root&quot;:&quot;gallery\/1702130560\/&quot;,&quot;patterns&quot;:{&quot;gallery-&quot;:{&quot;group&quot;:&quot;gallery&quot;}}}},&quot;modules&quot;:{&quot;core_filepicker&quot;:{&quot;name&quot;:&quot;core_filepicker&quot;,&quot;fullpath&quot;:&quot;https:\/\/school.moodledemo.net\/lib\/javascript.php\/1702130560\/repository\/filepicker.js&quot;,&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;node-event-simulate&quot;,&quot;json&quot;,&quot;async-queue&quot;,&quot;io-base&quot;,&quot;io-upload-iframe&quot;,&quot;io-form&quot;,&quot;yui2-treeview&quot;,&quot;panel&quot;,&quot;cookie&quot;,&quot;datatable&quot;,&quot;datatable-sort&quot;,&quot;resize-plugin&quot;,&quot;dd-plugin&quot;,&quot;escape&quot;,&quot;moodle-core_filepicker&quot;,&quot;moodle-core-notification-dialogue&quot;]},&quot;core_comment&quot;:{&quot;name&quot;:&quot;core_comment&quot;,&quot;fullpath&quot;:&quot;https:\/\/school.moodledemo.net\/lib\/javascript.php\/1702130560\/comment\/comment.js&quot;,&quot;requires&quot;:[&quot;base&quot;,&quot;io-base&quot;,&quot;node&quot;,&quot;json&quot;,&quot;yui2-animation&quot;,&quot;overlay&quot;,&quot;escape&quot;]}},&quot;logInclude&quot;:[],&quot;logExclude&quot;:[],&quot;logLevel&quot;:null};
M.yui.loader = {modules: {}};

//]]&gt;



    
#katalon{font-family:monospace;font-size:13px;background-color:rgba(0,0,0,.7);position:fixed;top:0;left:0;right:0;display:block;z-index:999999999;line-height: normal} #katalon div{padding:0;margin:0;color:#fff;} #katalon kbd{display:inline-block;padding:3px 5px;font:13px Consolas,&quot;Liberation Mono&quot;,Menlo,Courier,monospace;line-height:10px;color:#555;vertical-align:middle;background-color:#fcfcfc;border:1px solid #ccc;border-bottom-color:#bbb;border-radius:3px;box-shadow:inset 0 -1px 0 #bbb;font-weight: bold} div#katalon-spy_elementInfoDiv {color: lightblue; padding: 0px 5px 5px} div#katalon-spy_instructionDiv {padding: 5px 5px 2.5px}MathJax.Hub.Config({&#xd;
    config: [&quot;Accessible.js&quot;, &quot;Safe.js&quot;],&#xd;
    errorSettings: { message: [&quot;!&quot;] },&#xd;
    skipStartupTypeset: true,&#xd;
    messageStyle: &quot;none&quot;&#xd;
});&#xd;






    
    Skip to main content



//&lt;![CDATA[
document.body.className += &quot; , &quot;'&quot; , &quot; jsenabled&quot; , &quot;'&quot; , &quot;;
//]]&gt;




    
        
            
                
                
                    
                    
        Log in to Mount Orange School
    
        
        document.getElementById(&quot; , &quot;'&quot; , &quot;anchor&quot; , &quot;'&quot; , &quot;).value = location.hash;
        
        
            
                    Username
            
            
        
        
            Password
            
        
        
            Log in
        
        
            Lost password?
        
    
        
        
            Is this your first time here?
            To explore this site, log in with the role of:Student - with the username student and password moodleTeacher - with the username teacher and password moodleManager - with the username manager and password moodleParent - with the username parent and password moodlePrivacy officer - with the username privacyofficer and password moodleOr explore one of our many Other accounts
        
        
        Some courses may allow guest access
        
            
            
            
            Access as a guest
        
    
    
            
                
                
                        
                
                            
                
                
                                
                                    
                                        
                                            
                                            English (United States) ‎(en_us)‎
                                                
                                            
                                        
                                            
                                                                                                
                                                 አማርኛ ‎(am)‎
                                        
                                                                                                
                                                Afaan Oromoo ‎(om)‎
                                        
                                                                                                
                                                Afrikaans ‎(af)‎
                                        
                                                                                                
                                                Aragonés ‎(an)‎
                                        
                                                                                                
                                                Aranés ‎(oc_es)‎
                                        
                                                                                                
                                                Asturianu ‎(ast)‎
                                        
                                                                                                
                                                Azərbaycanca ‎(az)‎
                                        
                                                                                                
                                                Bahasa Indonesia ‎(id)‎
                                        
                                                                                                
                                                Bahasa Melayu ‎(ms)‎
                                        
                                                                                                
                                                Bamanankan ‎(bm)‎
                                        
                                                                                                
                                                Bislama ‎(bi)‎
                                        
                                                                                                
                                                Bosanski ‎(bs)‎
                                        
                                                                                                
                                                Brezhoneg ‎(br)‎
                                        
                                                                                                
                                                Català ‎(ca)‎
                                        
                                                                                                
                                                Català (Valencià) ‎(ca_valencia)‎
                                        
                                                                                                
                                                Čeština ‎(cs)‎
                                        
                                                                                                
                                                Crnogorski ‎(mis)‎
                                        
                                                                                                
                                                Cymraeg ‎(cy)‎
                                        
                                                                                                
                                                Dansk ‎(da)‎
                                        
                                                                                                
                                                Davvisámegiella ‎(se)‎
                                        
                                                                                                
                                                Deutsch ‎(de)‎
                                        
                                                                                                
                                                Dolnoserbski ‎(dsb)‎
                                        
                                                                                                
                                                Ebon ‎(mh)‎
                                        
                                                                                                
                                                eesti ‎(et)‎
                                        
                                                                                                
                                                English ‎(en)‎
                                        
                                                                                                
                                                English (United States) ‎(en_us)‎
                                        
                                                                                                
                                                Español - Internacional ‎(es)‎
                                        
                                                                                                
                                                Español - México ‎(es_mx)‎
                                        
                                                                                                
                                                Español - Venezuela ‎(es_ve)‎
                                        
                                                                                                
                                                Esperanto ‎(eo)‎
                                        
                                                                                                
                                                Euskara ‎(eu)‎
                                        
                                                                                                
                                                Èʋegbe ‎(ee)‎
                                        
                                                                                                
                                                Filipino ‎(fil)‎
                                        
                                                                                                
                                                Finlandssvenska ‎(sv_fi)‎
                                        
                                                                                                
                                                Føroyskt ‎(fo)‎
                                        
                                                                                                
                                                Français (Canada) ‎(fr_ca)‎
                                        
                                                                                                
                                                Français ‎(fr)‎
                                        
                                                                                                
                                                Gaeilge ‎(ga)‎
                                        
                                                                                                
                                                Gàidhlig ‎(gd)‎
                                        
                                                                                                
                                                Galego ‎(gl)‎
                                        
                                                                                                
                                                Gascon ‎(oc_gsc)‎
                                        
                                                                                                
                                                Hausa ‎(ha)‎
                                        
                                                                                                
                                                Hrvatski ‎(hr)‎
                                        
                                                                                                
                                                ʻŌlelo Hawaiʻi ‎(haw)‎
                                        
                                                                                                
                                                Igbo ‎(ig)‎
                                        
                                                                                                
                                                isiZulu ‎(zu)‎
                                        
                                                                                                
                                                Íslenska ‎(is)‎
                                        
                                                                                                
                                                Italiano ‎(it)‎
                                        
                                                                                                
                                                Kalaallisut ‎(kl)‎
                                        
                                                                                                
                                                Kinyarwanda ‎(rw)‎
                                        
                                                                                                
                                                Kiswahili ‎(sw)‎
                                        
                                                                                                
                                                Kreyòl Ayisyen ‎(hat)‎
                                        
                                                                                                
                                                Kurmanji ‎(kmr)‎
                                        
                                                                                                
                                                Latin ‎(la)‎
                                        
                                                                                                
                                                Latviešu ‎(lv)‎
                                        
                                                                                                
                                                Lëtzebuergesch ‎(lb)‎
                                        
                                                                                                
                                                Lietuvių ‎(lt)‎
                                        
                                                                                                
                                                Lulesamisk ‎(smj)‎
                                        
                                                                                                
                                                magyar ‎(hu)‎
                                        
                                                                                                
                                                Malagasy ‎(mg)‎
                                        
                                                                                                
                                                Māori - Tainui ‎(mi_tn)‎
                                        
                                                                                                
                                                Māori - Waikato ‎(mi_wwow)‎
                                        
                                                                                                
                                                Māori Te Reo ‎(mi)‎
                                        
                                                                                                
                                                Mongolian ‎(mn_mong)‎
                                        
                                                                                                
                                                Nederlands ‎(nl)‎
                                        
                                                                                                
                                                Norsk - nynorsk ‎(nn)‎
                                        
                                                                                                
                                                Norsk ‎(no_gr)‎
                                        
                                                                                                
                                                Norsk ‎(no)‎
                                        
                                                                                                
                                                O&quot; , &quot;'&quot; , &quot;zbekcha ‎(uz)‎
                                        
                                                                                                
                                                Occitan-Lengadocian ‎(oc_lnc)‎
                                        
                                                                                                
                                                Pidgin ‎(pcm)‎
                                        
                                                                                                
                                                Polski ‎(pl)‎
                                        
                                                                                                
                                                Português - Brasil ‎(pt_br)‎
                                        
                                                                                                
                                                Português - Portugal ‎(pt)‎
                                        
                                                                                                
                                                Română ‎(ro)‎
                                        
                                                                                                
                                                Romansh Sursilvan ‎(rm_surs)‎
                                        
                                                                                                
                                                Samoan ‎(sm)‎
                                        
                                                                                                
                                                Setswana ‎(tn)‎
                                        
                                                                                                
                                                Shqip ‎(sq)‎
                                        
                                                                                                
                                                Sicilianu ‎(scn)‎
                                        
                                                                                                
                                                Ślōnski ‎(szl)‎
                                        
                                                                                                
                                                Slovenčina ‎(sk)‎
                                        
                                                                                                
                                                Slovenščina ‎(sl)‎
                                        
                                                                                                
                                                Soomaali ‎(so)‎
                                        
                                                                                                
                                                Sørsamisk ‎(sma)‎
                                        
                                                                                                
                                                Srpski ‎(sr_lt)‎
                                        
                                                                                                
                                                Suomi ‎(fi)‎
                                        
                                                                                                
                                                Suomi+ ‎(fi_co)‎
                                        
                                                                                                
                                                Svenska ‎(sv)‎
                                        
                                                                                                
                                                Tagalog ‎(tl)‎
                                        
                                                                                                
                                                Tamil ‎(ta)‎
                                        
                                                                                                
                                                Taqbaylit ‎(kab)‎
                                        
                                                                                                
                                                Thai ‎(th)‎
                                        
                                                                                                
                                                Tibetan ‎(xct)‎
                                        
                                                                                                
                                                Tok Pisin ‎(tpi)‎
                                        
                                                                                                
                                                Tongan ‎(to)‎
                                        
                                                                                                
                                                Türkçe ‎(tr)‎
                                        
                                                                                                
                                                Turkmen ‎(tk)‎
                                        
                                                                                                
                                                Uyghur - latin ‎(ug_lt)‎
                                        
                                                                                                
                                                VakaViti ‎(fj)‎
                                        
                                                                                                
                                                Vietnamese ‎(vi)‎
                                        
                                                                                                
                                                Wolof ‎(wo)‎
                                        
                                                                                                
                                                Ελληνικά ‎(el)‎
                                        
                                                                                                
                                                Башҡорт теле ‎(ba)‎
                                        
                                                                                                
                                                Беларуская ‎(be)‎
                                        
                                                                                                
                                                Български ‎(bg)‎
                                        
                                                                                                
                                                Кыргызча ‎(ky)‎
                                        
                                                                                                
                                                Қазақша ‎(kk)‎
                                        
                                                                                                
                                                Македонски ‎(mk)‎
                                        
                                                                                                
                                                Монгол ‎(mn)‎
                                        
                                                                                                
                                                Русский ‎(ru)‎
                                        
                                                                                                
                                                Српски ‎(sr_cr)‎
                                        
                                                                                                
                                                Татар ‎(tt)‎
                                        
                                                                                                
                                                Тоҷикӣ ‎(tg)‎
                                        
                                                                                                
                                                Українська ‎(uk)‎
                                        
                                                                                                
                                                ქართული ‎(ka)‎
                                        
                                                                                                
                                                Հայերեն ‎(hy)‎
                                        
                                                                                                
                                                עברית ‎(he)‎
                                        
                                                                                                
                                                ئۇيغۇرچە ‎(ug_ug)‎
                                        
                                                                                                
                                                اردو ‎(ur)‎
                                        
                                                                                                
                                                العربية ‎(ar)‎
                                        
                                                                                                
                                                دری ‎(prs)‎
                                        
                                                                                                
                                                سۆرانی ‎(ckb)‎
                                        
                                                                                                
                                                فارسی ‎(fa)‎
                                        
                                                                                                
                                                لیسي ‎(ps)‎
                                        
                                                                                                
                                                ދިވެހި ‎(dv)‎
                                        
                                                                                                
                                                ⵜⴰⵎⴰⵣⵉⵖⵜ ‎(zgh)‎
                                        
                                                                                                
                                                ትግርኛ ‎(ti)‎
                                        
                                                                                                
                                                नेपाली ‎(ne)‎
                                        
                                                                                                
                                                मराठी ‎(mr)‎
                                        
                                                                                                
                                                हिंदी ‎(hi)‎
                                        
                                                                                                
                                                বাংলা ‎(bn)‎
                                        
                                                                                                
                                                ਪੰਜਾਬੀ ‎(pan)‎
                                        
                                                                                                
                                                ગુજરાતી ‎(gu)‎
                                        
                                                                                                
                                                ଓଡ଼ିଆ ‎(or)‎
                                        
                                                                                                
                                                தமிழ் ‎(ta_lk)‎
                                        
                                                                                                
                                                తెలుగు  ‎(te)‎
                                        
                                                                                                
                                                ಕನ್ನಡ ‎(kn)‎
                                        
                                                                                                
                                                മലയാളം ‎(ml)‎
                                        
                                                                                                
                                                සිංහල ‎(si)‎
                                        
                                                                                                
                                                ລາວ ‎(lo)‎
                                        
                                                                                                
                                                རྫོང་ཁ ‎(dz)‎
                                        
                                                                                                
                                                ဗမာစာ ‎(my)‎
                                        
                                                                                                
                                                ខ្មែរ ‎(km)‎
                                        
                                                                                                
                                                한국어 ‎(ko)‎
                                        
                                                                                                
                                                日本語 ‎(ja)‎
                                        
                                                                                                
                                                正體中文 ‎(zh_tw)‎
                                        
                                                                                                
                                                简体中文 ‎(zh_cn)‎
                                        
                                            
                                    
                                
                
                        
                
                
            
            
        Cookies notice
    

                    
                
                
            
        
    
    
    
        
            
                
            
        
        
            
                
                    You are not logged in.
                
                
                
    
                Data retention summaryGet the mobile appPolicies
                

window.onload = () => {
    let demoresetcountdown = document.createElement(&quot; , &quot;'&quot; , &quot;div&quot; , &quot;'&quot; , &quot;);
    document.getElementById(&quot; , &quot;'&quot; , &quot;page-footer&quot; , &quot;'&quot; , &quot;).appendChild(demoresetcountdown);
    demoresetcountdown.textContent = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
    demoresetcountdown.style.cssText = &quot; , &quot;'&quot; , &quot;z-index:99999999;position:fixed;bottom:0rem;right:1rem;padding:1px 0.5rem;&quot; , &quot;'&quot; , &quot;;
    setInterval(() => {
        let now = Math.floor(new Date().getTime() / 1000);
        let nextWholeHour = now - (now % 3600) + 3600;
        let display = new Date(1970, 0, 1);
        let remainingSecs = nextWholeHour - now;

        display.setSeconds(remainingSecs);

        let secs = display.getSeconds();
        let mins = display.getMinutes();
        let hours = Math.floor((remainingSecs - mins * 3600 - secs * 60) / 60 / 60);
        let text = &quot; , &quot;'&quot; , &quot;This site will be reset in &quot; , &quot;'&quot; , &quot;;

        if (hours > 0) {
            text += (&quot; , &quot;'&quot; , &quot;0&quot; , &quot;'&quot; , &quot; + hours).slice(-2) + &quot; , &quot;'&quot; , &quot; hours &quot; , &quot;'&quot; , &quot;;
        }

        text += (&quot; , &quot;'&quot; , &quot;0&quot; , &quot;'&quot; , &quot; + mins).slice(-2) + &quot; , &quot;'&quot; , &quot; mins &quot; , &quot;'&quot; , &quot; + (&quot; , &quot;'&quot; , &quot;0&quot; , &quot;'&quot; , &quot; + secs).slice(-2) + &quot; , &quot;'&quot; , &quot; secs&quot; , &quot;'&quot; , &quot;;

        demoresetcountdown.textContent = text;

        if (remainingSecs &lt; 300) {
            demoresetcountdown.style.cssText += &quot; , &quot;'&quot; , &quot;background-color:red;color:white;font-weight:bold&quot; , &quot;'&quot; , &quot;;
        } else {
            demoresetcountdown.style.cssText += &quot; , &quot;'&quot; , &quot;background-color:black;color:white;font-weight:normal&quot; , &quot;'&quot; , &quot;;
        }
    }, 1000);
}

//&lt;![CDATA[
var require = {
    baseUrl : &quot; , &quot;'&quot; , &quot;https://school.moodledemo.net/lib/requirejs.php/1702130560/&quot; , &quot;'&quot; , &quot;,
    // We only support AMD modules with an explicit define() statement.
    enforceDefine: true,
    skipDataMain: true,
    waitSeconds : 0,

    paths: {
        jquery: &quot; , &quot;'&quot; , &quot;https://school.moodledemo.net/lib/javascript.php/1702130560/lib/jquery/jquery-3.7.1.min&quot; , &quot;'&quot; , &quot;,
        jqueryui: &quot; , &quot;'&quot; , &quot;https://school.moodledemo.net/lib/javascript.php/1702130560/lib/jquery/ui-1.13.2/jquery-ui.min&quot; , &quot;'&quot; , &quot;,
        jqueryprivate: &quot; , &quot;'&quot; , &quot;https://school.moodledemo.net/lib/javascript.php/1702130560/lib/requirejs/jquery-private&quot; , &quot;'&quot; , &quot;
    },

    // Custom jquery config map.
    map: {
      // &quot; , &quot;'&quot; , &quot;*&quot; , &quot;'&quot; , &quot; means all modules will get &quot; , &quot;'&quot; , &quot;jqueryprivate&quot; , &quot;'&quot; , &quot;
      // for their &quot; , &quot;'&quot; , &quot;jquery&quot; , &quot;'&quot; , &quot; dependency.
      &quot; , &quot;'&quot; , &quot;*&quot; , &quot;'&quot; , &quot;: { jquery: &quot; , &quot;'&quot; , &quot;jqueryprivate&quot; , &quot;'&quot; , &quot; },
      // Stub module for &quot; , &quot;'&quot; , &quot;process&quot; , &quot;'&quot; , &quot;. This is a workaround for a bug in MathJax (see MDL-60458).
      &quot; , &quot;'&quot; , &quot;*&quot; , &quot;'&quot; , &quot;: { process: &quot; , &quot;'&quot; , &quot;core/first&quot; , &quot;'&quot; , &quot; },

      // &quot; , &quot;'&quot; , &quot;jquery-private&quot; , &quot;'&quot; , &quot; wants the real jQuery module
      // though. If this line was not here, there would
      // be an unresolvable cyclic dependency.
      jqueryprivate: { jquery: &quot; , &quot;'&quot; , &quot;jquery&quot; , &quot;'&quot; , &quot; }
    }
};

//]]&gt;



//&lt;![CDATA[
M.util.js_pending(&quot;core/first&quot;);
require([&quot; , &quot;'&quot; , &quot;core/first&quot; , &quot;'&quot; , &quot;], function() {
require([&quot; , &quot;'&quot; , &quot;core/prefetch&quot; , &quot;'&quot; , &quot;])
;
M.util.js_pending(&quot; , &quot;'&quot; , &quot;filter_mathjaxloader/loader&quot; , &quot;'&quot; , &quot;); require([&quot; , &quot;'&quot; , &quot;filter_mathjaxloader/loader&quot; , &quot;'&quot; , &quot;], function(amd) {amd.configure({&quot;mathjaxconfig&quot;:&quot;MathJax.Hub.Config({\r\n    config: [\&quot;Accessible.js\&quot;, \&quot;Safe.js\&quot;],\r\n    errorSettings: { message: [\&quot;!\&quot;] },\r\n    skipStartupTypeset: true,\r\n    messageStyle: \&quot;none\&quot;\r\n});\r\n&quot;,&quot;lang&quot;:&quot;en&quot;}); M.util.js_complete(&quot; , &quot;'&quot; , &quot;filter_mathjaxloader/loader&quot; , &quot;'&quot; , &quot;);});;
require([&quot;media_videojs/loader&quot;], function(loader) {
    loader.setUp(&quot; , &quot;'&quot; , &quot;en-GB&quot; , &quot;'&quot; , &quot;);
});;


require([&quot; , &quot;'&quot; , &quot;jquery&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;tool_policy/jquery-eu-cookie-law-popup&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;tool_policy/policyactions&quot; , &quot;'&quot; , &quot;], function($, Popup, ActionsMod) {
        // Initialise the guest popup.
        $(document).ready(function() {
            // Only show message if there is some policy related to guests.
                // Get localised messages.
                var textmessage = &quot;If you continue browsing this website, you agree to our policies:&quot; +
                   &quot;&lt;ul>&quot; +
                   &quot;&lt;li>&quot; +
                   &quot;&lt;a href=\&quot;https://school.moodledemo.net/admin/tool/policy/view.php?versionid=8&amp;amp;returnurl=https%3A%2F%2Fschool.moodledemo.net%2Flogin%2Findex.php\&quot; &quot; +
                   &quot;   data-action=\&quot;view-guest\&quot; data-versionid=\&quot;8\&quot; data-behalfid=\&quot;1\&quot; >&quot; +
                   &quot;Cookie statement&quot; +
                   &quot;&lt;/a>&quot; +
                   &quot;&lt;/li>&quot; +
                   &quot;&quot; +
                   &quot;&lt;li>&quot; +
                   &quot;&lt;a href=\&quot;https://school.moodledemo.net/admin/tool/policy/view.php?versionid=12&amp;amp;returnurl=https%3A%2F%2Fschool.moodledemo.net%2Flogin%2Findex.php\&quot; &quot; +
                   &quot;   data-action=\&quot;view-guest\&quot; data-versionid=\&quot;12\&quot; data-behalfid=\&quot;1\&quot; >&quot; +
                   &quot;Privacy policy&quot; +
                   &quot;&lt;/a>&quot; +
                   &quot;&lt;/li>&quot; +
                   &quot;&quot; +
                   &quot;&lt;li>&quot; +
                   &quot;&lt;a href=\&quot;https://school.moodledemo.net/admin/tool/policy/view.php?versionid=7&amp;amp;returnurl=https%3A%2F%2Fschool.moodledemo.net%2Flogin%2Findex.php\&quot; &quot; +
                   &quot;   data-action=\&quot;view-guest\&quot; data-versionid=\&quot;7\&quot; data-behalfid=\&quot;1\&quot; >&quot; +
                   &quot;Rules for guests&quot; +
                   &quot;&lt;/a>&quot; +
                   &quot;&lt;/li>&quot; +
                   &quot;&quot; +
                   &quot;&lt;/ul>&quot;;
                var continuemessage = &quot;Continue&quot;;

                // Initialize popup.
                $(document.body).addClass(&quot; , &quot;'&quot; , &quot;eupopup&quot; , &quot;'&quot; , &quot;);
                if ($(&quot;.eupopup&quot;).length > 0) {
                    $(document).euCookieLawPopup().init({
                        popupPosition: &quot; , &quot;'&quot; , &quot;bottom&quot; , &quot;'&quot; , &quot;,
                        popupTitle: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                        popupText: textmessage,
                        buttonContinueTitle: continuemessage,
                        buttonLearnmoreTitle: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                        compactStyle: true,
                    });
                }

            // Initialise the JS for the modal window which displays the policy versions.
            ActionsMod.init(&quot; , &quot;'&quot; , &quot;[data-action=&quot;view-guest&quot;]&quot; , &quot;'&quot; , &quot;);
        });
});

;

require([&quot; , &quot;'&quot; , &quot;theme_boost/footer-popover&quot; , &quot;'&quot; , &quot;], function(FooterPopover) {
    FooterPopover.init();
});
;

M.util.js_pending(&quot; , &quot;'&quot; , &quot;theme_boost/loader&quot; , &quot;'&quot; , &quot;);
require([&quot; , &quot;'&quot; , &quot;theme_boost/loader&quot; , &quot;'&quot; , &quot;], function() {
  M.util.js_complete(&quot; , &quot;'&quot; , &quot;theme_boost/loader&quot; , &quot;'&quot; , &quot;);
});
;

    require([&quot; , &quot;'&quot; , &quot;core_form/submit&quot; , &quot;'&quot; , &quot;], function(Submit) {
        Submit.init(&quot;loginbtn&quot;);
            Submit.init(&quot;loginguestbtn&quot;);
    });
;
M.util.js_pending(&quot; , &quot;'&quot; , &quot;core/notification&quot; , &quot;'&quot; , &quot;); require([&quot; , &quot;'&quot; , &quot;core/notification&quot; , &quot;'&quot; , &quot;], function(amd) {amd.init(1, []); M.util.js_complete(&quot; , &quot;'&quot; , &quot;core/notification&quot; , &quot;'&quot; , &quot;);});;
M.util.js_pending(&quot; , &quot;'&quot; , &quot;core/log&quot; , &quot;'&quot; , &quot;); require([&quot; , &quot;'&quot; , &quot;core/log&quot; , &quot;'&quot; , &quot;], function(amd) {amd.setConfig({&quot;level&quot;:&quot;warn&quot;}); M.util.js_complete(&quot; , &quot;'&quot; , &quot;core/log&quot; , &quot;'&quot; , &quot;);});;
M.util.js_pending(&quot; , &quot;'&quot; , &quot;core/page_global&quot; , &quot;'&quot; , &quot;); require([&quot; , &quot;'&quot; , &quot;core/page_global&quot; , &quot;'&quot; , &quot;], function(amd) {amd.init(); M.util.js_complete(&quot; , &quot;'&quot; , &quot;core/page_global&quot; , &quot;'&quot; , &quot;);});;
M.util.js_pending(&quot; , &quot;'&quot; , &quot;core/utility&quot; , &quot;'&quot; , &quot;); require([&quot; , &quot;'&quot; , &quot;core/utility&quot; , &quot;'&quot; , &quot;], function(amd) {M.util.js_complete(&quot; , &quot;'&quot; , &quot;core/utility&quot; , &quot;'&quot; , &quot;);});
    M.util.js_complete(&quot;core/first&quot;);
});
//]]&gt;



//&lt;![CDATA[
M.str = {&quot;moodle&quot;:{&quot;lastmodified&quot;:&quot;Last modified&quot;,&quot;name&quot;:&quot;Name&quot;,&quot;error&quot;:&quot;Error&quot;,&quot;info&quot;:&quot;Information&quot;,&quot;yes&quot;:&quot;Yes&quot;,&quot;no&quot;:&quot;No&quot;,&quot;ok&quot;:&quot;OK&quot;,&quot;cancel&quot;:&quot;Cancel&quot;,&quot;confirm&quot;:&quot;Confirm&quot;,&quot;areyousure&quot;:&quot;Are you sure?&quot;,&quot;closebuttontitle&quot;:&quot;Close&quot;,&quot;unknownerror&quot;:&quot;Unknown error&quot;,&quot;file&quot;:&quot;File&quot;,&quot;url&quot;:&quot;URL&quot;,&quot;collapseall&quot;:&quot;Collapse all&quot;,&quot;expandall&quot;:&quot;Expand all&quot;},&quot;repository&quot;:{&quot;type&quot;:&quot;Type&quot;,&quot;size&quot;:&quot;Size&quot;,&quot;invalidjson&quot;:&quot;Invalid JSON string&quot;,&quot;nofilesattached&quot;:&quot;No files attached&quot;,&quot;filepicker&quot;:&quot;File picker&quot;,&quot;logout&quot;:&quot;Logout&quot;,&quot;nofilesavailable&quot;:&quot;No files available&quot;,&quot;norepositoriesavailable&quot;:&quot;Sorry, none of your current repositories can return files in the required format.&quot;,&quot;fileexistsdialogheader&quot;:&quot;File exists&quot;,&quot;fileexistsdialog_editor&quot;:&quot;A file with that name has already been attached to the text you are editing.&quot;,&quot;fileexistsdialog_filemanager&quot;:&quot;A file with that name has already been attached&quot;,&quot;renameto&quot;:&quot;Rename to \&quot;{$a}\&quot;&quot;,&quot;referencesexist&quot;:&quot;There are {$a} links to this file&quot;,&quot;select&quot;:&quot;Select&quot;},&quot;admin&quot;:{&quot;confirmdeletecomments&quot;:&quot;Are you sure you want to delete the selected comment(s)?&quot;,&quot;confirmation&quot;:&quot;Confirmation&quot;},&quot;debug&quot;:{&quot;debuginfo&quot;:&quot;Debug info&quot;,&quot;line&quot;:&quot;Line&quot;,&quot;stacktrace&quot;:&quot;Stack trace&quot;},&quot;langconfig&quot;:{&quot;labelsep&quot;:&quot;:&quot;}};
//]]&gt;


//&lt;![CDATA[
(function() {Y.use(&quot;moodle-filter_glossary-autolinker&quot;,function() {M.filter_glossary.init_filter_autolinking({&quot;courseid&quot;:0});
});
M.util.help_popups.setup(Y);
 M.util.js_pending(&quot; , &quot;'&quot; , &quot;random65748643dc765150&quot; , &quot;'&quot; , &quot;); Y.on(&quot; , &quot;'&quot; , &quot;domready&quot; , &quot;'&quot; , &quot;, function() { M.util.js_complete(&quot;init&quot;);  M.util.js_complete(&quot; , &quot;'&quot; , &quot;random65748643dc765150&quot; , &quot;'&quot; , &quot;); });
})();
//]]&gt;


            
            
                Powered by Moodle
            
        
    
        
            
                
            
        
    This site will be reset in 37 mins 09 secs



/html[@class=&quot;yui3-js-enabled&quot;]If you continue browsing this website, you agree to our policies:Cookie statementPrivacy policyRules for guestsContinuex&quot;) or . = concat(&quot;
    Log in to the site | Mount Orange School
    
    

/** Required in order to fix style inclusion problems in IE with YUI **/

//&lt;![CDATA[
var M = {}; M.yui = {};
M.pageloadstarttime = new Date();
M.cfg = {&quot;wwwroot&quot;:&quot;https:\/\/school.moodledemo.net&quot;,&quot;homeurl&quot;:{},&quot;sesskey&quot;:&quot;h7aTgK4p2a&quot;,&quot;sessiontimeout&quot;:&quot;7200&quot;,&quot;sessiontimeoutwarning&quot;:1200,&quot;themerev&quot;:&quot;1702130560&quot;,&quot;slasharguments&quot;:1,&quot;theme&quot;:&quot;boost&quot;,&quot;iconsystemmodule&quot;:&quot;core\/icon_system_fontawesome&quot;,&quot;jsrev&quot;:&quot;1702130560&quot;,&quot;admin&quot;:&quot;admin&quot;,&quot;svgicons&quot;:true,&quot;usertimezone&quot;:&quot;UTC+1&quot;,&quot;courseId&quot;:1,&quot;courseContextId&quot;:2,&quot;contextid&quot;:1,&quot;contextInstanceId&quot;:0,&quot;langrev&quot;:1702134037,&quot;templaterev&quot;:&quot;1702130560&quot;};var yui1ConfigFn = function(me) {if(/-skin|reset|fonts|grids|base/.test(me.name)){me.type=&quot; , &quot;'&quot; , &quot;css&quot; , &quot;'&quot; , &quot;;me.path=me.path.replace(/\.js/,&quot; , &quot;'&quot; , &quot;.css&quot; , &quot;'&quot; , &quot;);me.path=me.path.replace(/\/yui2-skin/,&quot; , &quot;'&quot; , &quot;/assets/skins/sam/yui2-skin&quot; , &quot;'&quot; , &quot;)}};
var yui2ConfigFn = function(me) {var parts=me.name.replace(/^moodle-/,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;).split(&quot; , &quot;'&quot; , &quot;-&quot; , &quot;'&quot; , &quot;),component=parts.shift(),module=parts[0],min=&quot; , &quot;'&quot; , &quot;-min&quot; , &quot;'&quot; , &quot;;if(/-(skin|core)$/.test(me.name)){parts.pop();me.type=&quot; , &quot;'&quot; , &quot;css&quot; , &quot;'&quot; , &quot;;min=&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;}
if(module){var filename=parts.join(&quot; , &quot;'&quot; , &quot;-&quot; , &quot;'&quot; , &quot;);me.path=component+&quot; , &quot;'&quot; , &quot;/&quot; , &quot;'&quot; , &quot;+module+&quot; , &quot;'&quot; , &quot;/&quot; , &quot;'&quot; , &quot;+filename+min+&quot; , &quot;'&quot; , &quot;.&quot; , &quot;'&quot; , &quot;+me.type}else{me.path=component+&quot; , &quot;'&quot; , &quot;/&quot; , &quot;'&quot; , &quot;+component+&quot; , &quot;'&quot; , &quot;.&quot; , &quot;'&quot; , &quot;+me.type}};
YUI_config = {&quot;debug&quot;:false,&quot;base&quot;:&quot;https:\/\/school.moodledemo.net\/lib\/yuilib\/3.18.1\/&quot;,&quot;comboBase&quot;:&quot;https:\/\/school.moodledemo.net\/theme\/yui_combo.php?&quot;,&quot;combine&quot;:true,&quot;filter&quot;:null,&quot;insertBefore&quot;:&quot;firstthemesheet&quot;,&quot;groups&quot;:{&quot;yui2&quot;:{&quot;base&quot;:&quot;https:\/\/school.moodledemo.net\/lib\/yuilib\/2in3\/2.9.0\/build\/&quot;,&quot;comboBase&quot;:&quot;https:\/\/school.moodledemo.net\/theme\/yui_combo.php?&quot;,&quot;combine&quot;:true,&quot;ext&quot;:false,&quot;root&quot;:&quot;2in3\/2.9.0\/build\/&quot;,&quot;patterns&quot;:{&quot;yui2-&quot;:{&quot;group&quot;:&quot;yui2&quot;,&quot;configFn&quot;:yui1ConfigFn}}},&quot;moodle&quot;:{&quot;name&quot;:&quot;moodle&quot;,&quot;base&quot;:&quot;https:\/\/school.moodledemo.net\/theme\/yui_combo.php?m\/1702130560\/&quot;,&quot;combine&quot;:true,&quot;comboBase&quot;:&quot;https:\/\/school.moodledemo.net\/theme\/yui_combo.php?&quot;,&quot;ext&quot;:false,&quot;root&quot;:&quot;m\/1702130560\/&quot;,&quot;patterns&quot;:{&quot;moodle-&quot;:{&quot;group&quot;:&quot;moodle&quot;,&quot;configFn&quot;:yui2ConfigFn}},&quot;filter&quot;:null,&quot;modules&quot;:{&quot;moodle-core-formchangechecker&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;event-focus&quot;,&quot;moodle-core-event&quot;]},&quot;moodle-core-blocks&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;io&quot;,&quot;dom&quot;,&quot;dd&quot;,&quot;dd-scroll&quot;,&quot;moodle-core-dragdrop&quot;,&quot;moodle-core-notification&quot;]},&quot;moodle-core-maintenancemodetimer&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;]},&quot;moodle-core-event&quot;:{&quot;requires&quot;:[&quot;event-custom&quot;]},&quot;moodle-core-dragdrop&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;io&quot;,&quot;dom&quot;,&quot;dd&quot;,&quot;event-key&quot;,&quot;event-focus&quot;,&quot;moodle-core-notification&quot;]},&quot;moodle-core-lockscroll&quot;:{&quot;requires&quot;:[&quot;plugin&quot;,&quot;base-build&quot;]},&quot;moodle-core-handlebars&quot;:{&quot;condition&quot;:{&quot;trigger&quot;:&quot;handlebars&quot;,&quot;when&quot;:&quot;after&quot;}},&quot;moodle-core-chooserdialogue&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;panel&quot;,&quot;moodle-core-notification&quot;]},&quot;moodle-core-notification&quot;:{&quot;requires&quot;:[&quot;moodle-core-notification-dialogue&quot;,&quot;moodle-core-notification-alert&quot;,&quot;moodle-core-notification-confirm&quot;,&quot;moodle-core-notification-exception&quot;,&quot;moodle-core-notification-ajaxexception&quot;]},&quot;moodle-core-notification-dialogue&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;panel&quot;,&quot;escape&quot;,&quot;event-key&quot;,&quot;dd-plugin&quot;,&quot;moodle-core-widget-focusafterclose&quot;,&quot;moodle-core-lockscroll&quot;]},&quot;moodle-core-notification-alert&quot;:{&quot;requires&quot;:[&quot;moodle-core-notification-dialogue&quot;]},&quot;moodle-core-notification-confirm&quot;:{&quot;requires&quot;:[&quot;moodle-core-notification-dialogue&quot;]},&quot;moodle-core-notification-exception&quot;:{&quot;requires&quot;:[&quot;moodle-core-notification-dialogue&quot;]},&quot;moodle-core-notification-ajaxexception&quot;:{&quot;requires&quot;:[&quot;moodle-core-notification-dialogue&quot;]},&quot;moodle-core-actionmenu&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;event&quot;,&quot;node-event-simulate&quot;]},&quot;moodle-core_availability-form&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;event-delegate&quot;,&quot;panel&quot;,&quot;moodle-core-notification-dialogue&quot;,&quot;json&quot;]},&quot;moodle-backup-backupselectall&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;event&quot;,&quot;node-event-simulate&quot;,&quot;anim&quot;]},&quot;moodle-course-util&quot;:{&quot;requires&quot;:[&quot;node&quot;],&quot;use&quot;:[&quot;moodle-course-util-base&quot;],&quot;submodules&quot;:{&quot;moodle-course-util-base&quot;:{},&quot;moodle-course-util-section&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;moodle-course-util-base&quot;]},&quot;moodle-course-util-cm&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;moodle-course-util-base&quot;]}}},&quot;moodle-course-categoryexpander&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;event-key&quot;]},&quot;moodle-course-management&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;io-base&quot;,&quot;moodle-core-notification-exception&quot;,&quot;json-parse&quot;,&quot;dd-constrain&quot;,&quot;dd-proxy&quot;,&quot;dd-drop&quot;,&quot;dd-delegate&quot;,&quot;node-event-delegate&quot;]},&quot;moodle-course-dragdrop&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;io&quot;,&quot;dom&quot;,&quot;dd&quot;,&quot;dd-scroll&quot;,&quot;moodle-core-dragdrop&quot;,&quot;moodle-core-notification&quot;,&quot;moodle-course-coursebase&quot;,&quot;moodle-course-util&quot;]},&quot;moodle-form-dateselector&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;overlay&quot;,&quot;calendar&quot;]},&quot;moodle-form-shortforms&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;base&quot;,&quot;selector-css3&quot;,&quot;moodle-core-event&quot;]},&quot;moodle-question-preview&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;dom&quot;,&quot;event-delegate&quot;,&quot;event-key&quot;,&quot;core_question_engine&quot;]},&quot;moodle-question-searchform&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;]},&quot;moodle-question-chooser&quot;:{&quot;requires&quot;:[&quot;moodle-core-chooserdialogue&quot;]},&quot;moodle-availability_completion-form&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;moodle-core_availability-form&quot;]},&quot;moodle-availability_date-form&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;io&quot;,&quot;moodle-core_availability-form&quot;]},&quot;moodle-availability_grade-form&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;moodle-core_availability-form&quot;]},&quot;moodle-availability_group-form&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;moodle-core_availability-form&quot;]},&quot;moodle-availability_grouping-form&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;moodle-core_availability-form&quot;]},&quot;moodle-availability_profile-form&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;moodle-core_availability-form&quot;]},&quot;moodle-mod_assign-history&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;transition&quot;]},&quot;moodle-mod_quiz-modform&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;]},&quot;moodle-mod_quiz-util&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;moodle-core-actionmenu&quot;],&quot;use&quot;:[&quot;moodle-mod_quiz-util-base&quot;],&quot;submodules&quot;:{&quot;moodle-mod_quiz-util-base&quot;:{},&quot;moodle-mod_quiz-util-slot&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;moodle-mod_quiz-util-base&quot;]},&quot;moodle-mod_quiz-util-page&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;moodle-mod_quiz-util-base&quot;]}}},&quot;moodle-mod_quiz-questionchooser&quot;:{&quot;requires&quot;:[&quot;moodle-core-chooserdialogue&quot;,&quot;moodle-mod_quiz-util&quot;,&quot;querystring-parse&quot;]},&quot;moodle-mod_quiz-dragdrop&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;io&quot;,&quot;dom&quot;,&quot;dd&quot;,&quot;dd-scroll&quot;,&quot;moodle-core-dragdrop&quot;,&quot;moodle-core-notification&quot;,&quot;moodle-mod_quiz-quizbase&quot;,&quot;moodle-mod_quiz-util-base&quot;,&quot;moodle-mod_quiz-util-page&quot;,&quot;moodle-mod_quiz-util-slot&quot;,&quot;moodle-course-util&quot;]},&quot;moodle-mod_quiz-quizbase&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;]},&quot;moodle-mod_quiz-toolboxes&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;event-key&quot;,&quot;io&quot;,&quot;moodle-mod_quiz-quizbase&quot;,&quot;moodle-mod_quiz-util-slot&quot;,&quot;moodle-core-notification-ajaxexception&quot;]},&quot;moodle-mod_quiz-autosave&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;event&quot;,&quot;event-valuechange&quot;,&quot;node-event-delegate&quot;,&quot;io-form&quot;]},&quot;moodle-message_airnotifier-toolboxes&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;io&quot;]},&quot;moodle-filter_glossary-autolinker&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;io-base&quot;,&quot;json-parse&quot;,&quot;event-delegate&quot;,&quot;overlay&quot;,&quot;moodle-core-event&quot;,&quot;moodle-core-notification-alert&quot;,&quot;moodle-core-notification-exception&quot;,&quot;moodle-core-notification-ajaxexception&quot;]},&quot;moodle-editor_atto-rangy&quot;:{&quot;requires&quot;:[]},&quot;moodle-editor_atto-editor&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;transition&quot;,&quot;io&quot;,&quot;overlay&quot;,&quot;escape&quot;,&quot;event&quot;,&quot;event-simulate&quot;,&quot;event-custom&quot;,&quot;node-event-html5&quot;,&quot;node-event-simulate&quot;,&quot;yui-throttle&quot;,&quot;moodle-core-notification-dialogue&quot;,&quot;moodle-editor_atto-rangy&quot;,&quot;handlebars&quot;,&quot;timers&quot;,&quot;querystring-stringify&quot;]},&quot;moodle-editor_atto-plugin&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;base&quot;,&quot;escape&quot;,&quot;event&quot;,&quot;event-outside&quot;,&quot;handlebars&quot;,&quot;event-custom&quot;,&quot;timers&quot;,&quot;moodle-editor_atto-menu&quot;]},&quot;moodle-editor_atto-menu&quot;:{&quot;requires&quot;:[&quot;moodle-core-notification-dialogue&quot;,&quot;node&quot;,&quot;event&quot;,&quot;event-custom&quot;]},&quot;moodle-report_eventlist-eventfilter&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;event&quot;,&quot;node&quot;,&quot;node-event-delegate&quot;,&quot;datatable&quot;,&quot;autocomplete&quot;,&quot;autocomplete-filters&quot;]},&quot;moodle-report_loglive-fetchlogs&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;event&quot;,&quot;node&quot;,&quot;io&quot;,&quot;node-event-delegate&quot;]},&quot;moodle-gradereport_history-userselector&quot;:{&quot;requires&quot;:[&quot;escape&quot;,&quot;event-delegate&quot;,&quot;event-key&quot;,&quot;handlebars&quot;,&quot;io-base&quot;,&quot;json-parse&quot;,&quot;moodle-core-notification-dialogue&quot;]},&quot;moodle-qbank_editquestion-chooser&quot;:{&quot;requires&quot;:[&quot;moodle-core-chooserdialogue&quot;]},&quot;moodle-tool_lp-dragdrop-reorder&quot;:{&quot;requires&quot;:[&quot;moodle-core-dragdrop&quot;]},&quot;moodle-assignfeedback_editpdf-editor&quot;:{&quot;requires&quot;:[&quot;base&quot;,&quot;event&quot;,&quot;node&quot;,&quot;io&quot;,&quot;graphics&quot;,&quot;json&quot;,&quot;event-move&quot;,&quot;event-resize&quot;,&quot;transition&quot;,&quot;querystring-stringify-simple&quot;,&quot;moodle-core-notification-dialog&quot;,&quot;moodle-core-notification-alert&quot;,&quot;moodle-core-notification-warning&quot;,&quot;moodle-core-notification-exception&quot;,&quot;moodle-core-notification-ajaxexception&quot;]},&quot;moodle-atto_accessibilitychecker-button&quot;:{&quot;requires&quot;:[&quot;color-base&quot;,&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_accessibilityhelper-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_align-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_bold-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_charmap-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_clear-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_collapse-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_emojipicker-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_emoticon-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_equation-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;,&quot;moodle-core-event&quot;,&quot;io&quot;,&quot;event-valuechange&quot;,&quot;tabview&quot;,&quot;array-extras&quot;]},&quot;moodle-atto_h5p-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_html-beautify&quot;:{},&quot;moodle-atto_html-codemirror&quot;:{&quot;requires&quot;:[&quot;moodle-atto_html-codemirror-skin&quot;]},&quot;moodle-atto_html-button&quot;:{&quot;requires&quot;:[&quot;promise&quot;,&quot;moodle-editor_atto-plugin&quot;,&quot;moodle-atto_html-beautify&quot;,&quot;moodle-atto_html-codemirror&quot;,&quot;event-valuechange&quot;]},&quot;moodle-atto_image-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_indent-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_italic-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_link-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_managefiles-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_managefiles-usedfiles&quot;:{&quot;requires&quot;:[&quot;node&quot;,&quot;escape&quot;]},&quot;moodle-atto_media-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;,&quot;moodle-form-shortforms&quot;]},&quot;moodle-atto_noautolink-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_orderedlist-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_recordrtc-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;,&quot;moodle-atto_recordrtc-recording&quot;]},&quot;moodle-atto_recordrtc-recording&quot;:{&quot;requires&quot;:[&quot;moodle-atto_recordrtc-button&quot;]},&quot;moodle-atto_rtl-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_strike-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_subscript-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_superscript-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_table-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;,&quot;moodle-editor_atto-menu&quot;,&quot;event&quot;,&quot;event-valuechange&quot;]},&quot;moodle-atto_title-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_underline-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_undo-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]},&quot;moodle-atto_unorderedlist-button&quot;:{&quot;requires&quot;:[&quot;moodle-editor_atto-plugin&quot;]}}},&quot;gallery&quot;:{&quot;name&quot;:&quot;gallery&quot;,&quot;base&quot;:&quot;https:\/\/school.moodledemo.net\/lib\/yuilib\/gallery\/&quot;,&quot;combine&quot;:true,&quot;comboBase&quot;:&quot;https:\/\/school.moodledemo.net\/theme\/yui_combo.php?&quot;,&quot;ext&quot;:false,&quot;root&quot;:&quot;gallery\/1702130560\/&quot;,&quot;patterns&quot;:{&quot;gallery-&quot;:{&quot;group&quot;:&quot;gallery&quot;}}}},&quot;modules&quot;:{&quot;core_filepicker&quot;:{&quot;name&quot;:&quot;core_filepicker&quot;,&quot;fullpath&quot;:&quot;https:\/\/school.moodledemo.net\/lib\/javascript.php\/1702130560\/repository\/filepicker.js&quot;,&quot;requires&quot;:[&quot;base&quot;,&quot;node&quot;,&quot;node-event-simulate&quot;,&quot;json&quot;,&quot;async-queue&quot;,&quot;io-base&quot;,&quot;io-upload-iframe&quot;,&quot;io-form&quot;,&quot;yui2-treeview&quot;,&quot;panel&quot;,&quot;cookie&quot;,&quot;datatable&quot;,&quot;datatable-sort&quot;,&quot;resize-plugin&quot;,&quot;dd-plugin&quot;,&quot;escape&quot;,&quot;moodle-core_filepicker&quot;,&quot;moodle-core-notification-dialogue&quot;]},&quot;core_comment&quot;:{&quot;name&quot;:&quot;core_comment&quot;,&quot;fullpath&quot;:&quot;https:\/\/school.moodledemo.net\/lib\/javascript.php\/1702130560\/comment\/comment.js&quot;,&quot;requires&quot;:[&quot;base&quot;,&quot;io-base&quot;,&quot;node&quot;,&quot;json&quot;,&quot;yui2-animation&quot;,&quot;overlay&quot;,&quot;escape&quot;]}},&quot;logInclude&quot;:[],&quot;logExclude&quot;:[],&quot;logLevel&quot;:null};
M.yui.loader = {modules: {}};

//]]&gt;



    
#katalon{font-family:monospace;font-size:13px;background-color:rgba(0,0,0,.7);position:fixed;top:0;left:0;right:0;display:block;z-index:999999999;line-height: normal} #katalon div{padding:0;margin:0;color:#fff;} #katalon kbd{display:inline-block;padding:3px 5px;font:13px Consolas,&quot;Liberation Mono&quot;,Menlo,Courier,monospace;line-height:10px;color:#555;vertical-align:middle;background-color:#fcfcfc;border:1px solid #ccc;border-bottom-color:#bbb;border-radius:3px;box-shadow:inset 0 -1px 0 #bbb;font-weight: bold} div#katalon-spy_elementInfoDiv {color: lightblue; padding: 0px 5px 5px} div#katalon-spy_instructionDiv {padding: 5px 5px 2.5px}MathJax.Hub.Config({&#xd;
    config: [&quot;Accessible.js&quot;, &quot;Safe.js&quot;],&#xd;
    errorSettings: { message: [&quot;!&quot;] },&#xd;
    skipStartupTypeset: true,&#xd;
    messageStyle: &quot;none&quot;&#xd;
});&#xd;






    
    Skip to main content



//&lt;![CDATA[
document.body.className += &quot; , &quot;'&quot; , &quot; jsenabled&quot; , &quot;'&quot; , &quot;;
//]]&gt;




    
        
            
                
                
                    
                    
        Log in to Mount Orange School
    
        
        document.getElementById(&quot; , &quot;'&quot; , &quot;anchor&quot; , &quot;'&quot; , &quot;).value = location.hash;
        
        
            
                    Username
            
            
        
        
            Password
            
        
        
            Log in
        
        
            Lost password?
        
    
        
        
            Is this your first time here?
            To explore this site, log in with the role of:Student - with the username student and password moodleTeacher - with the username teacher and password moodleManager - with the username manager and password moodleParent - with the username parent and password moodlePrivacy officer - with the username privacyofficer and password moodleOr explore one of our many Other accounts
        
        
        Some courses may allow guest access
        
            
            
            
            Access as a guest
        
    
    
            
                
                
                        
                
                            
                
                
                                
                                    
                                        
                                            
                                            English (United States) ‎(en_us)‎
                                                
                                            
                                        
                                            
                                                                                                
                                                 አማርኛ ‎(am)‎
                                        
                                                                                                
                                                Afaan Oromoo ‎(om)‎
                                        
                                                                                                
                                                Afrikaans ‎(af)‎
                                        
                                                                                                
                                                Aragonés ‎(an)‎
                                        
                                                                                                
                                                Aranés ‎(oc_es)‎
                                        
                                                                                                
                                                Asturianu ‎(ast)‎
                                        
                                                                                                
                                                Azərbaycanca ‎(az)‎
                                        
                                                                                                
                                                Bahasa Indonesia ‎(id)‎
                                        
                                                                                                
                                                Bahasa Melayu ‎(ms)‎
                                        
                                                                                                
                                                Bamanankan ‎(bm)‎
                                        
                                                                                                
                                                Bislama ‎(bi)‎
                                        
                                                                                                
                                                Bosanski ‎(bs)‎
                                        
                                                                                                
                                                Brezhoneg ‎(br)‎
                                        
                                                                                                
                                                Català ‎(ca)‎
                                        
                                                                                                
                                                Català (Valencià) ‎(ca_valencia)‎
                                        
                                                                                                
                                                Čeština ‎(cs)‎
                                        
                                                                                                
                                                Crnogorski ‎(mis)‎
                                        
                                                                                                
                                                Cymraeg ‎(cy)‎
                                        
                                                                                                
                                                Dansk ‎(da)‎
                                        
                                                                                                
                                                Davvisámegiella ‎(se)‎
                                        
                                                                                                
                                                Deutsch ‎(de)‎
                                        
                                                                                                
                                                Dolnoserbski ‎(dsb)‎
                                        
                                                                                                
                                                Ebon ‎(mh)‎
                                        
                                                                                                
                                                eesti ‎(et)‎
                                        
                                                                                                
                                                English ‎(en)‎
                                        
                                                                                                
                                                English (United States) ‎(en_us)‎
                                        
                                                                                                
                                                Español - Internacional ‎(es)‎
                                        
                                                                                                
                                                Español - México ‎(es_mx)‎
                                        
                                                                                                
                                                Español - Venezuela ‎(es_ve)‎
                                        
                                                                                                
                                                Esperanto ‎(eo)‎
                                        
                                                                                                
                                                Euskara ‎(eu)‎
                                        
                                                                                                
                                                Èʋegbe ‎(ee)‎
                                        
                                                                                                
                                                Filipino ‎(fil)‎
                                        
                                                                                                
                                                Finlandssvenska ‎(sv_fi)‎
                                        
                                                                                                
                                                Føroyskt ‎(fo)‎
                                        
                                                                                                
                                                Français (Canada) ‎(fr_ca)‎
                                        
                                                                                                
                                                Français ‎(fr)‎
                                        
                                                                                                
                                                Gaeilge ‎(ga)‎
                                        
                                                                                                
                                                Gàidhlig ‎(gd)‎
                                        
                                                                                                
                                                Galego ‎(gl)‎
                                        
                                                                                                
                                                Gascon ‎(oc_gsc)‎
                                        
                                                                                                
                                                Hausa ‎(ha)‎
                                        
                                                                                                
                                                Hrvatski ‎(hr)‎
                                        
                                                                                                
                                                ʻŌlelo Hawaiʻi ‎(haw)‎
                                        
                                                                                                
                                                Igbo ‎(ig)‎
                                        
                                                                                                
                                                isiZulu ‎(zu)‎
                                        
                                                                                                
                                                Íslenska ‎(is)‎
                                        
                                                                                                
                                                Italiano ‎(it)‎
                                        
                                                                                                
                                                Kalaallisut ‎(kl)‎
                                        
                                                                                                
                                                Kinyarwanda ‎(rw)‎
                                        
                                                                                                
                                                Kiswahili ‎(sw)‎
                                        
                                                                                                
                                                Kreyòl Ayisyen ‎(hat)‎
                                        
                                                                                                
                                                Kurmanji ‎(kmr)‎
                                        
                                                                                                
                                                Latin ‎(la)‎
                                        
                                                                                                
                                                Latviešu ‎(lv)‎
                                        
                                                                                                
                                                Lëtzebuergesch ‎(lb)‎
                                        
                                                                                                
                                                Lietuvių ‎(lt)‎
                                        
                                                                                                
                                                Lulesamisk ‎(smj)‎
                                        
                                                                                                
                                                magyar ‎(hu)‎
                                        
                                                                                                
                                                Malagasy ‎(mg)‎
                                        
                                                                                                
                                                Māori - Tainui ‎(mi_tn)‎
                                        
                                                                                                
                                                Māori - Waikato ‎(mi_wwow)‎
                                        
                                                                                                
                                                Māori Te Reo ‎(mi)‎
                                        
                                                                                                
                                                Mongolian ‎(mn_mong)‎
                                        
                                                                                                
                                                Nederlands ‎(nl)‎
                                        
                                                                                                
                                                Norsk - nynorsk ‎(nn)‎
                                        
                                                                                                
                                                Norsk ‎(no_gr)‎
                                        
                                                                                                
                                                Norsk ‎(no)‎
                                        
                                                                                                
                                                O&quot; , &quot;'&quot; , &quot;zbekcha ‎(uz)‎
                                        
                                                                                                
                                                Occitan-Lengadocian ‎(oc_lnc)‎
                                        
                                                                                                
                                                Pidgin ‎(pcm)‎
                                        
                                                                                                
                                                Polski ‎(pl)‎
                                        
                                                                                                
                                                Português - Brasil ‎(pt_br)‎
                                        
                                                                                                
                                                Português - Portugal ‎(pt)‎
                                        
                                                                                                
                                                Română ‎(ro)‎
                                        
                                                                                                
                                                Romansh Sursilvan ‎(rm_surs)‎
                                        
                                                                                                
                                                Samoan ‎(sm)‎
                                        
                                                                                                
                                                Setswana ‎(tn)‎
                                        
                                                                                                
                                                Shqip ‎(sq)‎
                                        
                                                                                                
                                                Sicilianu ‎(scn)‎
                                        
                                                                                                
                                                Ślōnski ‎(szl)‎
                                        
                                                                                                
                                                Slovenčina ‎(sk)‎
                                        
                                                                                                
                                                Slovenščina ‎(sl)‎
                                        
                                                                                                
                                                Soomaali ‎(so)‎
                                        
                                                                                                
                                                Sørsamisk ‎(sma)‎
                                        
                                                                                                
                                                Srpski ‎(sr_lt)‎
                                        
                                                                                                
                                                Suomi ‎(fi)‎
                                        
                                                                                                
                                                Suomi+ ‎(fi_co)‎
                                        
                                                                                                
                                                Svenska ‎(sv)‎
                                        
                                                                                                
                                                Tagalog ‎(tl)‎
                                        
                                                                                                
                                                Tamil ‎(ta)‎
                                        
                                                                                                
                                                Taqbaylit ‎(kab)‎
                                        
                                                                                                
                                                Thai ‎(th)‎
                                        
                                                                                                
                                                Tibetan ‎(xct)‎
                                        
                                                                                                
                                                Tok Pisin ‎(tpi)‎
                                        
                                                                                                
                                                Tongan ‎(to)‎
                                        
                                                                                                
                                                Türkçe ‎(tr)‎
                                        
                                                                                                
                                                Turkmen ‎(tk)‎
                                        
                                                                                                
                                                Uyghur - latin ‎(ug_lt)‎
                                        
                                                                                                
                                                VakaViti ‎(fj)‎
                                        
                                                                                                
                                                Vietnamese ‎(vi)‎
                                        
                                                                                                
                                                Wolof ‎(wo)‎
                                        
                                                                                                
                                                Ελληνικά ‎(el)‎
                                        
                                                                                                
                                                Башҡорт теле ‎(ba)‎
                                        
                                                                                                
                                                Беларуская ‎(be)‎
                                        
                                                                                                
                                                Български ‎(bg)‎
                                        
                                                                                                
                                                Кыргызча ‎(ky)‎
                                        
                                                                                                
                                                Қазақша ‎(kk)‎
                                        
                                                                                                
                                                Македонски ‎(mk)‎
                                        
                                                                                                
                                                Монгол ‎(mn)‎
                                        
                                                                                                
                                                Русский ‎(ru)‎
                                        
                                                                                                
                                                Српски ‎(sr_cr)‎
                                        
                                                                                                
                                                Татар ‎(tt)‎
                                        
                                                                                                
                                                Тоҷикӣ ‎(tg)‎
                                        
                                                                                                
                                                Українська ‎(uk)‎
                                        
                                                                                                
                                                ქართული ‎(ka)‎
                                        
                                                                                                
                                                Հայերեն ‎(hy)‎
                                        
                                                                                                
                                                עברית ‎(he)‎
                                        
                                                                                                
                                                ئۇيغۇرچە ‎(ug_ug)‎
                                        
                                                                                                
                                                اردو ‎(ur)‎
                                        
                                                                                                
                                                العربية ‎(ar)‎
                                        
                                                                                                
                                                دری ‎(prs)‎
                                        
                                                                                                
                                                سۆرانی ‎(ckb)‎
                                        
                                                                                                
                                                فارسی ‎(fa)‎
                                        
                                                                                                
                                                لیسي ‎(ps)‎
                                        
                                                                                                
                                                ދިވެހި ‎(dv)‎
                                        
                                                                                                
                                                ⵜⴰⵎⴰⵣⵉⵖⵜ ‎(zgh)‎
                                        
                                                                                                
                                                ትግርኛ ‎(ti)‎
                                        
                                                                                                
                                                नेपाली ‎(ne)‎
                                        
                                                                                                
                                                मराठी ‎(mr)‎
                                        
                                                                                                
                                                हिंदी ‎(hi)‎
                                        
                                                                                                
                                                বাংলা ‎(bn)‎
                                        
                                                                                                
                                                ਪੰਜਾਬੀ ‎(pan)‎
                                        
                                                                                                
                                                ગુજરાતી ‎(gu)‎
                                        
                                                                                                
                                                ଓଡ଼ିଆ ‎(or)‎
                                        
                                                                                                
                                                தமிழ் ‎(ta_lk)‎
                                        
                                                                                                
                                                తెలుగు  ‎(te)‎
                                        
                                                                                                
                                                ಕನ್ನಡ ‎(kn)‎
                                        
                                                                                                
                                                മലയാളം ‎(ml)‎
                                        
                                                                                                
                                                සිංහල ‎(si)‎
                                        
                                                                                                
                                                ລາວ ‎(lo)‎
                                        
                                                                                                
                                                རྫོང་ཁ ‎(dz)‎
                                        
                                                                                                
                                                ဗမာစာ ‎(my)‎
                                        
                                                                                                
                                                ខ្មែរ ‎(km)‎
                                        
                                                                                                
                                                한국어 ‎(ko)‎
                                        
                                                                                                
                                                日本語 ‎(ja)‎
                                        
                                                                                                
                                                正體中文 ‎(zh_tw)‎
                                        
                                                                                                
                                                简体中文 ‎(zh_cn)‎
                                        
                                            
                                    
                                
                
                        
                
                
            
            
        Cookies notice
    

                    
                
                
            
        
    
    
    
        
            
                
            
        
        
            
                
                    You are not logged in.
                
                
                
    
                Data retention summaryGet the mobile appPolicies
                

window.onload = () => {
    let demoresetcountdown = document.createElement(&quot; , &quot;'&quot; , &quot;div&quot; , &quot;'&quot; , &quot;);
    document.getElementById(&quot; , &quot;'&quot; , &quot;page-footer&quot; , &quot;'&quot; , &quot;).appendChild(demoresetcountdown);
    demoresetcountdown.textContent = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
    demoresetcountdown.style.cssText = &quot; , &quot;'&quot; , &quot;z-index:99999999;position:fixed;bottom:0rem;right:1rem;padding:1px 0.5rem;&quot; , &quot;'&quot; , &quot;;
    setInterval(() => {
        let now = Math.floor(new Date().getTime() / 1000);
        let nextWholeHour = now - (now % 3600) + 3600;
        let display = new Date(1970, 0, 1);
        let remainingSecs = nextWholeHour - now;

        display.setSeconds(remainingSecs);

        let secs = display.getSeconds();
        let mins = display.getMinutes();
        let hours = Math.floor((remainingSecs - mins * 3600 - secs * 60) / 60 / 60);
        let text = &quot; , &quot;'&quot; , &quot;This site will be reset in &quot; , &quot;'&quot; , &quot;;

        if (hours > 0) {
            text += (&quot; , &quot;'&quot; , &quot;0&quot; , &quot;'&quot; , &quot; + hours).slice(-2) + &quot; , &quot;'&quot; , &quot; hours &quot; , &quot;'&quot; , &quot;;
        }

        text += (&quot; , &quot;'&quot; , &quot;0&quot; , &quot;'&quot; , &quot; + mins).slice(-2) + &quot; , &quot;'&quot; , &quot; mins &quot; , &quot;'&quot; , &quot; + (&quot; , &quot;'&quot; , &quot;0&quot; , &quot;'&quot; , &quot; + secs).slice(-2) + &quot; , &quot;'&quot; , &quot; secs&quot; , &quot;'&quot; , &quot;;

        demoresetcountdown.textContent = text;

        if (remainingSecs &lt; 300) {
            demoresetcountdown.style.cssText += &quot; , &quot;'&quot; , &quot;background-color:red;color:white;font-weight:bold&quot; , &quot;'&quot; , &quot;;
        } else {
            demoresetcountdown.style.cssText += &quot; , &quot;'&quot; , &quot;background-color:black;color:white;font-weight:normal&quot; , &quot;'&quot; , &quot;;
        }
    }, 1000);
}

//&lt;![CDATA[
var require = {
    baseUrl : &quot; , &quot;'&quot; , &quot;https://school.moodledemo.net/lib/requirejs.php/1702130560/&quot; , &quot;'&quot; , &quot;,
    // We only support AMD modules with an explicit define() statement.
    enforceDefine: true,
    skipDataMain: true,
    waitSeconds : 0,

    paths: {
        jquery: &quot; , &quot;'&quot; , &quot;https://school.moodledemo.net/lib/javascript.php/1702130560/lib/jquery/jquery-3.7.1.min&quot; , &quot;'&quot; , &quot;,
        jqueryui: &quot; , &quot;'&quot; , &quot;https://school.moodledemo.net/lib/javascript.php/1702130560/lib/jquery/ui-1.13.2/jquery-ui.min&quot; , &quot;'&quot; , &quot;,
        jqueryprivate: &quot; , &quot;'&quot; , &quot;https://school.moodledemo.net/lib/javascript.php/1702130560/lib/requirejs/jquery-private&quot; , &quot;'&quot; , &quot;
    },

    // Custom jquery config map.
    map: {
      // &quot; , &quot;'&quot; , &quot;*&quot; , &quot;'&quot; , &quot; means all modules will get &quot; , &quot;'&quot; , &quot;jqueryprivate&quot; , &quot;'&quot; , &quot;
      // for their &quot; , &quot;'&quot; , &quot;jquery&quot; , &quot;'&quot; , &quot; dependency.
      &quot; , &quot;'&quot; , &quot;*&quot; , &quot;'&quot; , &quot;: { jquery: &quot; , &quot;'&quot; , &quot;jqueryprivate&quot; , &quot;'&quot; , &quot; },
      // Stub module for &quot; , &quot;'&quot; , &quot;process&quot; , &quot;'&quot; , &quot;. This is a workaround for a bug in MathJax (see MDL-60458).
      &quot; , &quot;'&quot; , &quot;*&quot; , &quot;'&quot; , &quot;: { process: &quot; , &quot;'&quot; , &quot;core/first&quot; , &quot;'&quot; , &quot; },

      // &quot; , &quot;'&quot; , &quot;jquery-private&quot; , &quot;'&quot; , &quot; wants the real jQuery module
      // though. If this line was not here, there would
      // be an unresolvable cyclic dependency.
      jqueryprivate: { jquery: &quot; , &quot;'&quot; , &quot;jquery&quot; , &quot;'&quot; , &quot; }
    }
};

//]]&gt;



//&lt;![CDATA[
M.util.js_pending(&quot;core/first&quot;);
require([&quot; , &quot;'&quot; , &quot;core/first&quot; , &quot;'&quot; , &quot;], function() {
require([&quot; , &quot;'&quot; , &quot;core/prefetch&quot; , &quot;'&quot; , &quot;])
;
M.util.js_pending(&quot; , &quot;'&quot; , &quot;filter_mathjaxloader/loader&quot; , &quot;'&quot; , &quot;); require([&quot; , &quot;'&quot; , &quot;filter_mathjaxloader/loader&quot; , &quot;'&quot; , &quot;], function(amd) {amd.configure({&quot;mathjaxconfig&quot;:&quot;MathJax.Hub.Config({\r\n    config: [\&quot;Accessible.js\&quot;, \&quot;Safe.js\&quot;],\r\n    errorSettings: { message: [\&quot;!\&quot;] },\r\n    skipStartupTypeset: true,\r\n    messageStyle: \&quot;none\&quot;\r\n});\r\n&quot;,&quot;lang&quot;:&quot;en&quot;}); M.util.js_complete(&quot; , &quot;'&quot; , &quot;filter_mathjaxloader/loader&quot; , &quot;'&quot; , &quot;);});;
require([&quot;media_videojs/loader&quot;], function(loader) {
    loader.setUp(&quot; , &quot;'&quot; , &quot;en-GB&quot; , &quot;'&quot; , &quot;);
});;


require([&quot; , &quot;'&quot; , &quot;jquery&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;tool_policy/jquery-eu-cookie-law-popup&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;tool_policy/policyactions&quot; , &quot;'&quot; , &quot;], function($, Popup, ActionsMod) {
        // Initialise the guest popup.
        $(document).ready(function() {
            // Only show message if there is some policy related to guests.
                // Get localised messages.
                var textmessage = &quot;If you continue browsing this website, you agree to our policies:&quot; +
                   &quot;&lt;ul>&quot; +
                   &quot;&lt;li>&quot; +
                   &quot;&lt;a href=\&quot;https://school.moodledemo.net/admin/tool/policy/view.php?versionid=8&amp;amp;returnurl=https%3A%2F%2Fschool.moodledemo.net%2Flogin%2Findex.php\&quot; &quot; +
                   &quot;   data-action=\&quot;view-guest\&quot; data-versionid=\&quot;8\&quot; data-behalfid=\&quot;1\&quot; >&quot; +
                   &quot;Cookie statement&quot; +
                   &quot;&lt;/a>&quot; +
                   &quot;&lt;/li>&quot; +
                   &quot;&quot; +
                   &quot;&lt;li>&quot; +
                   &quot;&lt;a href=\&quot;https://school.moodledemo.net/admin/tool/policy/view.php?versionid=12&amp;amp;returnurl=https%3A%2F%2Fschool.moodledemo.net%2Flogin%2Findex.php\&quot; &quot; +
                   &quot;   data-action=\&quot;view-guest\&quot; data-versionid=\&quot;12\&quot; data-behalfid=\&quot;1\&quot; >&quot; +
                   &quot;Privacy policy&quot; +
                   &quot;&lt;/a>&quot; +
                   &quot;&lt;/li>&quot; +
                   &quot;&quot; +
                   &quot;&lt;li>&quot; +
                   &quot;&lt;a href=\&quot;https://school.moodledemo.net/admin/tool/policy/view.php?versionid=7&amp;amp;returnurl=https%3A%2F%2Fschool.moodledemo.net%2Flogin%2Findex.php\&quot; &quot; +
                   &quot;   data-action=\&quot;view-guest\&quot; data-versionid=\&quot;7\&quot; data-behalfid=\&quot;1\&quot; >&quot; +
                   &quot;Rules for guests&quot; +
                   &quot;&lt;/a>&quot; +
                   &quot;&lt;/li>&quot; +
                   &quot;&quot; +
                   &quot;&lt;/ul>&quot;;
                var continuemessage = &quot;Continue&quot;;

                // Initialize popup.
                $(document.body).addClass(&quot; , &quot;'&quot; , &quot;eupopup&quot; , &quot;'&quot; , &quot;);
                if ($(&quot;.eupopup&quot;).length > 0) {
                    $(document).euCookieLawPopup().init({
                        popupPosition: &quot; , &quot;'&quot; , &quot;bottom&quot; , &quot;'&quot; , &quot;,
                        popupTitle: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                        popupText: textmessage,
                        buttonContinueTitle: continuemessage,
                        buttonLearnmoreTitle: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                        compactStyle: true,
                    });
                }

            // Initialise the JS for the modal window which displays the policy versions.
            ActionsMod.init(&quot; , &quot;'&quot; , &quot;[data-action=&quot;view-guest&quot;]&quot; , &quot;'&quot; , &quot;);
        });
});

;

require([&quot; , &quot;'&quot; , &quot;theme_boost/footer-popover&quot; , &quot;'&quot; , &quot;], function(FooterPopover) {
    FooterPopover.init();
});
;

M.util.js_pending(&quot; , &quot;'&quot; , &quot;theme_boost/loader&quot; , &quot;'&quot; , &quot;);
require([&quot; , &quot;'&quot; , &quot;theme_boost/loader&quot; , &quot;'&quot; , &quot;], function() {
  M.util.js_complete(&quot; , &quot;'&quot; , &quot;theme_boost/loader&quot; , &quot;'&quot; , &quot;);
});
;

    require([&quot; , &quot;'&quot; , &quot;core_form/submit&quot; , &quot;'&quot; , &quot;], function(Submit) {
        Submit.init(&quot;loginbtn&quot;);
            Submit.init(&quot;loginguestbtn&quot;);
    });
;
M.util.js_pending(&quot; , &quot;'&quot; , &quot;core/notification&quot; , &quot;'&quot; , &quot;); require([&quot; , &quot;'&quot; , &quot;core/notification&quot; , &quot;'&quot; , &quot;], function(amd) {amd.init(1, []); M.util.js_complete(&quot; , &quot;'&quot; , &quot;core/notification&quot; , &quot;'&quot; , &quot;);});;
M.util.js_pending(&quot; , &quot;'&quot; , &quot;core/log&quot; , &quot;'&quot; , &quot;); require([&quot; , &quot;'&quot; , &quot;core/log&quot; , &quot;'&quot; , &quot;], function(amd) {amd.setConfig({&quot;level&quot;:&quot;warn&quot;}); M.util.js_complete(&quot; , &quot;'&quot; , &quot;core/log&quot; , &quot;'&quot; , &quot;);});;
M.util.js_pending(&quot; , &quot;'&quot; , &quot;core/page_global&quot; , &quot;'&quot; , &quot;); require([&quot; , &quot;'&quot; , &quot;core/page_global&quot; , &quot;'&quot; , &quot;], function(amd) {amd.init(); M.util.js_complete(&quot; , &quot;'&quot; , &quot;core/page_global&quot; , &quot;'&quot; , &quot;);});;
M.util.js_pending(&quot; , &quot;'&quot; , &quot;core/utility&quot; , &quot;'&quot; , &quot;); require([&quot; , &quot;'&quot; , &quot;core/utility&quot; , &quot;'&quot; , &quot;], function(amd) {M.util.js_complete(&quot; , &quot;'&quot; , &quot;core/utility&quot; , &quot;'&quot; , &quot;);});
    M.util.js_complete(&quot;core/first&quot;);
});
//]]&gt;



//&lt;![CDATA[
M.str = {&quot;moodle&quot;:{&quot;lastmodified&quot;:&quot;Last modified&quot;,&quot;name&quot;:&quot;Name&quot;,&quot;error&quot;:&quot;Error&quot;,&quot;info&quot;:&quot;Information&quot;,&quot;yes&quot;:&quot;Yes&quot;,&quot;no&quot;:&quot;No&quot;,&quot;ok&quot;:&quot;OK&quot;,&quot;cancel&quot;:&quot;Cancel&quot;,&quot;confirm&quot;:&quot;Confirm&quot;,&quot;areyousure&quot;:&quot;Are you sure?&quot;,&quot;closebuttontitle&quot;:&quot;Close&quot;,&quot;unknownerror&quot;:&quot;Unknown error&quot;,&quot;file&quot;:&quot;File&quot;,&quot;url&quot;:&quot;URL&quot;,&quot;collapseall&quot;:&quot;Collapse all&quot;,&quot;expandall&quot;:&quot;Expand all&quot;},&quot;repository&quot;:{&quot;type&quot;:&quot;Type&quot;,&quot;size&quot;:&quot;Size&quot;,&quot;invalidjson&quot;:&quot;Invalid JSON string&quot;,&quot;nofilesattached&quot;:&quot;No files attached&quot;,&quot;filepicker&quot;:&quot;File picker&quot;,&quot;logout&quot;:&quot;Logout&quot;,&quot;nofilesavailable&quot;:&quot;No files available&quot;,&quot;norepositoriesavailable&quot;:&quot;Sorry, none of your current repositories can return files in the required format.&quot;,&quot;fileexistsdialogheader&quot;:&quot;File exists&quot;,&quot;fileexistsdialog_editor&quot;:&quot;A file with that name has already been attached to the text you are editing.&quot;,&quot;fileexistsdialog_filemanager&quot;:&quot;A file with that name has already been attached&quot;,&quot;renameto&quot;:&quot;Rename to \&quot;{$a}\&quot;&quot;,&quot;referencesexist&quot;:&quot;There are {$a} links to this file&quot;,&quot;select&quot;:&quot;Select&quot;},&quot;admin&quot;:{&quot;confirmdeletecomments&quot;:&quot;Are you sure you want to delete the selected comment(s)?&quot;,&quot;confirmation&quot;:&quot;Confirmation&quot;},&quot;debug&quot;:{&quot;debuginfo&quot;:&quot;Debug info&quot;,&quot;line&quot;:&quot;Line&quot;,&quot;stacktrace&quot;:&quot;Stack trace&quot;},&quot;langconfig&quot;:{&quot;labelsep&quot;:&quot;:&quot;}};
//]]&gt;


//&lt;![CDATA[
(function() {Y.use(&quot;moodle-filter_glossary-autolinker&quot;,function() {M.filter_glossary.init_filter_autolinking({&quot;courseid&quot;:0});
});
M.util.help_popups.setup(Y);
 M.util.js_pending(&quot; , &quot;'&quot; , &quot;random65748643dc765150&quot; , &quot;'&quot; , &quot;); Y.on(&quot; , &quot;'&quot; , &quot;domready&quot; , &quot;'&quot; , &quot;, function() { M.util.js_complete(&quot;init&quot;);  M.util.js_complete(&quot; , &quot;'&quot; , &quot;random65748643dc765150&quot; , &quot;'&quot; , &quot;); });
})();
//]]&gt;


            
            
                Powered by Moodle
            
        
    
        
            
                
            
        
    This site will be reset in 37 mins 09 secs



/html[@class=&quot;yui3-js-enabled&quot;]If you continue browsing this website, you agree to our policies:Cookie statementPrivacy policyRules for guestsContinuex&quot;))]</value>
      <webElementGuid>d563cb82-2b20-497c-88f3-8b6653ed7292</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
