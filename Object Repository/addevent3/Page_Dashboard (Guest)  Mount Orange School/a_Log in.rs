<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Log in</name>
   <tag></tag>
   <elementGuidId>f43e6045-9f63-45f4-b5e6-cf8ba9728130</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='usernavigation']/div[5]/div/span/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.login.pl-2 > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>966d33fa-b5c4-419a-82f0-02aff2292e3c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://school.moodledemo.net/login/index.php</value>
      <webElementGuid>de5578e7-8032-443b-9adb-a043f4d4f092</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Log in</value>
      <webElementGuid>6f47f5d9-6729-49f7-a12e-c31a0bc420c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;usernavigation&quot;)/div[@class=&quot;d-flex align-items-stretch usermenu-container&quot;]/div[@class=&quot;usermenu&quot;]/span[@class=&quot;login pl-2&quot;]/a[1]</value>
      <webElementGuid>76da4967-2e96-4f1d-835f-3ae68126a74a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='usernavigation']/div[5]/div/span/a</value>
      <webElementGuid>669b885f-c70f-44e9-8d86-753c820eecb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Log in')]</value>
      <webElementGuid>b47ab664-2d92-4d75-9ec3-8d0537561b35</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://school.moodledemo.net/login/index.php')]</value>
      <webElementGuid>54ef7421-1cd1-45d6-a896-26a8e41d586e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/a</value>
      <webElementGuid>a1c4aef3-f076-46a4-9e29-e29d691fa75b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://school.moodledemo.net/login/index.php' and (text() = 'Log in' or . = 'Log in')]</value>
      <webElementGuid>5f080402-6cda-408f-a34d-3d7db2fe45cb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
