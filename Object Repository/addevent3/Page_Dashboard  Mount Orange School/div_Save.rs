<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Save</name>
   <tag></tag>
   <elementGuidId>ab0c90dc-d54d-4257-a7d1-c9933ecd61ff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='yui_3_18_1_1_1701792699145_239']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#yui_3_18_1_1_1701792699145_239</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a78ecb8e-e5fc-4b10-9c7e-8db59c7b4952</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-footer</value>
      <webElementGuid>959f3147-94f1-45ed-b84b-a0fb4587099e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-region</name>
      <type>Main</type>
      <value>footer</value>
      <webElementGuid>dbf33646-fc32-4be8-bdb9-c9daa92fc91b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>yui_3_18_1_1_1701792699145_239</value>
      <webElementGuid>7a6a58c9-0a7a-4ee0-8f89-4387949b743c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

        

            Save
            


                
            
        
                </value>
      <webElementGuid>e56c8afe-521a-4454-9fb0-286ccb3b2b12</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;yui_3_18_1_1_1701792699145_239&quot;)</value>
      <webElementGuid>ac331f66-d631-4b12-8b08-e8617470c060</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='yui_3_18_1_1_1701792699145_239']</value>
      <webElementGuid>2a0060b2-29aa-4a65-aa22-7c375057cc93</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='yui_3_18_1_1_1701792699145_240']/div[3]</value>
      <webElementGuid>7a6b82f3-abb8-4a5b-abfa-863a0417edae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div[2]/div/div/div[3]</value>
      <webElementGuid>b8e2f8a3-c0ae-4656-9a9b-b0ad9de69c7f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'yui_3_18_1_1_1701792699145_239' and (text() = '

        

            Save
            


                
            
        
                ' or . = '

        

            Save
            


                
            
        
                ')]</value>
      <webElementGuid>16174f14-2043-40f3-94fd-3978895f158c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
