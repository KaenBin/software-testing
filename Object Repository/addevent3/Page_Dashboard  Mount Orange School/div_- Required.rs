<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_- Required</name>
   <tag></tag>
   <elementGuidId>efe8b617-9ebd-4a13-8dd1-d1461499f3fb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='yui_3_18_1_1_1701792699145_256']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#yui_3_18_1_1_1701792699145_256</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>484818ee-a773-420c-9e26-9269e5b51ecb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-9 form-inline align-items-start felement</value>
      <webElementGuid>4faae503-0516-4101-bef1-da02a682710c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-fieldtype</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>192798fa-dd5a-4d34-8864-ac0b4050d968</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>yui_3_18_1_1_1701792699145_256</value>
      <webElementGuid>38ffdb8d-97ac-415d-9332-9dee98d61a46</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
        
 - Required
    </value>
      <webElementGuid>3f8ec859-8c34-4571-bcdd-d0da167465d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;yui_3_18_1_1_1701792699145_256&quot;)</value>
      <webElementGuid>6afc0184-30c8-4f56-b494-07bd26892f26</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='yui_3_18_1_1_1701792699145_256']</value>
      <webElementGuid>5523925b-d55c-4150-988c-2e406c73bd7b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='fitem_id_name']/div[2]</value>
      <webElementGuid>ee1e88ef-28f6-4289-b630-ae96af7af8be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset/div[2]/div/div[2]</value>
      <webElementGuid>030e0311-13f9-4580-a05a-d7f063e81736</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'yui_3_18_1_1_1701792699145_256' and (text() = '
        
        
 - Required
    ' or . = '
        
        
 - Required
    ')]</value>
      <webElementGuid>42d80444-d07c-49af-a5df-ecc8dbbb323e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
