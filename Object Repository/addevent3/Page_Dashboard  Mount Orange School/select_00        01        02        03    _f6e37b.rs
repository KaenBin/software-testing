<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_00        01        02        03    _f6e37b</name>
   <tag></tag>
   <elementGuidId>8a707ed3-dbe7-4e6f-b666-4a3bf81598ce</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='id_timestart_hour']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#id_timestart_hour</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>68f37100-e9ce-45bb-8ae4-39412e742ad6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>custom-select
                   
                   </value>
      <webElementGuid>4ec8c6a4-8098-4710-af55-a8ac13a2acb2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>timestart[hour]</value>
      <webElementGuid>5b192e7c-6bab-49be-8488-dffb469f68d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>id_timestart_hour</value>
      <webElementGuid>55e9fad6-e0e6-48b2-b1da-274237723dee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-initial-value</name>
      <type>Main</type>
      <value>17</value>
      <webElementGuid>1980d3d8-1a8a-4401-b0a2-aca219a6c112</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        00
        01
        02
        03
        04
        05
        06
        07
        08
        09
        10
        11
        12
        13
        14
        15
        16
        17
        18
        19
        20
        21
        22
        23
    </value>
      <webElementGuid>ddbc6c48-b685-4b44-8999-b8794a957973</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;id_timestart_hour&quot;)</value>
      <webElementGuid>1b47c205-6b93-4d14-bd75-f0c657c1d5fc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='id_timestart_hour']</value>
      <webElementGuid>d5e544c2-e8b0-4a2b-af22-4882d6caefbb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='yui_3_18_1_1_1701792622153_383']/select</value>
      <webElementGuid>f42d9f27-cf34-4f28-8629-21af32803058</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/span/select</value>
      <webElementGuid>76d6e5db-c4db-4e9e-9ede-9753ca62d28e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'timestart[hour]' and @id = 'id_timestart_hour' and (text() = '
        00
        01
        02
        03
        04
        05
        06
        07
        08
        09
        10
        11
        12
        13
        14
        15
        16
        17
        18
        19
        20
        21
        22
        23
    ' or . = '
        00
        01
        02
        03
        04
        05
        06
        07
        08
        09
        10
        11
        12
        13
        14
        15
        16
        17
        18
        19
        20
        21
        22
        23
    ')]</value>
      <webElementGuid>ca0a744c-a2f6-4009-ac68-d803fadb8a96</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
