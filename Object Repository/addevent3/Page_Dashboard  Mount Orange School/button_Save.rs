<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Save</name>
   <tag></tag>
   <elementGuidId>8d3df818-5f0d-41f5-a1bb-17ff814f2df7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='yui_3_18_1_1_1701792622153_238']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#yui_3_18_1_1_1701792622153_238</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>dc5c3d1c-b399-4d40-9ecb-3b7f97795ea1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>ceb2a5da-960e-4f3a-a63f-7e7a98398b4c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
      <webElementGuid>9f43ec28-b8eb-4289-b0cd-7143304ab0e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-action</name>
      <type>Main</type>
      <value>save</value>
      <webElementGuid>61799daa-6e6b-4962-8996-5e214b7fa732</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>yui_3_18_1_1_1701792622153_238</value>
      <webElementGuid>2db9e38c-e24f-4bb7-a53d-50ae71af75e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

            Save
            


                
            
        </value>
      <webElementGuid>f7fdf714-f5e1-4e58-8809-4d418f95fccd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;yui_3_18_1_1_1701792622153_238&quot;)</value>
      <webElementGuid>2fe4df50-52da-414d-8a4a-e381a5aba160</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='yui_3_18_1_1_1701792622153_238']</value>
      <webElementGuid>8dab57fb-5fa0-4821-a1a4-2ff6ce6f3d6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='yui_3_18_1_1_1701792622153_239']/button</value>
      <webElementGuid>f0a3a769-5cc2-4535-92b4-7eb9f075000d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div[3]/button</value>
      <webElementGuid>aafd0843-1296-4595-914e-6c71cec453fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'yui_3_18_1_1_1701792622153_238' and (text() = '

            Save
            


                
            
        ' or . = '

            Save
            


                
            
        ')]</value>
      <webElementGuid>10e56f7a-b983-427a-a80b-59854c7028fb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
