<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_New event</name>
   <tag></tag>
   <elementGuidId>e4d408df-95f7-4413-b20c-15ad4fcb31ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='calendar-month-656f4b6bd3b37656f4b6b92f0e3-1']/div/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-primary.float-sm-right.float-right</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>c14a3cbf-5ad1-4fdf-b807-7e1b221105bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary float-sm-right float-right</value>
      <webElementGuid>9fc5a392-8b30-4f1e-8158-770699b15e13</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-context-id</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>c62396c1-85e4-431f-a026-a16390cab51b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-action</name>
      <type>Main</type>
      <value>new-event-button</value>
      <webElementGuid>e68ca177-3a2f-420a-a3b0-f9b08b21954e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            New event
        </value>
      <webElementGuid>aaa0ace1-9f1b-4acd-9fa9-8b80afb86e49</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;calendar-month-656f4b6bd3b37656f4b6b92f0e3-1&quot;)/div[@class=&quot;header d-flex flex-wrap p-1&quot;]/button[@class=&quot;btn btn-primary float-sm-right float-right&quot;]</value>
      <webElementGuid>88b28e62-c62f-48ad-8551-e206c4f827bf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='calendar-month-656f4b6bd3b37656f4b6b92f0e3-1']/div/button</value>
      <webElementGuid>90a238c7-30e0-41d0-b378-531924875fef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section[3]/div/div/div/div/div/button</value>
      <webElementGuid>3788be6b-8a54-4561-b7e8-323107d2e2a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
            New event
        ' or . = '
            New event
        ')]</value>
      <webElementGuid>ddd6c23f-8dce-4e25-bfdf-29869b99aecb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
