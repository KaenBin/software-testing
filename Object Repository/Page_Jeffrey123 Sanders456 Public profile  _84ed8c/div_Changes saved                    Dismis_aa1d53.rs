<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Changes saved                    Dismis_aa1d53</name>
   <tag></tag>
   <elementGuidId>475200be-d265-4f8e-aefd-1d02c90601ce</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='user-notifications']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-success.alert-block.fade.in.alert-dismissible</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>8185f174-a5fc-4f70-aaf4-670101c2b540</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-success alert-block fade in  alert-dismissible</value>
      <webElementGuid>62e40fa0-6af8-43db-9f2f-20ca9d1d9397</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>alert</value>
      <webElementGuid>ad3836db-bd77-42a7-8cb0-1a93d8b3565f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    Changes saved
    
        ×
        Dismiss this notification
    
 </value>
      <webElementGuid>980f6349-aa5f-44d5-93e0-fef46cb4ce9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;user-notifications&quot;)/div[@class=&quot;alert alert-success alert-block fade in  alert-dismissible&quot;]</value>
      <webElementGuid>a9a78644-37ea-43d8-90a7-fbae0f5da3ee</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='user-notifications']/div</value>
      <webElementGuid>c6c3dbfa-f5ba-42c0-bc7d-c181669ef58f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/div</value>
      <webElementGuid>63f2ff67-4369-4eeb-ae5c-20c7c0be2da9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    Changes saved
    
        ×
        Dismiss this notification
    
 ' or . = '
    Changes saved
    
        ×
        Dismiss this notification
    
 ')]</value>
      <webElementGuid>98a0a3ff-8561-465a-a7cd-4a7617237ba2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
