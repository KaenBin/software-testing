<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_Adding a new Workshop</name>
   <tag></tag>
   <elementGuidId>adef6026-42d1-4b68-971f-ee23e242a6e4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='yui_3_18_1_1_1702135393519_303']/h2</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>2db0adf7-42ff-4a72-8f38-ae1abad9eb28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Adding a new Workshop
  
</value>
      <webElementGuid>d9f41350-f227-4d5c-bb88-820d9d936602</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;yui_3_18_1_1_1702135393519_303&quot;)/h2[1]</value>
      <webElementGuid>2ece53f7-288a-435f-a68c-e0e61d6d9c25</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='yui_3_18_1_1_1702135393519_303']/h2</value>
      <webElementGuid>2863c47a-93ff-4b19-9fa6-df73195ddf96</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h2</value>
      <webElementGuid>cdd26f21-b862-4204-aaa2-962eb8beaca1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = 'Adding a new Workshop
  
' or . = 'Adding a new Workshop
  
')]</value>
      <webElementGuid>ff3babe9-a222-455b-b50c-089065174f6e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
