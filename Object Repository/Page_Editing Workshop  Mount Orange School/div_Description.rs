<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Description</name>
   <tag></tag>
   <elementGuidId>4029cfae-4445-4efb-acc1-d4dea4b77d58</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#fitem_id_introeditor > div.col-md-3.col-form-label.d-flex.pb-0.pr-md-0</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='fitem_id_introeditor']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b1ee2185-0687-489a-b850-b6249b751f36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-3 col-form-label d-flex pb-0 pr-md-0</value>
      <webElementGuid>c322e8ee-3d3b-4b13-b38e-30d47c406fac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
                
                    Description
                
        
        
            
        
    </value>
      <webElementGuid>da6618b5-e3df-46ef-83ff-4658b3e9ffda</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;fitem_id_introeditor&quot;)/div[@class=&quot;col-md-3 col-form-label d-flex pb-0 pr-md-0&quot;]</value>
      <webElementGuid>4e89bfaf-b510-48a8-99db-393fef4adfca</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='fitem_id_introeditor']/div</value>
      <webElementGuid>3622cd8c-15ea-47a8-8dc6-1959e71bcc1e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset/div[2]/div[2]/div</value>
      <webElementGuid>9dd44a57-0a59-4ba0-8bb7-4e8d93ba2750</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        
                
                    Description
                
        
        
            
        
    ' or . = '
        
                
                    Description
                
        
        
            
        
    ')]</value>
      <webElementGuid>c3d2c0e4-3faa-4945-b78c-33a6870280c4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
