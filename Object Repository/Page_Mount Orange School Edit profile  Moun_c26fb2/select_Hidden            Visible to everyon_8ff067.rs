<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Hidden            Visible to everyon_8ff067</name>
   <tag></tag>
   <elementGuidId>debac904-e89a-4582-9a9d-d51ed3de3109</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='id_maildisplay']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#id_maildisplay</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>2564ed34-2db8-4e17-9227-d7068dc7f878</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>custom-select
                       
                       </value>
      <webElementGuid>9c95fb1a-4e3c-40e9-8559-6a51799de193</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>maildisplay</value>
      <webElementGuid>839d3db7-9298-463a-9e14-179fbdd9bdc8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>id_maildisplay</value>
      <webElementGuid>3253d6b4-4232-4da4-bcee-33ff64e9e0c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Hidden
            Visible to everyone
            Visible to course participants
        </value>
      <webElementGuid>9d742afe-36e8-4361-9ba6-44c9d817fee2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;id_maildisplay&quot;)</value>
      <webElementGuid>8d1ecff6-45cd-4ea1-9c86-25bcef03f86c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='id_maildisplay']</value>
      <webElementGuid>bb60eac0-b607-428a-9727-dbf6cb35364f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='fitem_id_maildisplay']/div[2]/select</value>
      <webElementGuid>3c7f0638-fdb2-4695-9ead-fe37073476cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>ff96b9fa-873c-4ec7-89b5-0059318b762d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'maildisplay' and @id = 'id_maildisplay' and (text() = '
            Hidden
            Visible to everyone
            Visible to course participants
        ' or . = '
            Hidden
            Visible to everyone
            Visible to course participants
        ')]</value>
      <webElementGuid>e9e6ea66-c3c0-4f37-93af-6be1d044b534</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
